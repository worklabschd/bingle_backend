'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _kue = require('kue');

var _kue2 = _interopRequireDefault(_kue);

var _logging = require('../utils/logging');

var _logging2 = _interopRequireDefault(_logging);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isProd = process.env.NODE_ENV === 'production';

var q = _kue2.default.createQueue({
    prefix: 'q',
    redis: {
        port: 6379,
        host: isProd ? 'redis' : 'localhost'
    }
});

// Allowing the pending jobs to finish.
var graceful = function graceful(code) {
    return function () {
        q.shutdown(2000, function (err) {
            if (err) {
                (0, _logging2.default)('In Kue shutdown: ' + err.toString());
            }
            process.exit(code);
        });
    };
};

process.once('SIGINT', graceful(1));

process.once('SIGTERM', graceful(0));

q.on('error', function (err) {
    (0, _logging2.default)('In Kue error: ' + err.toString());
    if (err.code === 'ECONNREFUSED') {
        process.exit(1);
    }
});

function Queue(name, delay, ttl, cb) {
    var _this = this;

    q.process(name, function () {
        var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(job, done) {
            return _regenerator2.default.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            _context.prev = 0;
                            _context.next = 3;
                            return cb(job.data);

                        case 3:
                            done(null);
                            _context.next = 10;
                            break;

                        case 6:
                            _context.prev = 6;
                            _context.t0 = _context['catch'](0);

                            (0, _logging2.default)('Error In Que - ' + name + ', data - ' + job.data + ': ' + _context.t0.toString());
                            done(_context.t0);

                        case 10:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, _this, [[0, 6]]);
        }));

        return function (_x, _x2) {
            return _ref.apply(this, arguments);
        };
    }());

    this.name = name;
    this.delay = delay;
    this.ttl = ttl;
}

Queue.prototype.queue = function (data) {
    q.create(this.name, data).delay(this.delay).ttl(this.ttl).attempts(100).backoff({ delay: this.delay, type: 'fixed' }).removeOnComplete(true).save();
};

exports.default = Queue;