'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _queue = require('./queue');

var _queue2 = _interopRequireDefault(_queue);

var _guide = require('../models/guide');

var _guide2 = _interopRequireDefault(_guide);

var _config = require('../config.json');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Un queue a guide so he gets a booking again.
var unQueueGuide = function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(guideID) {
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        console.log('unqueing this guide', guideID);
                        _context.next = 3;
                        return _guide2.default.update({ _id: guideID }, { onQueue: false });

                    case 3:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function unQueueGuide(_x) {
        return _ref.apply(this, arguments);
    };
}();

// Start a que for Un queueing a guide after 5 mins of having received a call.
exports.default = new _queue2.default('unQueueGuide', _config.callConfig.GUIDE_QUEUE_PERIOD * 1000, 180000, unQueueGuide);