'use strict';

/* eslint-disable */
function onSubmit() {
    var form = document.getElementById('form');
    var password = form.password.value;
    var confirmPassword = form.confirmPassword.value;
    var token = form.token.value;
    if (!password || !confirmPassword) {
        swal('Fields Empty!', 'Please fill in both fields to continue.', 'error');
        return;
    }
    if (password !== confirmPassword) {
        swal('Password Mismatch!', 'Password fields do not match. Please try again.', 'error');
        return;
    }
    axios.post('/user/reset-password', {
        password: password,
        token: token
    }).then(function () {
        swal('Success!', 'Password successfully changed. Please login in inside the app using your new password.', 'success');
    }).catch(function (err) {
        console.warn(err);
        swal('Error', 'Having trouble connecting to the server. Please try again later.', 'warning');
    });
}
/* eslint-enable */