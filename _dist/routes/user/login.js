'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.resetPassword = exports.getResetPassword = exports.forgotPassword = exports.socialLogin = exports.verifyOTPHandler = exports.retryOTPHandler = exports.sendOTPHandler = exports.validateEmail = exports.register = exports.login = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _models = require('../../models');

var _responses = require('../utils/responses');

var _otp = require('../../api/otp');

var _social = require('../../api/social');

var _email = require('../utils/email');

var _token = require('../utils/token');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by M on 06/05/17. With ❤
 */

var login = {
    method: 'POST',
    path: '/user/login',
    config: {
        tags: ['user', 'api'],
        description: 'User logs in inside the app using this endpoint.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                email: _joi2.default.string().email().required(),
                password: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var _request$payload, email, password, user;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _request$payload = request.payload, email = _request$payload.email, password = _request$payload.password;
                                _context.next = 4;
                                return _models.User.findOne({ email: email });

                            case 4:
                                user = _context.sent;

                                if (!user) {
                                    reply({ message: 'INVALID EMAIL' }).code(400);
                                } else if (user.comparePassword(password)) {
                                    reply({
                                        id: user.id,
                                        name: user.name,
                                        email: user.email,
                                        countryCode: user.countryCode,
                                        phone: user.phone,
                                        age: user.age,
                                        gender: user.gender,
                                        avatar: user.avatar
                                    });
                                } else {
                                    reply({ message: 'INVALID PASSWORD' }).code(400);
                                }
                                _context.next = 11;
                                break;

                            case 8:
                                _context.prev = 8;
                                _context.t0 = _context['catch'](0);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 11:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[0, 8]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.userResponse
    }
};

var register = {
    method: 'POST',
    path: '/user/register',
    config: {
        tags: ['user', 'api'],
        description: 'User registers inside the app using this endpoint.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                name: _joi2.default.optional(),
                countryCode: _joi2.default.string().required(),
                phone: _joi2.default.string().required(),
                email: _joi2.default.string().email().required(),
                password: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(request, reply) {
                var _request$payload2, email, password, countryCode, phone, name, oldUser, hashed, user;

                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                _request$payload2 = request.payload, email = _request$payload2.email, password = _request$payload2.password, countryCode = _request$payload2.countryCode, phone = _request$payload2.phone;
                                name = request.payload.name || 'Meego Traveller';
                                _context2.next = 5;
                                return _models.User.findOne({ email: email });

                            case 5:
                                oldUser = _context2.sent;

                                if (!oldUser) {
                                    _context2.next = 9;
                                    break;
                                }

                                reply({ message: 'EMAIL TAKEN' }).code(400);
                                return _context2.abrupt('return');

                            case 9:
                                hashed = _bcrypt2.default.hashSync(password, 10);
                                _context2.next = 12;
                                return _models.User.create({
                                    email: email, password: hashed, countryCode: countryCode, phone: phone, name: name
                                });

                            case 12:
                                user = _context2.sent;

                                if (user) {
                                    reply({
                                        id: user.id,
                                        name: name,
                                        countryCode: countryCode,
                                        phone: phone,
                                        email: email
                                    });
                                }
                                _context2.next = 19;
                                break;

                            case 16:
                                _context2.prev = 16;
                                _context2.t0 = _context2['catch'](0);

                                (0, _responses.handleError)(request, reply, _context2.t0);

                            case 19:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, undefined, [[0, 16]]);
            }));

            function handler(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.userResponse
    }
};

var validateEmail = {
    method: 'GET',
    path: '/user/validate-email',
    config: {
        tags: ['user', 'api'],
        description: 'Validates uniqueness of a user\'s email.',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                email: _joi2.default.string().email().required()
            }
        },
        handler: function () {
            var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(request, reply) {
                var email, user;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;
                                email = request.query.email;
                                _context3.next = 4;
                                return _models.User.findOne({ email: email });

                            case 4:
                                user = _context3.sent;

                                if (!user) {
                                    reply({ message: 'OK' });
                                } else {
                                    reply({ message: 'EMAIL TAKEN' }).code(400);
                                }
                                _context3.next = 11;
                                break;

                            case 8:
                                _context3.prev = 8;
                                _context3.t0 = _context3['catch'](0);

                                (0, _responses.handleError)(request, reply, _context3.t0);

                            case 11:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, undefined, [[0, 8]]);
            }));

            function handler(_x5, _x6) {
                return _ref3.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

var sendOTPHandler = {
    method: 'GET',
    path: '/user/send-otp',
    config: {
        tags: ['user', 'api'],
        description: 'Used for sending OTP to the user for phone verification. (On the condition that the phone number is unique)',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                countryCode: _joi2.default.string().required(),
                phone: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(request, reply) {
                var _request$query, countryCode, phone;

                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                try {
                                    _request$query = request.query, countryCode = _request$query.countryCode, phone = _request$query.phone;

                                    (0, _otp.sendOTP)(countryCode, phone, function (err) {
                                        if (err) {
                                            (0, _responses.handleError)(request, reply, err);
                                        } else {
                                            reply({ message: 'OK' });
                                        }
                                    });
                                } catch (err) {
                                    (0, _responses.handleError)(request, reply, err);
                                }

                            case 1:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, undefined);
            }));

            function handler(_x7, _x8) {
                return _ref4.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

var retryOTPHandler = {
    method: 'GET',
    path: '/user/resend-otp',
    config: {
        tags: ['user', 'api'],
        description: 'Used for re sending OTP to the user for phone verification in case the earlier attempt fails.',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                countryCode: _joi2.default.string().required(),
                phone: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(request, reply) {
                var _request$query2, countryCode, phone;

                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                try {
                                    _request$query2 = request.query, countryCode = _request$query2.countryCode, phone = _request$query2.phone;

                                    (0, _otp.retryOTP)(countryCode, phone, function (err) {
                                        if (err) {
                                            (0, _responses.handleError)(request, reply, err);
                                        } else {
                                            reply({ message: 'OK' });
                                        }
                                    });
                                } catch (err) {
                                    (0, _responses.handleError)(request, reply, err);
                                }

                            case 1:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, undefined);
            }));

            function handler(_x9, _x10) {
                return _ref5.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

var verifyOTPHandler = {
    method: 'GET',
    path: '/user/verify-otp',
    config: {
        tags: ['user', 'api'],
        description: 'Used for veryfing OTP sent by the user.',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                countryCode: _joi2.default.string().required(),
                phone: _joi2.default.string().required(),
                otp: _joi2.default.number().required()
            }
        },
        handler: function () {
            var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(request, reply) {
                var _request$query3, countryCode, phone, otp;

                return _regenerator2.default.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                try {
                                    _request$query3 = request.query, countryCode = _request$query3.countryCode, phone = _request$query3.phone, otp = _request$query3.otp;

                                    (0, _otp.verifyOTP)(countryCode, phone, otp, function (err, status) {
                                        if (err) {
                                            (0, _responses.handleError)(request, reply, err);
                                            return;
                                        }
                                        switch (status) {
                                            case 'otp_verified':
                                                reply({ message: 'OK', status: 'OTP_VALID' });
                                                break;
                                            case 'otp_not_verified':
                                                reply({ message: 'ERROR', status: 'OTP_INVALID' });
                                                break;
                                            case 'otp_expired':
                                                reply({ message: 'ERROR', status: 'OTP_EXPIRED' });
                                                break;
                                            case 'mobile_not_found':
                                                reply({ message: 'ERROR', status: 'MOBILE_NOT_FOUND' });
                                                break;
                                            case 'invalid_mobile':
                                                reply({ message: 'ERROR', status: 'INVALID_MOBILE' });
                                                break;
                                            default:
                                                reply({ message: 'ERROR', status: status });
                                        }
                                    });
                                } catch (err) {
                                    (0, _responses.handleError)(request, reply, err);
                                }

                            case 1:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, undefined);
            }));

            function handler(_x11, _x12) {
                return _ref6.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.otpResponse
    }
};

var socialLogin = {
    method: 'POST',
    path: '/user/social-login',
    config: {
        tags: ['user', 'api'],
        description: 'Used for social login. Takes a OAuth token and a type. Response may not have all the user details depending on the login status.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                type: _joi2.default.string().valid(['facebook', 'google']).required(),
                token: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref7 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee7(request, reply) {
                var _request$payload3, type, token, resp, existingUser;

                return _regenerator2.default.wrap(function _callee7$(_context7) {
                    while (1) {
                        switch (_context7.prev = _context7.next) {
                            case 0:
                                _context7.prev = 0;
                                _request$payload3 = request.payload, type = _request$payload3.type, token = _request$payload3.token;
                                resp = void 0;

                                if (!(type === 'facebook')) {
                                    _context7.next = 9;
                                    break;
                                }

                                _context7.next = 6;
                                return (0, _social.facebookLogin)(token);

                            case 6:
                                resp = _context7.sent;
                                _context7.next = 12;
                                break;

                            case 9:
                                _context7.next = 11;
                                return (0, _social.googleLogin)(token);

                            case 11:
                                resp = _context7.sent;

                            case 12:
                                _context7.next = 14;
                                return _models.User.findOne({ email: resp.email });

                            case 14:
                                existingUser = _context7.sent;

                                if (existingUser) {
                                    _context7.next = 18;
                                    break;
                                }

                                reply({
                                    status: 'NEW',
                                    email: resp.email,
                                    name: resp.name,
                                    gender: resp.gender,
                                    age: resp.birthday
                                });
                                return _context7.abrupt('return');

                            case 18:
                                // If user is already registered in our system,
                                // fetch all the details and proceed with login.
                                reply({
                                    status: 'EXISTING',
                                    id: existingUser.id,
                                    email: existingUser.email,
                                    name: existingUser.name,
                                    gender: existingUser.gender,
                                    age: existingUser.age,
                                    avatar: existingUser.avatar,
                                    countryCode: existingUser.countryCode,
                                    phone: existingUser.phone
                                });
                                _context7.next = 24;
                                break;

                            case 21:
                                _context7.prev = 21;
                                _context7.t0 = _context7['catch'](0);

                                (0, _responses.handleError)(request, reply, _context7.t0);

                            case 24:
                            case 'end':
                                return _context7.stop();
                        }
                    }
                }, _callee7, undefined, [[0, 21]]);
            }));

            function handler(_x13, _x14) {
                return _ref7.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.socialResponse
    }
};

var forgotPassword = {
    method: 'GET',
    path: '/user/forgot-password',
    config: {
        tags: ['user', 'api'],
        description: 'A user sends his email and we send an email with further instructions on how to reset their password.',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                email: _joi2.default.string().email().required()
            }
        },
        handler: function () {
            var _ref8 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee8(request, reply) {
                var email, user, token;
                return _regenerator2.default.wrap(function _callee8$(_context8) {
                    while (1) {
                        switch (_context8.prev = _context8.next) {
                            case 0:
                                email = request.query.email;
                                _context8.prev = 1;
                                _context8.next = 4;
                                return _models.User.findOne({ email: email });

                            case 4:
                                user = _context8.sent;

                                if (user) {
                                    _context8.next = 8;
                                    break;
                                }

                                reply({ message: 'INVALID EMAIL' }).code(400);
                                return _context8.abrupt('return');

                            case 8:
                                _context8.next = 10;
                                return (0, _token.createToken)(user.id);

                            case 10:
                                token = _context8.sent;
                                _context8.next = 13;
                                return (0, _email.sendForgotPasswordEmail)(email, token);

                            case 13:
                                reply({ message: 'OK' });
                                _context8.next = 19;
                                break;

                            case 16:
                                _context8.prev = 16;
                                _context8.t0 = _context8['catch'](1);

                                (0, _responses.handleError)(request, reply, _context8.t0);

                            case 19:
                            case 'end':
                                return _context8.stop();
                        }
                    }
                }, _callee8, undefined, [[1, 16]]);
            }));

            function handler(_x15, _x16) {
                return _ref8.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

var getResetPassword = {
    method: 'GET',
    path: '/user/reset-password',
    config: {
        tags: ['user', 'api'],
        description: 'Handler that serves the reset password form.',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                token: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref9 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee9(request, reply) {
                var token, decoded, user;
                return _regenerator2.default.wrap(function _callee9$(_context9) {
                    while (1) {
                        switch (_context9.prev = _context9.next) {
                            case 0:
                                token = request.query.token;
                                _context9.prev = 1;
                                _context9.next = 4;
                                return (0, _token.verifyToken)(token);

                            case 4:
                                decoded = _context9.sent;

                                if (!(decoded === _token.EXPIRED_TOKEN)) {
                                    _context9.next = 8;
                                    break;
                                }

                                reply({ message: 'TOKEN EXPIRED' }).status(400);
                                return _context9.abrupt('return');

                            case 8:
                                if (!(decoded === _token.INVALID_TOKEN)) {
                                    _context9.next = 11;
                                    break;
                                }

                                reply({ message: 'TOKEN INVALID' }).status(400);
                                return _context9.abrupt('return');

                            case 11:
                                _context9.next = 13;
                                return _models.User.findById(decoded.data);

                            case 13:
                                user = _context9.sent;

                                if (user) {
                                    _context9.next = 17;
                                    break;
                                }

                                reply({ message: 'TOKEN INVALID' }).status(400);
                                return _context9.abrupt('return');

                            case 17:
                                reply.view('resetPassword', {
                                    token: token
                                });
                                _context9.next = 23;
                                break;

                            case 20:
                                _context9.prev = 20;
                                _context9.t0 = _context9['catch'](1);

                                (0, _responses.handleError)(request, reply, _context9.t0);

                            case 23:
                            case 'end':
                                return _context9.stop();
                        }
                    }
                }, _callee9, undefined, [[1, 20]]);
            }));

            function handler(_x17, _x18) {
                return _ref9.apply(this, arguments);
            }

            return handler;
        }()
    }
};

var resetPassword = {
    method: 'POST',
    path: '/user/reset-password',
    config: {
        tags: ['user', 'api'],
        description: 'Handler that resets the user\'s password.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                password: _joi2.default.string().required(),
                token: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref10 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee10(request, reply) {
                var _request$payload4, token, password, decoded, user, hashed;

                return _regenerator2.default.wrap(function _callee10$(_context10) {
                    while (1) {
                        switch (_context10.prev = _context10.next) {
                            case 0:
                                _request$payload4 = request.payload, token = _request$payload4.token, password = _request$payload4.password;
                                _context10.prev = 1;
                                _context10.next = 4;
                                return (0, _token.verifyToken)(token);

                            case 4:
                                decoded = _context10.sent;

                                if (!(decoded === _token.EXPIRED_TOKEN)) {
                                    _context10.next = 8;
                                    break;
                                }

                                reply({ message: 'TOKEN EXPIRED' }).status(400);
                                return _context10.abrupt('return');

                            case 8:
                                if (!(decoded === _token.INVALID_TOKEN)) {
                                    _context10.next = 11;
                                    break;
                                }

                                reply({ message: 'TOKEN INVALID' }).status(400);
                                return _context10.abrupt('return');

                            case 11:
                                _context10.next = 13;
                                return _models.User.findById(decoded.data);

                            case 13:
                                user = _context10.sent;

                                if (user) {
                                    _context10.next = 17;
                                    break;
                                }

                                reply({ message: 'TOKEN INVALID' }).status(400);
                                return _context10.abrupt('return');

                            case 17:
                                hashed = _bcrypt2.default.hashSync(password, 10);

                                user.password = hashed;
                                _context10.next = 21;
                                return user.save();

                            case 21:
                                reply({ message: 'OK' });
                                _context10.next = 27;
                                break;

                            case 24:
                                _context10.prev = 24;
                                _context10.t0 = _context10['catch'](1);

                                (0, _responses.handleError)(request, reply, _context10.t0);

                            case 27:
                            case 'end':
                                return _context10.stop();
                        }
                    }
                }, _callee10, undefined, [[1, 24]]);
            }));

            function handler(_x19, _x20) {
                return _ref10.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

exports.login = login;
exports.register = register;
exports.validateEmail = validateEmail;
exports.sendOTPHandler = sendOTPHandler;
exports.retryOTPHandler = retryOTPHandler;
exports.verifyOTPHandler = verifyOTPHandler;
exports.socialLogin = socialLogin;
exports.forgotPassword = forgotPassword;
exports.getResetPassword = getResetPassword;
exports.resetPassword = resetPassword;