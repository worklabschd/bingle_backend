'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getPaymentScreen = exports.makePayment = exports.getDue = undefined;

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _models = require('../../models');

var _responses = require('../utils/responses');

var _razorpay = require('../../api/razorpay');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getDue = {
    method: 'GET',
    path: '/user/payment-due',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User gets his payment from here.',
        notes: 'This should be authenticated.',
        validate: {
            query: {
                id: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var id, bookings, unbilledBookings, due, receipts, paid;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                id = request.query.id;
                                _context.next = 4;
                                return _models.CallRequest.count({
                                    user: id,
                                    $or: [{ status: _models.RequestStatus.UNBILLED }, { status: _models.RequestStatus.BILLED }]
                                });

                            case 4:
                                bookings = _context.sent;
                                _context.next = 7;
                                return _models.CallRequest.find({
                                    user: id,
                                    status: _models.RequestStatus.UNBILLED
                                });

                            case 7:
                                unbilledBookings = _context.sent;
                                due = unbilledBookings.reduce(function (acc, booking) {
                                    return acc + (booking.fare - 0);
                                }, 0);
                                _context.next = 11;
                                return _models.UserReceipt.find({
                                    user: id
                                });

                            case 11:
                                receipts = _context.sent;
                                paid = receipts.reduce(function (acc, receipt) {
                                    return acc + (receipt.amount - 0);
                                }, 0);

                                reply({ bookings: bookings, due: due, paid: paid });
                                _context.next = 19;
                                break;

                            case 16:
                                _context.prev = 16;
                                _context.t0 = _context['catch'](0);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 19:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[0, 16]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.dueResponse
    }
};

var getPaymentScreen = {
    method: 'GET',
    path: '/user/make-payment',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User gets served the razor pay screen from here.',
        notes: 'This should be authenticated.',
        validate: {
            query: {
                link: _joi2.default.string().required(),
                name: _joi2.default.string().required(),
                email: _joi2.default.string().required(),
                phone: _joi2.default.string().required(),
                amount: _joi2.default.string().required(),
                paymentGateway: _joi2.default.string().valid(['razorpay']).required()
            }
        },
        handler: function () {
            var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(request, reply) {
                var _request$query, link, name, email, phone, amount, paymentGateway;

                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _request$query = request.query, link = _request$query.link, name = _request$query.name, email = _request$query.email, phone = _request$query.phone, amount = _request$query.amount, paymentGateway = _request$query.paymentGateway;

                                reply.view(paymentGateway, {
                                    link: link.replace(' ', '+'),
                                    name: name,
                                    email: email,
                                    phone: phone,
                                    amount: (amount - 0) * 100
                                });

                            case 2:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, undefined);
            }));

            function handler(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return handler;
        }()
    }
};

var makePayment = {
    method: 'POST',
    path: '/user/make-payment',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User payment is confirmed by our backend here.',
        notes: 'This should be authenticated.',
        validate: {
            payload: {
                id: _joi2.default.string().required(),
                paymentID: _joi2.default.string().required(),
                amount: _joi2.default.number().required(),
                paymentGateway: _joi2.default.string().valid(['razorpay']).required()
            }
        },
        handler: function () {
            var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(request, reply) {
                var _request$payload, id, paymentID, amount, paymentGateway;

                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;
                                _request$payload = request.payload, id = _request$payload.id, paymentID = _request$payload.paymentID, amount = _request$payload.amount, paymentGateway = _request$payload.paymentGateway;
                                _context3.t0 = paymentGateway;
                                _context3.next = _context3.t0 === 'razorpay' ? 5 : 18;
                                break;

                            case 5:
                                _context3.t1 = _models.UserReceipt;
                                _context3.t2 = _extends3.default;
                                _context3.t3 = {
                                    user: id,
                                    amount: amount,
                                    paymentGateway: paymentGateway,
                                    paymentID: paymentID
                                };
                                _context3.next = 10;
                                return (0, _razorpay.capturePayment)(paymentID, amount);

                            case 10:
                                _context3.t4 = _context3.sent;
                                _context3.t5 = (0, _context3.t2)(_context3.t3, _context3.t4);
                                _context3.next = 14;
                                return _context3.t1.create.call(_context3.t1, _context3.t5);

                            case 14:
                                _context3.next = 16;
                                return _models.CallRequest.update({
                                    user: id,
                                    status: _models.RequestStatus.UNBILLED
                                }, { status: _models.RequestStatus.BILLED }, { multi: true });

                            case 16:
                                reply({ message: 'OK' });
                                return _context3.abrupt('break', 19);

                            case 18:
                                reply({ message: 'INVALID PAYMENT GATEWAY' }).code(400);

                            case 19:
                                _context3.next = 24;
                                break;

                            case 21:
                                _context3.prev = 21;
                                _context3.t6 = _context3['catch'](0);

                                (0, _responses.handleError)(request, reply, _context3.t6);

                            case 24:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, undefined, [[0, 21]]);
            }));

            function handler(_x5, _x6) {
                return _ref3.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

exports.getDue = getDue;
exports.makePayment = makePayment;
exports.getPaymentScreen = getPaymentScreen;