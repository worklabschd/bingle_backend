'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.editUserAvatar = exports.editUserProfile = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _models = require('../../models');

var _responses = require('../utils/responses');

var _fs = require('../../utils/fs');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by M on 21/01/18. With ❤
 */

var editUserProfile = {
    method: 'POST',
    path: '/user/edit-profile',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User can change his profile details from here.',
        notes: 'This should be authenticated.',
        validate: {
            payload: {
                id: _joi2.default.string().required(),
                name: _joi2.default.string().optional(),
                email: _joi2.default.string().email().optional(),
                age: _joi2.default.number().optional(),
                gender: _joi2.default.string().valid(['male', 'female']).optional()
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _context.next = 3;
                                return _models.User.update({
                                    _id: request.payload.id
                                }, request.payload);

                            case 3:
                                reply({ message: 'OK' });
                                _context.next = 9;
                                break;

                            case 6:
                                _context.prev = 6;
                                _context.t0 = _context['catch'](0);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 9:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[0, 6]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

var editUserAvatar = {
    method: 'POST',
    path: '/user/edit-avatar',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User can change his profile picture from here.',
        notes: 'This should be authenticated. Avatar is a base64 encoded image.',
        payload: {
            maxBytes: 1000 * 1000 * 40 // 40MB file size limit for uploads.
        },
        validate: {
            payload: {
                id: _joi2.default.string().required(),
                avatar: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(request, reply) {
                var _request$payload, id, avatar, user;

                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                _request$payload = request.payload, id = _request$payload.id, avatar = _request$payload.avatar;
                                _context2.next = 4;
                                return _models.User.findById(id);

                            case 4:
                                user = _context2.sent;

                                if (!user.avatar) {
                                    _context2.next = 8;
                                    break;
                                }

                                _context2.next = 8;
                                return (0, _fs.deleteFile)('user', user.avatar.split('user/')[1]);

                            case 8:
                                _context2.t0 = '/' + _fs.IMAGE_FOLDER + '/user/';
                                _context2.next = 11;
                                return (0, _fs.saveImage)('user', avatar);

                            case 11:
                                _context2.t1 = _context2.sent;
                                user.avatar = _context2.t0 + _context2.t1;
                                _context2.next = 15;
                                return user.save();

                            case 15:

                                reply({ message: user.avatar });
                                _context2.next = 21;
                                break;

                            case 18:
                                _context2.prev = 18;
                                _context2.t2 = _context2['catch'](0);

                                (0, _responses.handleError)(request, reply, _context2.t2);

                            case 21:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, undefined, [[0, 18]]);
            }));

            function handler(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

exports.editUserProfile = editUserProfile;
exports.editUserAvatar = editUserAvatar;