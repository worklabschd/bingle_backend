'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.callHistory = exports.makeBooking = exports.updateLocation = undefined;

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _lodash = require('lodash');

var _ = _interopRequireWildcard(_lodash);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _meegoEmitter = require('../../events/meegoEmitter');

var _meegoEmitter2 = _interopRequireDefault(_meegoEmitter);

var _constants = require('../../events/constants');

var _constants2 = _interopRequireDefault(_constants);

var _random = require('../../utils/random');

var _models = require('../../models');

var _responses = require('../utils/responses');

var _callCentre = require('../../api/callCentre');

var _callCentre2 = _interopRequireDefault(_callCentre);

var _distanceMatrix = require('../../api/distanceMatrix');

var _distanceMatrix2 = _interopRequireDefault(_distanceMatrix);

var _unQueueGuide = require('../../queues/unQueueGuide');

var _unQueueGuide2 = _interopRequireDefault(_unQueueGuide);

var _config = require('../../config.json');

var _time = require('../utils/time');

var _time2 = _interopRequireDefault(_time);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var updateLocation = {
    method: 'POST',
    path: '/user/update-location',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'Using this endpoint, a user updates his current location (if he is logged in) after he opens the app and gets nearby places in return.',
        notes: 'This should not be authenticated.',
        validate: {
            payload: {
                id: _joi2.default.string().optional(),
                latitude: _joi2.default.number().required(),
                longitude: _joi2.default.number().required()
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var _request$payload, id, latitude, longitude, places;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _request$payload = request.payload, id = _request$payload.id, latitude = _request$payload.latitude, longitude = _request$payload.longitude;
                                _context.prev = 1;

                                if (!id) {
                                    _context.next = 5;
                                    break;
                                }

                                _context.next = 5;
                                return _models.User.update({ _id: id }, { location: [longitude, latitude] });

                            case 5:
                                _context.next = 7;
                                return _models.NearbyPlace.find({
                                    location: {
                                        $nearSphere: {
                                            $geometry: {
                                                type: 'Point',
                                                coordinates: [longitude, latitude]
                                            },
                                            $maxDistance: _config.geoConfig.NEARBY_PLACE_RADIUS
                                        }
                                    }
                                }).limit(30);

                            case 7:
                                places = _context.sent;

                                if (places.length) {
                                    _context.next = 11;
                                    break;
                                }

                                reply(_.mapValues(_.mapKeys(_models.NearbyPlaceType, function (placeType) {
                                    return placeType.toLowerCase();
                                }), function () {
                                    return [];
                                }));
                                return _context.abrupt('return');

                            case 11:
                                _context.next = 13;
                                return (0, _distanceMatrix2.default)(latitude, longitude, places);

                            case 13:
                                places = _context.sent;


                                places = _.groupBy(places.map(function (place) {
                                    return {
                                        name: place.name,
                                        description: place.description,
                                        image: place.image,
                                        type: place.type,
                                        distance: place.distance,
                                        duration: place.duration,
                                        latitude: place.location[1],
                                        longitude: place.location[0]
                                    };
                                }), function (place) {
                                    return place.type;
                                });

                                // We put in place an empty array if a
                                // type has no places associated with it.
                                // This ensures data consistency in the API.
                                (0, _values2.default)(_models.NearbyPlaceType).forEach(function (type) {
                                    if (!places[type]) {
                                        places[type] = [];
                                    }
                                });

                                // TODO add fare.

                                reply(places);
                                _context.next = 22;
                                break;

                            case 19:
                                _context.prev = 19;
                                _context.t0 = _context['catch'](1);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 22:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[1, 19]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.nearbyAppResponse
    }
}; /**
    * Created by M on 05/01/18. With ❤
    */

var makeBooking = {
    method: 'GET',
    path: '/user/call-booking',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'Endpoint responsible for call bookings generated by the customer app.',
        notes: 'This should be authenticated.',
        validate: {
            query: {
                id: _joi2.default.string().required(),
                latitude: _joi2.default.number().required(),
                longitude: _joi2.default.number().required(),
                phone: _joi2.default.string().required(),
                language: _joi2.default.string().valid((0, _values2.default)(_models.Language)).required()
            }
        },
        handler: function () {
            var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(request, reply) {
                var _request$query, id, latitude, longitude, phone, language, unbilledRequests, unbilled, guides, guideIDs, requestToken, callRequest, selectedGuideIDs, user, CALL_REQUEST_EVENT;

                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _request$query = request.query, id = _request$query.id, latitude = _request$query.latitude, longitude = _request$query.longitude, phone = _request$query.phone, language = _request$query.language;

                                // Check if there's any unbilled balance for this user.

                                _context3.next = 3;
                                return _models.CallRequest.find({
                                    user: id,
                                    status: _models.RequestStatus.UNBILLED
                                });

                            case 3:
                                unbilledRequests = _context3.sent;

                                if (!unbilledRequests.length) {
                                    _context3.next = 8;
                                    break;
                                }

                                unbilled = unbilledRequests.reduce(function (sum, _ref3) {
                                    var fare = _ref3.fare;
                                    return sum + (fare - 0);
                                }, 0);

                                reply({ message: 'BALANCE PENDING|' + unbilled });
                                return _context3.abrupt('return');

                            case 8:
                                _context3.prev = 8;
                                _context3.next = 11;
                                return _models.Guide.find({
                                    languages: language,
                                    onDuty: true,
                                    onQueue: false,
                                    location: {
                                        $nearSphere: {
                                            $geometry: {
                                                type: 'Point',
                                                coordinates: [longitude, latitude]
                                            },
                                            $maxDistance: _config.geoConfig.GUIDE_CALL_BOOKING_RADIUS
                                        }
                                    }
                                }).select('_id').limit(50);

                            case 11:
                                guides = _context3.sent;

                                if (guides.length) {
                                    _context3.next = 15;
                                    break;
                                }

                                reply({ message: 'NO GUIDE AVAILABLE' });
                                return _context3.abrupt('return');

                            case 15:

                                // Generate guideIDs and a unique call request token.
                                guideIDs = guides.map(function (guide) {
                                    return guide.id;
                                });
                                _context3.next = 18;
                                return (0, _random.randCallRequestToken)();

                            case 18:
                                requestToken = _context3.sent;
                                _context3.next = 21;
                                return _models.CallRequest.create({
                                    user: id,
                                    associatedGuides: guideIDs,
                                    location: [longitude, latitude],
                                    callCentreToken: requestToken,
                                    phone: phone,
                                    language: language,
                                    rate: _config.callConfig.RATE + '/' + _config.callConfig.TIME_UNIT_MINS,
                                    guideRate: _config.callConfig.GUIDE_RATE + '/' + _config.callConfig.TIME_UNIT_MINS,
                                    status: _models.RequestStatus.CREATED
                                });

                            case 21:
                                callRequest = _context3.sent;


                                // Choose max of 5 guideIDs from the above guideID list.
                                selectedGuideIDs = (0, _random.selectRandElements)(guideIDs, 5);

                                // Get user and fare details (TODO Fare later).

                                _context3.next = 25;
                                return _models.User.findById(id).select('name');

                            case 25:
                                user = _context3.sent;
                                CALL_REQUEST_EVENT = _constants2.default.CALL_REQUEST_EVENT;

                                // Send booking to these 5 guides and wait for the first one to accept.
                                // This request times out if no guides responded within the alloted time.

                                _meegoEmitter2.default.emitToGuidesAndRegisterOnce(CALL_REQUEST_EVENT, requestToken, selectedGuideIDs, {
                                    name: user.name,
                                    fare: _config.callConfig.GUIDE_RATE + '/' + _config.callConfig.TIME_UNIT_MINS + 'mins',
                                    language: _.upperFirst(language)
                                }, _config.callConfig.TIMEOUT * 1000, function () {
                                    var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(payload) {
                                        var guideID, guidePhone, guide;
                                        return _regenerator2.default.wrap(function _callee2$(_context2) {
                                            while (1) {
                                                switch (_context2.prev = _context2.next) {
                                                    case 0:
                                                        if (payload) {
                                                            _context2.next = 3;
                                                            break;
                                                        }

                                                        // No guide responded in the alloted time.
                                                        reply({ message: 'NO GUIDE AVAILABLE' });
                                                        return _context2.abrupt('return');

                                                    case 3:
                                                        _context2.prev = 3;
                                                        guideID = payload.guideID, guidePhone = payload.guidePhone;

                                                        // Save the accepted state.

                                                        _context2.next = 7;
                                                        return callRequest.acceptRequest(guideID);

                                                    case 7:
                                                        _context2.next = 9;
                                                        return (0, _callCentre2.default)(guidePhone, phone, requestToken);

                                                    case 9:
                                                        guide = guides.find(function (g) {
                                                            return g.id === guideID;
                                                        });

                                                        // Set queued state for this guide so he doesn't get a booking.

                                                        _context2.next = 12;
                                                        return guide.queueGuide();

                                                    case 12:

                                                        // Un queue this guide so he may get a booking after 2 hours.
                                                        _unQueueGuide2.default.queue(guideID);

                                                        reply({ message: 'OK' });
                                                        _context2.next = 19;
                                                        break;

                                                    case 16:
                                                        _context2.prev = 16;
                                                        _context2.t0 = _context2['catch'](3);

                                                        (0, _responses.handleError)(request, reply, _context2.t0);

                                                    case 19:
                                                    case 'end':
                                                        return _context2.stop();
                                                }
                                            }
                                        }, _callee2, undefined, [[3, 16]]);
                                    }));

                                    return function (_x5) {
                                        return _ref4.apply(this, arguments);
                                    };
                                }());
                                _context3.next = 33;
                                break;

                            case 30:
                                _context3.prev = 30;
                                _context3.t0 = _context3['catch'](8);

                                (0, _responses.handleError)(request, reply, _context3.t0);

                            case 33:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, undefined, [[8, 30]]);
            }));

            function handler(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

var callHistory = {
    method: 'GET',
    path: '/user/call-history',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'Using this endpoint, a user gets all his completed call bookings with the status of UNBILLED OR BILLED.',
        notes: 'Offset starts from 0. Incrementing the offset will get the next batch of 20 bookings.',
        validate: {
            query: {
                id: _joi2.default.string().required(),
                offset: _joi2.default.number().required()
            }
        },
        handler: function () {
            var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(request, reply) {
                var _request$query2, id, offset, callRequests;

                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _request$query2 = request.query, id = _request$query2.id, offset = _request$query2.offset;
                                _context4.prev = 1;
                                _context4.next = 4;
                                return _models.CallRequest.find({
                                    user: id,
                                    $or: [{ status: _models.RequestStatus.UNBILLED }, { status: _models.RequestStatus.BILLED }]
                                }).sort('-createdAt').skip(offset * 20).limit(20).populate('guide');

                            case 4:
                                callRequests = _context4.sent;


                                callRequests = _.map(callRequests, function (callRequest) {
                                    var fare = callRequest.fare,
                                        duration = callRequest.duration,
                                        createdAt = callRequest.createdAt,
                                        _callRequest$guide = callRequest.guide,
                                        name = _callRequest$guide.name,
                                        avatar = _callRequest$guide.avatar,
                                        language = callRequest.language,
                                        location = callRequest.location;

                                    var _location = (0, _slicedToArray3.default)(location, 2),
                                        longitude = _location[0],
                                        latitude = _location[1];

                                    var createdAtMoment = (0, _time2.default)(createdAt);
                                    var durationMoment = (0, _moment2.default)().startOf('day').seconds(duration);
                                    return {
                                        id: callRequest.id,
                                        status: callRequest.status,
                                        fare: 'Rs ' + fare + '/-',
                                        duration: durationMoment.minutes() + ' min(s) ' + durationMoment.seconds() + ' second(s)',
                                        date: createdAtMoment.format('D MMM'),
                                        year: createdAtMoment.format('YYYY'),
                                        time: createdAtMoment.format('HH:mm'),
                                        guide: name,
                                        avatar: avatar,
                                        language: language,
                                        startLocation: 'Not Available',
                                        endLocation: 'Not Available',
                                        latitude: latitude,
                                        longitude: longitude
                                    };
                                });
                                reply(callRequests);
                                _context4.next = 12;
                                break;

                            case 9:
                                _context4.prev = 9;
                                _context4.t0 = _context4['catch'](1);

                                (0, _responses.handleError)(request, reply, _context4.t0);

                            case 12:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, undefined, [[1, 9]]);
            }));

            function handler(_x6, _x7) {
                return _ref5.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.customerCallHistoryResponse
    }
};

exports.updateLocation = updateLocation;
exports.makeBooking = makeBooking;
exports.callHistory = callHistory;