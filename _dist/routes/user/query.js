'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _models = require('../../models');

var _responses = require('../utils/responses');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var receiveUserQuery = {
    method: 'POST',
    path: '/user/query',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User submits a support query here.',
        notes: 'This should be authenticated.',
        validate: {
            payload: {
                id: _joi2.default.string().required(),
                name: _joi2.default.string().required(),
                phone: _joi2.default.string().required(),
                email: _joi2.default.string().email().required(),
                body: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var _request$payload, id, name, phone, email, body;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _request$payload = request.payload, id = _request$payload.id, name = _request$payload.name, phone = _request$payload.phone, email = _request$payload.email, body = _request$payload.body;
                                _context.next = 4;
                                return _models.Query.create({
                                    user: id,
                                    type: _models.QueryType.USER_QUERY,
                                    name: name,
                                    phone: phone,
                                    email: email,
                                    body: body
                                });

                            case 4:
                                reply({ message: 'OK' });
                                _context.next = 10;
                                break;

                            case 7:
                                _context.prev = 7;
                                _context.t0 = _context['catch'](0);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 10:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[0, 7]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
}; /**
    * Created by M on 12/01/18. With ❤
    */

exports.default = receiveUserQuery;