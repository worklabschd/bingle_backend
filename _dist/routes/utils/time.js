'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TIME_ZONE = '+05:30';
var localMoment = function localMoment(date) {
  return (0, _moment2.default)(date).utcOffset(TIME_ZONE);
};

exports.default = localMoment;