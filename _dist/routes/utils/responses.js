'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.handleError = exports.statsResponse = exports.newPartnerResponse = exports.newGuidesResponse = exports.usersResponse = exports.guidesResponse = exports.queriesResponse = exports.nearbyResponse = exports.nearbyAddResponse = exports.earningsResponse = exports.guideCallHistoryResponse = exports.dutyResponse = exports.guideResponse = exports.dueResponse = exports.customerCallHistoryResponse = exports.nearbyAppResponse = exports.socialResponse = exports.otpResponse = exports.userResponse = exports.generalResponse = undefined;

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _lodash = require('lodash');

var _ = _interopRequireWildcard(_lodash);

var _models = require('../../models');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// General reponses.
var general = _joi2.default.object({
    statusCode: _joi2.default.number().optional(),
    message: _joi2.default.string().required()
}); // Contains response models for Hapi.js endpoints.

var error = general.keys({
    error: _joi2.default.string().optional()
});

var response = function response(successful) {
    return { status: { 200: successful, 400: error, 500: error } };
};

var generalResponse = response(general);

// User responses.
var otp = general.keys({
    status: _joi2.default.string().valid(['OTP_VALID', 'OTP_INVALID', 'MOBILE_NOT_FOUND', 'INVALID_MOBILE', 'OTP_EXPIRED']).required()
});
var user = _joi2.default.object({
    id: _joi2.default.string().required(),
    name: _joi2.default.string().required(),
    countryCode: _joi2.default.string().required(),
    phone: _joi2.default.string().required(),
    age: _joi2.default.number().optional(),
    gender: _joi2.default.string().valid(['male', 'female', 'na']),
    avatar: _joi2.default.string().optional(),
    email: _joi2.default.string().email().required(),
    latitude: _joi2.default.number().optional(),
    longitude: _joi2.default.number().optional()
});
var social = _joi2.default.object({
    id: _joi2.default.string().optional(),
    name: _joi2.default.string().required(),
    email: _joi2.default.string().email().required(),
    countryCode: _joi2.default.string().optional(),
    phone: _joi2.default.string().optional(),
    age: _joi2.default.optional(),
    gender: _joi2.default.string().optional().valid(['male', 'female', 'na']),
    avatar: _joi2.default.string().optional(),
    status: _joi2.default.string().valid(['EXISTING', 'NEW']).required()
});
var nearbyPlace = _joi2.default.object({
    id: _joi2.default.string().optional(),
    name: _joi2.default.string().required(),
    description: _joi2.default.string().required(),
    image: _joi2.default.string().required(),
    type: _joi2.default.string().required(),
    latitude: _joi2.default.number().required(),
    longitude: _joi2.default.number().required(),
    distance: _joi2.default.string().optional(),
    duration: _joi2.default.string().optional()
});
var nearbyPlaces = _joi2.default.array().items(nearbyPlace);
var nearbyPlacesForApp = _joi2.default.object(_.mapValues(_.mapKeys(_models.NearbyPlaceType, function (placeType) {
    return placeType.toLowerCase();
}), function () {
    return nearbyPlaces;
}));
var callHistory = _joi2.default.object({
    id: _joi2.default.string().required(),
    status: _joi2.default.string().required(),
    fare: _joi2.default.string().required(),
    duration: _joi2.default.string().required(),
    date: _joi2.default.string().required(),
    year: _joi2.default.string().required(),
    time: _joi2.default.string().required(),
    language: _joi2.default.string().valid((0, _values2.default)(_models.Language)).required(),
    startLocation: _joi2.default.string().required(),
    endLocation: _joi2.default.string().required(),
    latitude: _joi2.default.number().required(),
    longitude: _joi2.default.number().required()
});
var customerCallHistory = _joi2.default.array().items(callHistory.keys({
    guide: _joi2.default.string().required(),
    avatar: _joi2.default.string().optional()
}));
var due = _joi2.default.object({
    bookings: _joi2.default.number().required(),
    due: _joi2.default.number().required(),
    paid: _joi2.default.number().required()
});
var userResponse = response(user);
var otpResponse = response(otp);
var socialResponse = response(social);
var nearbyAppResponse = response(nearbyPlacesForApp);
var customerCallHistoryResponse = response(customerCallHistory);
var dueResponse = response(due);

// Guide responses.
var guide = _joi2.default.object({
    id: _joi2.default.string().required(),
    name: _joi2.default.string().required(),
    address: _joi2.default.string(),
    state: _joi2.default.string(),
    pinCode: _joi2.default.string(),
    countryCode: _joi2.default.string().required(),
    phone: _joi2.default.string().required(),
    email: _joi2.default.string().required(),
    languages: _joi2.default.array().items(_joi2.default.string().valid((0, _values2.default)(_models.Language))).required(),
    license: _joi2.default.string().optional(),
    avatar: _joi2.default.string().optional(),
    accountNumber: _joi2.default.string().optional(),
    ifsc: _joi2.default.string().optional(),
    licenseExpiry: _joi2.default.string().optional(),
    dob: _joi2.default.string().optional(),
    activatedOn: _joi2.default.string().optional(),
    lastActivity: _joi2.default.string().optional(),
    latitude: _joi2.default.number().optional(),
    longitude: _joi2.default.number().optional()
});
var duty = _joi2.default.object({
    onDuty: _joi2.default.boolean().required()
});
var guideCallHistory = _joi2.default.array().items(callHistory.keys({
    user: _joi2.default.string().required()
}));
var earnings = _joi2.default.object({
    earnings: _joi2.default.string().required(),
    received: _joi2.default.string().required(),
    pending: _joi2.default.string().required(),
    duration: _joi2.default.string().required(),
    duty: _joi2.default.string().required(),
    durationPercent: _joi2.default.string().required(),
    dutyPercent: _joi2.default.string().required(),
    yesterdaysDuration: _joi2.default.string().required(),
    yesterdaysDuty: _joi2.default.string().required()
});
var guideResponse = response(guide);
var dutyResponse = response(duty);
var guideCallHistoryResponse = response(guideCallHistory);
var earningsResponse = response(earnings);

// Portal responses.
var queries = _joi2.default.array().items(_joi2.default.object({
    id: _joi2.default.string().required(),
    guide: _joi2.default.alternatives().try(_joi2.default.object({
        name: _joi2.default.string().required(),
        phone: _joi2.default.string().required(),
        email: _joi2.default.string().required()
    }), null).optional(),
    user: _joi2.default.alternatives().try(_joi2.default.object({
        name: _joi2.default.string().required(),
        phone: _joi2.default.string().required(),
        email: _joi2.default.string().required()
    }), null).optional(),
    type: _joi2.default.string().valid((0, _values2.default)(_models.QueryType)).required(),
    status: _joi2.default.string().valid((0, _values2.default)(_models.QueryStatus)).required(),
    body: _joi2.default.string().required(),
    phone: _joi2.default.string().required(),
    email: _joi2.default.string().required(),
    name: _joi2.default.string().required(),
    createdAt: _joi2.default.string().required()
}));
var guides = _joi2.default.array().items(guide);
var users = _joi2.default.array().items(user);
var newGuide = _joi2.default.object({
    id: _joi2.default.string().required(),
    name: _joi2.default.string().required(),
    countryCode: _joi2.default.string().required(),
    phone: _joi2.default.string().required(),
    email: _joi2.default.string().required(),
    status: _joi2.default.boolean().required(),
    rejected: _joi2.default.boolean().required(),
    registredWhen: _joi2.default.string().required(),
    activatedWhen: _joi2.default.string().optional(),
    rejectedWhen: _joi2.default.string().optional()
});
var newGuides = _joi2.default.array().items(newGuide);

var newPartner = _joi2.default.object({
    id: _joi2.default.string().required(),
    name: _joi2.default.string().required(),
    countryCode: _joi2.default.string().required(),
    phone: _joi2.default.string().required(),
    email: _joi2.default.string().required(),
    address: _joi2.default.string().optional(),
    state: _joi2.default.string().optional(),
    pinCode: _joi2.default.string().optional()
});
var newPartners = _joi2.default.array().items(newPartner);

var stats = _joi2.default.object({
    numberOfGuides: _joi2.default.number().required(),
    guidesOnDuty: _joi2.default.number().required(),
    callOrders: _joi2.default.number().required(),
    callOrdersDelivered: _joi2.default.number().required(),
    moneyReceived: _joi2.default.number().required(),
    appOpens: _joi2.default.number().required()
});
var nearbyAddResponse = response(nearbyPlace);
var nearbyResponse = response(nearbyPlaces);
var queriesResponse = response(queries);
var guidesResponse = response(guides);
var usersResponse = response(users);
var newGuidesResponse = response(newGuides);
var newPartnerResponse = response(newPartners);
var statsResponse = response(stats);

var handleError = function handleError(request, reply, err) {
    request.log(['error'], err);
    reply({ message: 'INTERNAL SERVER ERROR' }).code(500);
};

exports.generalResponse = generalResponse;
exports.userResponse = userResponse;
exports.otpResponse = otpResponse;
exports.socialResponse = socialResponse;
exports.nearbyAppResponse = nearbyAppResponse;
exports.customerCallHistoryResponse = customerCallHistoryResponse;
exports.dueResponse = dueResponse;
exports.guideResponse = guideResponse;
exports.dutyResponse = dutyResponse;
exports.guideCallHistoryResponse = guideCallHistoryResponse;
exports.earningsResponse = earningsResponse;
exports.nearbyAddResponse = nearbyAddResponse;
exports.nearbyResponse = nearbyResponse;
exports.queriesResponse = queriesResponse;
exports.guidesResponse = guidesResponse;
exports.usersResponse = usersResponse;
exports.newGuidesResponse = newGuidesResponse;
exports.newPartnerResponse = newPartnerResponse;
exports.statsResponse = statsResponse;
exports.handleError = handleError;