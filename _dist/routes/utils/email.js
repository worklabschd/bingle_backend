'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.sendQueryReply = exports.sendForgotPasswordEmail = undefined;

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _config = require('../../config.json');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ADDRESS = _config.smtp.ADDRESS,
    HOST = _config.smtp.HOST,
    USERNAME = _config.smtp.USERNAME,
    PASSWORD = _config.smtp.PASSWORD,
    PORT = _config.smtp.PORT,
    EMAIL = _config.smtp.EMAIL;


var transporter = _nodemailer2.default.createTransport({
    host: HOST,
    port: PORT,
    secure: true,
    auth: {
        user: USERNAME,
        pass: PASSWORD
    }
});

var send = function send(to, subject, html) {
    return new _promise2.default(function (resolve, reject) {
        transporter.sendMail({
            from: EMAIL,
            to: to,
            subject: subject,
            html: html
        }, function (error, info) {
            if (error) {
                reject(error);
                return;
            }
            resolve(info.messageId);
        });
    });
};

var sendForgotPasswordEmail = function sendForgotPasswordEmail(email, resetToken) {
    return send(email, 'Forgot your password?', '\n      Looks like you\'ve requested for a new password. Click <a href="' + ADDRESS + '/user/reset-password?token=' + resetToken + '">here</a> to continue.\n      <br />\n      This link shall remain active for the next <b>one hour</b>.\n      <br />\n      Ignore this mail if this wasn\'t you.\n    ');
};

var sendQueryReply = function sendQueryReply(email, reply, query) {
    return send(email, 'Meego Support', '\n    ' + reply + '\n    <br />\n    <br />\n    Your query raised was: ' + query + '\n  ');
};

exports.sendForgotPasswordEmail = sendForgotPasswordEmail;
exports.sendQueryReply = sendQueryReply; // eslint-disable-line