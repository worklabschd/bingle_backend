'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.login = exports.register = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _models = require('../../models');

var _responses = require('../utils/responses');

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// not ../../models/Partner okay? will look in index.js for ../../models if you dont specify a file. is it okay now ? yes. Let me run it
var login = {
    method: 'POST',
    path: '/partner/login',
    config: {
        tags: ['partner', 'api'],
        description: 'Partner logs in into the partner dashboard using this endpoint.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                email: _joi2.default.string().required(),
                password: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var _request$payload, email, password, partner;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _request$payload = request.payload, email = _request$payload.email, password = _request$payload.password;
                                _context.next = 4;
                                return _models.Partner.findOne({ email: email });

                            case 4:
                                partner = _context.sent;

                                if (!partner) {
                                    reply({ message: 'INVALID EMAIL' }).code(400);
                                } else if (partner.comparePassword(password)) {
                                    console.log("Successful");
                                    reply({
                                        id: partner.id,
                                        name: partner.name,
                                        address: partner.address,
                                        state: partner.state,
                                        pinCode: partner.pinCode,
                                        email: partner.email,
                                        countryCode: partner.countryCode,
                                        phone: partner.phone,
                                        license: partner.license
                                    });
                                } else {
                                    reply({ message: 'INVALID PASSWORD' }).code(400);
                                }
                                _context.next = 11;
                                break;

                            case 8:
                                _context.prev = 8;
                                _context.t0 = _context['catch'](0);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 11:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[0, 8]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

// const validateEmail = {
//     method: 'GET',
//     path: '/partner/validateEmail',
//     config: {
//         tags: ['guide', 'api'],
//         description: 'This validates the email of the tour guide for uniqueness',
//         notes: 'This should be unauthenticated.',
//         validate: {
//             query: {
//                 email: Joi.string().required(),
//             }
//         },
//         handler: async (request, reply) => {
//             try {
//                 const { email } = request.query;
//                 const user = await Guide.findOne({ email });
//                 if (!user) {
//                     reply({ message: 'OK' });
//                 } else {
//                     reply({ message: 'INVALID' }).code(400);
//                 }
//             } catch (err) {
//                 handleError(request, reply, err);
//             }
//         },
//         response: generalResponse
//     }
// };

var register = {
    method: 'POST',
    path: '/partner/register',
    config: {
        tags: ['partner', 'api'],
        description: 'Partner registers on the Portal using this endpoint.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                name: _joi2.default.string().required(),
                countryCode: _joi2.default.string().required(),
                phone: _joi2.default.string().required(),
                email: _joi2.default.string().email().required(),
                password: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(request, reply) {
                var _request$payload2, email, name, countryCode, phone, password, oldRegisteration, partner, hashed, newPartner;

                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                _request$payload2 = request.payload, email = _request$payload2.email, name = _request$payload2.name, countryCode = _request$payload2.countryCode, phone = _request$payload2.phone, password = _request$payload2.password;

                                // Checks if the tour guide has already registered in our system.
                                // By checking email || (countryCode && phone)

                                _context2.next = 4;
                                return _models.Partner.findOne({
                                    $or: [{ email: email }],
                                    status: true
                                });

                            case 4:
                                oldRegisteration = _context2.sent;

                                if (!oldRegisteration) {
                                    _context2.next = 8;
                                    break;
                                }

                                reply({ message: 'ALREADY REGISTERED' }).code(400);
                                return _context2.abrupt('return');

                            case 8:
                                _context2.next = 10;
                                return _models.Partner.findOne({
                                    $or: [{ email: email }]
                                });

                            case 10:
                                partner = _context2.sent;

                                if (!partner) {
                                    _context2.next = 14;
                                    break;
                                }

                                reply({ message: 'ACCOUNT EXISTS' }).code(400);
                                return _context2.abrupt('return');

                            case 14:
                                // Add a new registration.
                                hashed = _bcrypt2.default.hashSync(password, 10);
                                _context2.next = 17;
                                return _models.Partner.create({
                                    name: name,
                                    phone: phone,
                                    countryCode: countryCode,
                                    email: email,
                                    password: hashed
                                });

                            case 17:
                                newPartner = _context2.sent;


                                // Let's activate the partner, for now. (Since we don't have the portal yet)
                                //const partnerID = await newPartner.activate();

                                reply({ message: "Account Created Successfully" });
                                _context2.next = 24;
                                break;

                            case 21:
                                _context2.prev = 21;
                                _context2.t0 = _context2['catch'](0);

                                (0, _responses.handleError)(request, reply, _context2.t0);

                            case 24:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, undefined, [[0, 21]]);
            }));

            function handler(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

exports.register = register;
exports.login = login;