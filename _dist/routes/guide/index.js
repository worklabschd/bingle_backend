'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _login = require('./login');

var loginRoutes = _interopRequireWildcard(_login);

var _booking = require('./booking');

var bookingRoutes = _interopRequireWildcard(_booking);

var _query = require('./query');

var _query2 = _interopRequireDefault(_query);

var _profile = require('./profile');

var _profile2 = _interopRequireDefault(_profile);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by M on 12/10/17. With ❤
 */

exports.default = (0, _values2.default)(loginRoutes).concat((0, _values2.default)(bookingRoutes)).concat([_query2.default, _profile2.default]);