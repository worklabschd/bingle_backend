'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.sendOTPHandler = exports.register = exports.validateEmail = exports.login = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _models = require('../../models');

var _responses = require('../utils/responses');

var _otp = require('../../api/otp');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var login = {
    method: 'POST',
    path: '/guide/login',
    config: {
        tags: ['guide', 'api'],
        description: 'Tour guide logs in inside the app using this endpoint.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                countryCode: _joi2.default.string().required(),
                phone: _joi2.default.string().required(),
                password: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var _request$payload, countryCode, phone, password, guide;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _request$payload = request.payload, countryCode = _request$payload.countryCode, phone = _request$payload.phone, password = _request$payload.password;
                                _context.next = 4;
                                return _models.Guide.findOne({ countryCode: countryCode, phone: phone });

                            case 4:
                                guide = _context.sent;

                                if (!guide) {
                                    reply({ message: 'INVALID PHONE NUMBER' }).code(400);
                                } else if (guide.comparePassword(password)) {
                                    reply({
                                        id: guide.id,
                                        name: guide.name,
                                        address: guide.address,
                                        state: guide.state,
                                        pinCode: guide.pinCode,
                                        email: guide.email,
                                        countryCode: guide.countryCode,
                                        phone: guide.phone,
                                        languages: guide.languages,
                                        license: guide.license,
                                        avatar: guide.avatar,
                                        accountNumber: guide.accountNumber,
                                        ifsc: guide.ifsc,
                                        licenseExpiry: guide.licenseExpiry ? (0, _moment2.default)(guide.licenseExpiry).format('DD-MM-YYYY') : undefined,
                                        dob: guide.dob ? (0, _moment2.default)(guide.dob).format('DD-MM-YYYY') : undefined
                                    });
                                } else {
                                    reply({ message: 'INVALID PASSWORD' }).code(400);
                                }
                                _context.next = 11;
                                break;

                            case 8:
                                _context.prev = 8;
                                _context.t0 = _context['catch'](0);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 11:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[0, 8]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.guideResponse
    }
};

var validateEmail = {
    method: 'GET',
    path: '/guide/validateEmail',
    config: {
        tags: ['guide', 'api'],
        description: 'This validates the email of the tour guide for uniqueness',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                email: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(request, reply) {
                var email, user;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                email = request.query.email;
                                _context2.next = 4;
                                return _models.Guide.findOne({ email: email });

                            case 4:
                                user = _context2.sent;

                                if (!user) {
                                    reply({ message: 'OK' });
                                } else {
                                    reply({ message: 'INVALID' }).code(400);
                                }
                                _context2.next = 11;
                                break;

                            case 8:
                                _context2.prev = 8;
                                _context2.t0 = _context2['catch'](0);

                                (0, _responses.handleError)(request, reply, _context2.t0);

                            case 11:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, undefined, [[0, 8]]);
            }));

            function handler(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

var register = {
    method: 'POST',
    path: '/guide/register',
    config: {
        tags: ['guide', 'api'],
        description: 'Guide registers inside the app using this endpoint. This registration is subject to verification.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                name: _joi2.default.string().required(),
                countryCode: _joi2.default.string().required(),
                phone: _joi2.default.string().required(),
                email: _joi2.default.string().email().required(),
                password: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(request, reply) {
                var _request$payload2, email, name, countryCode, phone, password, oldRegisteration, guide, newGuide, guideID;

                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;
                                _request$payload2 = request.payload, email = _request$payload2.email, name = _request$payload2.name, countryCode = _request$payload2.countryCode, phone = _request$payload2.phone, password = _request$payload2.password;

                                // Checks if the tour guide has already registered in our system.
                                // By checking email || (countryCode && phone)

                                _context3.next = 4;
                                return _models.NewGuide.findOne({
                                    $or: [{ email: email }, {
                                        $and: [{ countryCode: countryCode }, { phone: phone }]
                                    }],
                                    status: true
                                });

                            case 4:
                                oldRegisteration = _context3.sent;

                                if (!oldRegisteration) {
                                    _context3.next = 8;
                                    break;
                                }

                                reply({ message: 'ALREADY REGISTERED' }).code(400);
                                return _context3.abrupt('return');

                            case 8:
                                _context3.next = 10;
                                return _models.Guide.findOne({
                                    $or: [{ email: email }, {
                                        $and: [{ countryCode: countryCode }, { phone: phone }]
                                    }]
                                });

                            case 10:
                                guide = _context3.sent;

                                if (!guide) {
                                    _context3.next = 14;
                                    break;
                                }

                                reply({ message: 'ACCOUNT EXISTS' }).code(400);
                                return _context3.abrupt('return');

                            case 14:
                                _context3.next = 16;
                                return _models.NewGuide.create({
                                    name: name,
                                    phone: phone,
                                    countryCode: countryCode,
                                    email: email,
                                    password: password
                                });

                            case 16:
                                newGuide = _context3.sent;
                                _context3.next = 19;
                                return newGuide.activate();

                            case 19:
                                guideID = _context3.sent;


                                reply({ message: guideID });
                                _context3.next = 26;
                                break;

                            case 23:
                                _context3.prev = 23;
                                _context3.t0 = _context3['catch'](0);

                                (0, _responses.handleError)(request, reply, _context3.t0);

                            case 26:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, undefined, [[0, 23]]);
            }));

            function handler(_x5, _x6) {
                return _ref3.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

var sendOTPHandler = {
    method: 'GET',
    path: '/guide/send-otp',
    config: {
        tags: ['guide', 'api'],
        description: 'Used for sending OTP to the user for phone verification. (On the condition that the phone number is unique)',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                countryCode: _joi2.default.string().required(),
                phone: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(request, reply) {
                var _request$query, countryCode, phone, guide;

                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.prev = 0;
                                _request$query = request.query, countryCode = _request$query.countryCode, phone = _request$query.phone;
                                _context4.next = 4;
                                return _models.Guide.findOne({ countryCode: countryCode, phone: phone });

                            case 4:
                                guide = _context4.sent;

                                if (guide) {
                                    _context4.next = 8;
                                    break;
                                }

                                (0, _otp.sendOTP)(countryCode, phone, function (err) {
                                    if (err) {
                                        (0, _responses.handleError)(request, reply, err);
                                        return;
                                    }
                                    reply({ message: 'OK' });
                                });
                                return _context4.abrupt('return');

                            case 8:
                                reply({ message: 'PHONE NUMBER TAKEN' }).code(400);
                                _context4.next = 14;
                                break;

                            case 11:
                                _context4.prev = 11;
                                _context4.t0 = _context4['catch'](0);

                                (0, _responses.handleError)(request, reply, _context4.t0);

                            case 14:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, undefined, [[0, 11]]);
            }));

            function handler(_x7, _x8) {
                return _ref4.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

exports.login = login;
exports.validateEmail = validateEmail;
exports.register = register;
exports.sendOTPHandler = sendOTPHandler;