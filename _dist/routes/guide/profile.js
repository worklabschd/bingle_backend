'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _responses = require('../utils/responses');

var _models = require('../../models');

var _fs = require('../../utils/fs');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var editProfile = {
    method: 'POST',
    path: '/guide/edit-profile',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Guide app uses this endpoint to update a tour guide\'s profile. ',
        notes: 'avatar and license are both base64 encoded images and both are optional.',
        payload: {
            maxBytes: 1000 * 1000 * 40 // 40MB file size limit for uploads.
        },
        validate: {
            payload: {
                id: _joi2.default.string().required(),
                name: _joi2.default.string().required(),
                address: _joi2.default.string().required(),
                state: _joi2.default.string().required(),
                pinCode: _joi2.default.string().required(),
                countryCode: _joi2.default.string().required(),
                phone: _joi2.default.string().required(),
                email: _joi2.default.string().required(),
                languages: _joi2.default.array().items(_joi2.default.string().valid((0, _values2.default)(_models.Language))).required(),
                license: _joi2.default.alternatives().try(_joi2.default.string(), null).optional(),
                avatar: _joi2.default.alternatives().try(_joi2.default.string(), null).optional(),
                accountNumber: _joi2.default.string().required(),
                ifsc: _joi2.default.string().required(),
                licenseExpiry: _joi2.default.string().required(),
                dob: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var _request$payload, id, license, avatar, licenseExpiry, dob, guide, licenseExpiryDate, dobDate;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _request$payload = request.payload, id = _request$payload.id, license = _request$payload.license, avatar = _request$payload.avatar, licenseExpiry = _request$payload.licenseExpiry, dob = _request$payload.dob;
                                _context.next = 4;
                                return _models.Guide.findById(id);

                            case 4:
                                guide = _context.sent;

                                if (!license) {
                                    _context.next = 14;
                                    break;
                                }

                                if (!guide.license) {
                                    _context.next = 9;
                                    break;
                                }

                                _context.next = 9;
                                return (0, _fs.deleteFile)('guide_license', guide.license.split('guide_license/')[1]);

                            case 9:
                                _context.t0 = '/' + _fs.IMAGE_FOLDER + '/guide_license/';
                                _context.next = 12;
                                return (0, _fs.saveImage)('guide_license', license);

                            case 12:
                                _context.t1 = _context.sent;
                                guide.license = _context.t0 + _context.t1;

                            case 14:
                                if (!avatar) {
                                    _context.next = 23;
                                    break;
                                }

                                if (!guide.avatar) {
                                    _context.next = 18;
                                    break;
                                }

                                _context.next = 18;
                                return (0, _fs.deleteFile)('guide', guide.avatar.split('guide/')[1]);

                            case 18:
                                _context.t2 = '/' + _fs.IMAGE_FOLDER + '/guide/';
                                _context.next = 21;
                                return (0, _fs.saveImage)('guide', avatar);

                            case 21:
                                _context.t3 = _context.sent;
                                guide.avatar = _context.t2 + _context.t3;

                            case 23:
                                licenseExpiryDate = (0, _moment2.default)(licenseExpiry, 'DD-MM-YYYY').utc().startOf('day').toDate();
                                dobDate = (0, _moment2.default)(dob, 'DD-MM-YYYY').utc().startOf('day').toDate();
                                _context.next = 27;
                                return guide.update((0, _extends3.default)({}, request.payload, {
                                    avatar: guide.avatar,
                                    license: guide.license,
                                    dob: dobDate,
                                    licenseExpiry: licenseExpiryDate
                                }));

                            case 27:

                                reply({ message: guide.avatar + '|' + guide.license });
                                _context.next = 33;
                                break;

                            case 30:
                                _context.prev = 30;
                                _context.t4 = _context['catch'](0);

                                (0, _responses.handleError)(request, reply, _context.t4);

                            case 33:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[0, 30]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
}; /**
    * Created by M on 27/01/18. With ❤
    */

exports.default = editProfile;