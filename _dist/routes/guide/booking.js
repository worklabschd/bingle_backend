'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.earnings = exports.callHistory = exports.toggleDuty = exports.updateLocation = undefined;

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _lodash = require('lodash');

var _ = _interopRequireWildcard(_lodash);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _numeral = require('numeral');

var _numeral2 = _interopRequireDefault(_numeral);

var _responses = require('../utils/responses');

var _models = require('../../models');

var _time = require('../utils/time');

var _time2 = _interopRequireDefault(_time);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var updateLocation = {
    method: 'POST',
    path: '/guide/update-location',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Using this endpoint, a guide updates his current location after he opens the app. He gets his onDuty status in return.',
        notes: 'This should be authenticated.',
        validate: {
            payload: {
                id: _joi2.default.string().required(),
                latitude: _joi2.default.number().required(),
                longitude: _joi2.default.number().required()
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var _request$payload, id, latitude, longitude, guide;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _request$payload = request.payload, id = _request$payload.id, latitude = _request$payload.latitude, longitude = _request$payload.longitude;
                                _context.prev = 1;
                                _context.next = 4;
                                return _models.Guide.findByIdAndUpdate(id, { location: [longitude, latitude] });

                            case 4:
                                guide = _context.sent;

                                reply({ onDuty: guide.onDuty });
                                _context.next = 11;
                                break;

                            case 8:
                                _context.prev = 8;
                                _context.t0 = _context['catch'](1);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 11:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[1, 8]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.dutyResponse
    }
}; /**
    * Created by M on 05/01/18. With ❤
    */

var toggleDuty = {
    method: 'POST',
    path: '/guide/toggle-duty',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Using this endpoint, a guide changes his on duty status so he can go on and off duty.',
        notes: 'This should be authenticated.',
        validate: {
            payload: {
                id: _joi2.default.string().required(),
                onDuty: _joi2.default.boolean().required()
            }
        },
        handler: function () {
            var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(request, reply) {
                var _request$payload2, id, onDuty;

                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _request$payload2 = request.payload, id = _request$payload2.id, onDuty = _request$payload2.onDuty;
                                _context2.prev = 1;
                                _context2.next = 4;
                                return _models.Guide.update({ _id: id }, { onDuty: onDuty, toggledDuty: new Date() });

                            case 4:
                                reply({ message: 'OK' });
                                _context2.next = 10;
                                break;

                            case 7:
                                _context2.prev = 7;
                                _context2.t0 = _context2['catch'](1);

                                (0, _responses.handleError)(request, reply, _context2.t0);

                            case 10:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, undefined, [[1, 7]]);
            }));

            function handler(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

var callHistory = {
    method: 'GET',
    path: '/guide/call-history',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Using this endpoint, a guide gets all his completed call bookings with the status of UNBILLED OR BILLED.',
        notes: 'Offset starts from 0. Incrementing the offset will get the next batch of 20 bookings.',
        validate: {
            query: {
                id: _joi2.default.string().required(),
                offset: _joi2.default.number().required()
            }
        },
        handler: function () {
            var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(request, reply) {
                var _request$query, id, offset, callRequests;

                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _request$query = request.query, id = _request$query.id, offset = _request$query.offset;
                                _context3.prev = 1;
                                _context3.next = 4;
                                return _models.CallRequest.find({
                                    guide: id,
                                    $or: [{ status: _models.RequestStatus.UNBILLED }, { status: _models.RequestStatus.BILLED }]
                                }).sort('-createdAt').skip(offset * 20).limit(20).populate('user').populate('guideReceipt');

                            case 4:
                                callRequests = _context3.sent;


                                callRequests = _.map(callRequests, function (callRequest) {
                                    var duration = callRequest.duration,
                                        createdAt = callRequest.createdAt,
                                        name = callRequest.user.name,
                                        language = callRequest.language,
                                        location = callRequest.location,
                                        amount = callRequest.guideReceipt.amount;

                                    var _location = (0, _slicedToArray3.default)(location, 2),
                                        longitude = _location[0],
                                        latitude = _location[1];

                                    var createdAtMoment = (0, _time2.default)(createdAt);
                                    var durationMoment = (0, _moment2.default)().startOf('day').seconds(duration);
                                    return {
                                        id: callRequest.id,
                                        status: callRequest.status,
                                        fare: 'Rs ' + amount + '/-',
                                        duration: durationMoment.minutes() + ' min(s) ' + durationMoment.seconds() + ' second(s)',
                                        date: createdAtMoment.format('D MMM'),
                                        year: createdAtMoment.format('YYYY'),
                                        time: createdAtMoment.format('HH:mm'),
                                        user: name,
                                        language: language,
                                        startLocation: 'Not Available',
                                        endLocation: 'Not Available',
                                        latitude: latitude,
                                        longitude: longitude
                                    };
                                });
                                reply(callRequests);
                                _context3.next = 12;
                                break;

                            case 9:
                                _context3.prev = 9;
                                _context3.t0 = _context3['catch'](1);

                                (0, _responses.handleError)(request, reply, _context3.t0);

                            case 12:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, undefined, [[1, 9]]);
            }));

            function handler(_x5, _x6) {
                return _ref3.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.guideCallHistoryResponse
    }
};

var earnings = {
    method: 'GET',
    path: '/guide/earnings',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Using this endpoint, a guide gets all his completed call bookings with the status of UNBILLED OR BILLED.',
        notes: 'Offset starts from 0. Incrementing the offset will get the next batch of 20 bookings.',
        validate: {
            query: {
                id: _joi2.default.string().required(),
                timeSpan: _joi2.default.string().valid(['weekly', 'monthly']).required()
            }
        },
        handler: function () {
            var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(request, reply) {
                var _request$query2, id, timeSpan, today, yesterday, currentThreshold, previousThreshold, callRequests, currentDuration, previousDuration, todaysDuration, yesterdaysDuration, currentDuties, previousDuties, todaysDuties, yesterdaysDuties, _earnings, received, totalDuration, totalDuties, percentFormatter;

                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _request$query2 = request.query, id = _request$query2.id, timeSpan = _request$query2.timeSpan;
                                _context4.prev = 1;
                                today = (0, _moment2.default)().startOf('day');
                                yesterday = today.clone().subtract(1, 'days');
                                currentThreshold = today.clone();
                                previousThreshold = void 0;

                                // If the guide wants a monthly comparison.

                                if (timeSpan === 'monthly') {
                                    currentThreshold.subtract(1, 'months').add(1, 'days');
                                    previousThreshold = currentThreshold.clone().subtract(1, 'months');
                                } else {
                                    // If the guide wants a weekly comparison.
                                    currentThreshold.subtract(6, 'days');
                                    previousThreshold = currentThreshold.clone().subtract(7, 'days');
                                }

                                _context4.next = 9;
                                return _models.CallRequest.find({
                                    guide: id,
                                    $or: [{ status: _models.RequestStatus.UNBILLED }, { status: _models.RequestStatus.BILLED }]
                                }).populate('guideReceipt').select('fare duration createdAt').sort('-createdAt');

                            case 9:
                                callRequests = _context4.sent;


                                /* eslint-disable */
                                currentDuration = 0, previousDuration = 0, todaysDuration = 0, yesterdaysDuration = 0, currentDuties = 0, previousDuties = 0, todaysDuties = 0, yesterdaysDuties = 0, _earnings = 0, received = 0, totalDuration = 0, totalDuties = 0;
                                /* eslint-enable */

                                callRequests.forEach(function (callRequest) {
                                    var duration = callRequest.duration,
                                        createdAt = callRequest.createdAt,
                                        _callRequest$guideRec = callRequest.guideReceipt,
                                        amount = _callRequest$guideRec.amount,
                                        status = _callRequest$guideRec.status;

                                    var createdAtMoment = (0, _moment2.default)(createdAt);

                                    // For yesterday's comparison.
                                    if (createdAtMoment.isSameOrAfter(today)) {
                                        todaysDuration += duration - 0;
                                        todaysDuties += 1;
                                    } else if (createdAtMoment.isSameOrAfter(yesterday)) {
                                        yesterdaysDuration += duration - 0;
                                        yesterdaysDuties += 1;
                                    }

                                    // For timeSpan comparison.
                                    if (createdAtMoment.isSameOrAfter(currentThreshold)) {
                                        currentDuration += duration - 0;
                                        currentDuties += 1;
                                    } else if (createdAtMoment.isSameOrAfter(previousThreshold)) {
                                        previousDuration += duration - 0;
                                        previousDuties += 1;
                                    }

                                    // Lifetime statistics.
                                    // Duration and duties.
                                    totalDuration += duration - 0;
                                    totalDuties += 1;

                                    // To calculate guide's earnings.
                                    if (status === _models.GuideReceiptStatus.PAID) {
                                        received += amount - 0;
                                    }
                                    _earnings += amount - 0;
                                });

                                percentFormatter = function percentFormatter(current, previous) {
                                    if (!previous) {
                                        return (0, _numeral2.default)(current).format('%0a');
                                    }
                                    return (0, _numeral2.default)(current / previous - 1).format('%0a');
                                };

                                reply({
                                    earnings: 'Rs ' + (0, _numeral2.default)(_earnings).format('0,000'),
                                    received: 'Rs ' + (0, _numeral2.default)(received).format('0,000'),
                                    pending: 'Rs ' + (0, _numeral2.default)(_earnings - received).format('0,000'),
                                    duration: totalDuration ? _moment2.default.duration(currentDuration, 'seconds').humanize() : '0 seconds',
                                    duty: '' + totalDuties,
                                    durationPercent: percentFormatter(currentDuration, previousDuration),
                                    dutyPercent: percentFormatter(currentDuties, previousDuties),
                                    yesterdaysDuration: percentFormatter(todaysDuration, yesterdaysDuration),
                                    yesterdaysDuty: percentFormatter(todaysDuties, yesterdaysDuties)
                                });
                                _context4.next = 19;
                                break;

                            case 16:
                                _context4.prev = 16;
                                _context4.t0 = _context4['catch'](1);

                                (0, _responses.handleError)(request, reply, _context4.t0);

                            case 19:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, undefined, [[1, 16]]);
            }));

            function handler(_x7, _x8) {
                return _ref4.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.earningsResponse
    }
};

exports.updateLocation = updateLocation;
exports.toggleDuty = toggleDuty;
exports.callHistory = callHistory;
exports.earnings = earnings;