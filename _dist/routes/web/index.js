'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _query = require('./query');

var _query2 = _interopRequireDefault(_query);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = [_query2.default];