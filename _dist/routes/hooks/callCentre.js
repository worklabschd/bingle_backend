'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.hungUpHook = undefined;

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _responses = require('../utils/responses');

var _models = require('../../models');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// This webhook is called by prp to signal answer or hangup of a call.
// Web hooks for call centre API.

var hungUpHook = {
    method: 'GET',
    path: '/hooks/hungup',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Web hook for call centre API.',
        notes: 'After a call is hung up or answered. This end point should get called.',
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var _request$query, type, duration_out, call_status_out, call_status_in, call_in, outgoing_picked, src, call_out, callRequests, callRequest, guideReceipt;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;

                                console.log(request.query);
                                /* eslint-disable */
                                _request$query = request.query, type = _request$query.type, duration_out = _request$query.duration_out, call_status_out = _request$query.call_status_out, call_status_in = _request$query.call_status_in, call_in = _request$query.call_in, outgoing_picked = _request$query.outgoing_picked, src = _request$query.src;

                                type = type.toLowerCase();
                                call_status_out = call_status_out ? call_status_out.toLowerCase() : '';
                                call_status_in = call_status_in ? call_status_in.toLowerCase() : '';
                                call_in = call_in ? call_in.toLowerCase() : '';
                                call_out = request.query['call _out'] ? request.query['call _out'].toLowerCase() : '';

                                duration_out -= 0;
                                // Just to make the call was infact received by both the parties.

                                if (!(type === 'hangup' && (call_status_out === 'answered' && call_status_in === 'answered' || call_out === 'answered' && call_in === 'answered'))) {
                                    _context.next = 26;
                                    break;
                                }

                                _context.next = 12;
                                return _models.CallRequest.find({ phone: outgoing_picked }).populate('guide', null, { phone: src }).sort('-createdAt');

                            case 12:
                                callRequests = _context.sent;


                                console.log('callRequests', callRequests);
                                callRequests = callRequests.filter(function (_ref2) {
                                    var guide = _ref2.guide;
                                    return !!guide;
                                });

                                // Processing the latest call for this guide/user pair.
                                callRequest = callRequests[0];

                                if (!callRequest) {
                                    _context.next = 26;
                                    break;
                                }

                                // Attach a fare to this call request.
                                callRequest.fare = calculateFare(duration_out, callRequest.rate);
                                if (callRequest.fare) {
                                    callRequest.status = _models.RequestStatus.UNBILLED;
                                } else {
                                    callRequest.status = _models.RequestStatus.BILLED;
                                }
                                callRequest.duration = duration_out - 0;

                                // Generate the receipt for guide.
                                _context.next = 22;
                                return _models.GuideReceipt.create({
                                    guide: callRequest.guide.id,
                                    amount: calculateFare(duration_out, callRequest.guideRate),
                                    status: _models.GuideReceiptStatus.DUE
                                });

                            case 22:
                                guideReceipt = _context.sent;


                                // Attach the receipt to this call request.
                                callRequest.guideReceipt = guideReceipt.id;

                                // Save this call quest.
                                _context.next = 26;
                                return callRequest.save();

                            case 26:

                                /* eslint-enable */
                                reply({ message: 'OK' });
                                _context.next = 32;
                                break;

                            case 29:
                                _context.prev = 29;
                                _context.t0 = _context['catch'](0);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 32:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[0, 29]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

var calculateFare = function calculateFare(durationSecs, rate) {
    if (durationSecs) {
        var _rate$split = rate.split('/'),
            _rate$split2 = (0, _slicedToArray3.default)(_rate$split, 2),
            ratePerUnit = _rate$split2[0],
            timeUnit = _rate$split2[1];

        ratePerUnit -= 0;
        timeUnit -= 0;

        // 17 mins = 6 time units if 1 time unit = 3 mins.
        var durationAsPerTimeUnit = Math.ceil(Math.ceil(durationSecs / 60) / timeUnit);

        // Call fare = duration in time units * (rate per unit)
        return durationAsPerTimeUnit * ratePerUnit;
    }
    return 0;
};

exports.hungUpHook = hungUpHook; // eslint-disable-line