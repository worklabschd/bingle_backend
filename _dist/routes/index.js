'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.hooks = exports.partnerRoutes = exports.webRoutes = exports.portalRoutes = exports.userRoutes = exports.guideRoutes = undefined;

var _guide = require('./guide');

var _guide2 = _interopRequireDefault(_guide);

var _user = require('./user');

var _user2 = _interopRequireDefault(_user);

var _portal = require('./portal');

var _portal2 = _interopRequireDefault(_portal);

var _web = require('./web');

var _web2 = _interopRequireDefault(_web);

var _partner = require('./partner');

var _partner2 = _interopRequireDefault(_partner);

var _hooks = require('./hooks');

var _hooks2 = _interopRequireDefault(_hooks);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by M on 17/04/17. With ❤
 */

exports.guideRoutes = _guide2.default;
exports.userRoutes = _user2.default;
exports.portalRoutes = _portal2.default;
exports.webRoutes = _web2.default;
exports.partnerRoutes = _partner2.default;
exports.hooks = _hooks2.default;