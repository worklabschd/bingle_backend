'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _models = require('../../models');

var _responses = require('../utils/responses');

var _time = require('../utils/time');

var _time2 = _interopRequireDefault(_time);

var _store = require('../../store');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getStats = {
    method: 'GET',
    path: '/portal/stats',
    config: {
        tags: ['portal', 'api'],
        description: 'The admin can get vital statistics of Meego from here.',
        notes: 'Statistics include total number of guides,\n        guides on duty, total number of call orders,\n        total numer of call orders successfully completed, total money received, app opens in the last 24 hours, etc.\n        To and from are optional query params that apply to call orders, call orders received and money received and must be in the format MM-DD-YYYY.\n        If they are not given, start and end of the current day is chosen in IST.',
        validate: {
            query: {
                to: _joi2.default.date().max('now').optional(),
                from: _joi2.default.date().max(_joi2.default.ref('to')).optional()
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var _request$query, to, from, fromDate, toDate, dateFilter, numberOfGuides, guidesOnDuty, callOrders, callOrdersDelivered, userReceipts, moneyReceived, appOpens;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _request$query = request.query, to = _request$query.to, from = _request$query.from;
                                fromDate = (0, _time2.default)(from).startOf('day').toDate();
                                toDate = (0, _time2.default)(to).endOf('day').toDate();
                                dateFilter = { $gte: fromDate, $lte: toDate };
                                _context.prev = 4;
                                _context.next = 7;
                                return _models.Guide.find({}).count();

                            case 7:
                                numberOfGuides = _context.sent;
                                _context.next = 10;
                                return _models.Guide.find({ onDuty: true }).count();

                            case 10:
                                guidesOnDuty = _context.sent;
                                _context.next = 13;
                                return _models.CallRequest.find({
                                    status: {
                                        $ne: _models.RequestStatus.CREATED
                                    },
                                    createdAt: dateFilter
                                }).count();

                            case 13:
                                callOrders = _context.sent;
                                _context.next = 16;
                                return _models.CallRequest.find({
                                    $or: [{ status: _models.RequestStatus.UNBILLED }, { status: _models.RequestStatus.BILLED }],
                                    createdAt: dateFilter
                                }).count();

                            case 16:
                                callOrdersDelivered = _context.sent;
                                _context.next = 19;
                                return _models.UserReceipt.find({
                                    createdAt: dateFilter
                                }).select('amount');

                            case 19:
                                userReceipts = _context.sent;
                                moneyReceived = userReceipts.reduce(function (sum, _ref2) {
                                    var amount = _ref2.amount;
                                    return sum + (amount - 0);
                                }, 0);
                                _context.next = 23;
                                return (0, _store.getAppOpens)();

                            case 23:
                                appOpens = _context.sent;

                                reply({
                                    numberOfGuides: numberOfGuides,
                                    guidesOnDuty: guidesOnDuty,
                                    callOrders: callOrders,
                                    callOrdersDelivered: callOrdersDelivered,
                                    moneyReceived: moneyReceived,
                                    appOpens: appOpens
                                });
                                _context.next = 30;
                                break;

                            case 27:
                                _context.prev = 27;
                                _context.t0 = _context['catch'](4);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 30:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[4, 27]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.statsResponse
    }
}; /**
    * Created by M on 01/03/18. With ❤
    */

exports.default = getStats;