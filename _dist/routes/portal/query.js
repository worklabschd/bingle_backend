'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.replyToQuery = exports.changeQueryStatus = exports.getQueries = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _lodash = require('lodash');

var _ = _interopRequireWildcard(_lodash);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _models = require('../../models');

var _responses = require('../utils/responses');

var _email = require('../utils/email');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by M on 20/01/18. With ❤
 */

var getQueries = {
    method: 'GET',
    path: '/portal/get-queries',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets support queries submitted inside both the apps from here.',
        notes: 'Offset starts from 0 with a default value of 0. Limit is set to a default value of 20. Type pertains to "user" or "guide". Status can be one of "created", "not_resolved" and "resolved". Type and status are both optional.',
        validate: {
            query: {
                offset: _joi2.default.number().optional().default(0),
                limit: _joi2.default.number().optional().default(20),
                type: _joi2.default.string().valid((0, _values2.default)(_models.QueryType)).optional(),
                status: _joi2.default.string().valid((0, _values2.default)(_models.QueryStatus)).optional()
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var _request$query, offset, limit, type, status, queryObject, queries;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _request$query = request.query, offset = _request$query.offset, limit = _request$query.limit, type = _request$query.type, status = _request$query.status;
                                queryObject = {};

                                if (type) {
                                    queryObject.type = type;
                                }
                                if (status) {
                                    queryObject.status = status;
                                }
                                _context.next = 7;
                                return _models.Query.find(queryObject).sort('-createdAt').skip(offset * limit).limit(limit).populate('user').populate('guide');

                            case 7:
                                queries = _context.sent;

                                queries = _.map(queries, function (query) {
                                    return (0, _extends3.default)({}, _.omit(query.toJSON(), ['_id', '__v']), {
                                        id: query.id,
                                        user: query.user ? _.pick(query.user.toJSON(), ['name', 'email', 'phone']) : null,
                                        guide: query.guide ? _.pick(query.guide.toJSON(), ['name', 'email', 'phone']) : null,
                                        createdAt: (0, _moment2.default)(query.createdAt).format('HH:mm DD:MM:YY')
                                    });
                                });
                                reply(queries);
                                _context.next = 15;
                                break;

                            case 12:
                                _context.prev = 12;
                                _context.t0 = _context['catch'](0);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 15:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[0, 12]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.queriesResponse
    }
};

var changeQueryStatus = {
    method: 'PUT',
    path: '/portal/change-query-status',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets to change a user/guide submitted query\'s state.',
        notes: 'Status can be one of "created", "not_resolved" and "resolved". id and status are both required.',
        validate: {
            payload: {
                id: _joi2.default.string().required(),
                status: _joi2.default.string().valid((0, _values2.default)(_models.QueryStatus)).required()
            }
        },
        handler: function () {
            var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(request, reply) {
                var _request$payload, id, status;

                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                _request$payload = request.payload, id = _request$payload.id, status = _request$payload.status;
                                _context2.next = 4;
                                return _models.Query.findByIdAndUpdate(id, { status: status });

                            case 4:
                                reply({ message: 'OK' });
                                _context2.next = 10;
                                break;

                            case 7:
                                _context2.prev = 7;
                                _context2.t0 = _context2['catch'](0);

                                (0, _responses.handleError)(request, reply, _context2.t0);

                            case 10:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, undefined, [[0, 7]]);
            }));

            function handler(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

var replyToQuery = {
    method: 'POST',
    path: '/portal/reply-query',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin can reply to a query raised by either the customer or the guide from here.',
        notes: 'The reply is sent on the email provided inside the query.',
        validate: {
            payload: {
                id: _joi2.default.string().required(),
                response: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(request, reply) {
                var _request$payload2, id, response, _ref4, email, body;

                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;
                                _request$payload2 = request.payload, id = _request$payload2.id, response = _request$payload2.response;
                                _context3.next = 4;
                                return _models.Query.findById(id);

                            case 4:
                                _ref4 = _context3.sent;
                                email = _ref4.email;
                                body = _ref4.body;
                                _context3.next = 9;
                                return (0, _email.sendQueryReply)(email, response, body);

                            case 9:
                                reply({ message: 'OK' });
                                _context3.next = 15;
                                break;

                            case 12:
                                _context3.prev = 12;
                                _context3.t0 = _context3['catch'](0);

                                (0, _responses.handleError)(request, reply, _context3.t0);

                            case 15:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, undefined, [[0, 12]]);
            }));

            function handler(_x5, _x6) {
                return _ref3.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

exports.getQueries = getQueries;
exports.changeQueryStatus = changeQueryStatus;
exports.replyToQuery = replyToQuery;