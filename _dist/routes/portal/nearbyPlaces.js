'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.deleteNearby = exports.addNearby = exports.getNearby = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _ba = require('ba64');

var _ba2 = _interopRequireDefault(_ba);

var _v = require('uuid/v1');

var _v2 = _interopRequireDefault(_v);

var _models = require('../../models');

var _responses = require('../utils/responses');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by M on 06/01/18. With ❤
 */

var getNearby = {
    method: 'GET',
    path: '/portal/get-nearby',
    config: {
        tags: ['portal', 'api'],
        description: 'The admin can get list of all the nearby places in the system.',
        notes: 'All the query params are optional. Offset will default to 0 and limit to 20.',
        validate: {
            query: {
                name: _joi2.default.string().default('').optional(),
                type: _joi2.default.string().valid((0, _values2.default)(_models.NearbyPlaceType)).optional(),
                city: _joi2.default.string().optional(),
                offset: _joi2.default.number().optional().default(0),
                limit: _joi2.default.number().optional().default(20)
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var _request$query, name, type, city, offset, limit, queryObject, places;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _request$query = request.query, name = _request$query.name, type = _request$query.type, city = _request$query.city, offset = _request$query.offset, limit = _request$query.limit;
                                _context.prev = 1;
                                queryObject = {};

                                queryObject.name = new RegExp(name, 'i');
                                if (type) {
                                    queryObject.type = type;
                                }
                                if (city) {
                                    queryObject.city = new RegExp(city, 'i');
                                }
                                _context.next = 8;
                                return _models.NearbyPlace.find(queryObject).sort('name').skip(offset * limit).limit(limit);

                            case 8:
                                places = _context.sent;


                                places = places.map(function (place) {
                                    return {
                                        id: place.id,
                                        name: place.name,
                                        description: place.description,
                                        image: place.image,
                                        type: place.type,
                                        latitude: place.location[1],
                                        longitude: place.location[0]
                                    };
                                });

                                reply(places);
                                _context.next = 16;
                                break;

                            case 13:
                                _context.prev = 13;
                                _context.t0 = _context['catch'](1);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 16:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[1, 13]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.nearbyResponse
    }
};

var addNearby = {
    method: 'POST',
    path: '/portal/add-nearby',
    config: {
        tags: ['portal', 'api'],
        description: 'Used for adding nearby places from the admin portal.',
        notes: 'All the params are required. Image stands for base64 encoded image.',
        validate: {
            payload: {
                name: _joi2.default.string().required(),
                description: _joi2.default.string().required(),
                type: _joi2.default.string().valid((0, _values2.default)(_models.NearbyPlaceType)).required(),
                image: _joi2.default.string().required(),
                latitude: _joi2.default.number().min(-180).max(180).required(),
                longitude: _joi2.default.number().min(0).max(90).required()
            }
        },
        handler: function () {
            var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(request, reply) {
                var _request$payload, name, description, type, latitude, longitude, image, fileName, filePath, newPlace;

                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _request$payload = request.payload, name = _request$payload.name, description = _request$payload.description, type = _request$payload.type, latitude = _request$payload.latitude, longitude = _request$payload.longitude, image = _request$payload.image;
                                _context2.prev = 1;
                                fileName = (0, _v2.default)();
                                filePath = _path2.default.join('assets', 'images', 'nearby', fileName);

                                _ba2.default.writeImageSync(filePath, image);
                                _context2.next = 7;
                                return _models.NearbyPlace.create({
                                    name: name,
                                    description: description,
                                    image: '/images/nearby/' + fileName + '.' + _ba2.default.getExt(image),
                                    type: type,
                                    location: [longitude, latitude]
                                });

                            case 7:
                                newPlace = _context2.sent;

                                reply({
                                    id: newPlace.id,
                                    name: name,
                                    description: description,
                                    image: newPlace.image,
                                    type: type,
                                    latitude: latitude,
                                    longitude: longitude
                                });
                                _context2.next = 14;
                                break;

                            case 11:
                                _context2.prev = 11;
                                _context2.t0 = _context2['catch'](1);

                                (0, _responses.handleError)(request, reply, _context2.t0);

                            case 14:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, undefined, [[1, 11]]);
            }));

            function handler(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.nearbyAddResponse
    }
};

var deleteNearby = {
    method: 'DELETE',
    path: '/portal/delete-nearby',
    config: {
        tags: ['portal', 'api'],
        description: 'Used for deleting a nearby place from the admin portal.',
        notes: 'Only needs a place ID',
        validate: {
            payload: {
                id: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(request, reply) {
                var id;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                id = request.payload.id;
                                _context3.prev = 1;
                                _context3.next = 4;
                                return _models.NearbyPlace.remove({
                                    _id: id
                                });

                            case 4:
                                reply({ message: 'OK' });
                                _context3.next = 10;
                                break;

                            case 7:
                                _context3.prev = 7;
                                _context3.t0 = _context3['catch'](1);

                                (0, _responses.handleError)(request, reply, _context3.t0);

                            case 10:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, undefined, [[1, 7]]);
            }));

            function handler(_x5, _x6) {
                return _ref3.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

exports.getNearby = getNearby;
exports.addNearby = addNearby;
exports.deleteNearby = deleteNearby;