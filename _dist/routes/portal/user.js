'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getUser = exports.getUsers = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _lodash = require('lodash');

var _ = _interopRequireWildcard(_lodash);

var _models = require('../../models');

var _responses = require('../utils/responses');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by M on 20/01/18. With ❤
 */

var getUsers = {
    method: 'GET',
    path: '/portal/users',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets the users present in the system from here.',
        notes: 'Offset starts from 0 with a default value of 0. Limit is set to a default value of 20.',
        validate: {
            query: {
                name: _joi2.default.string().default('').optional(),
                phone: _joi2.default.string().default('').optional(),
                offset: _joi2.default.number().optional().default(0),
                limit: _joi2.default.number().optional().default(20)
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var _request$query, name, phone, offset, limit, queryObject, users;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _request$query = request.query, name = _request$query.name, phone = _request$query.phone, offset = _request$query.offset, limit = _request$query.limit;
                                queryObject = {};

                                queryObject.name = new RegExp(name, 'i');
                                queryObject.phone = new RegExp(phone, 'i');
                                _context.next = 7;
                                return _models.User.find(queryObject).sort('name').skip(offset * limit).limit(limit);

                            case 7:
                                users = _context.sent;


                                users = _.map(users, function (user) {
                                    return {
                                        id: user.id,
                                        name: user.name,
                                        email: user.email,
                                        countryCode: user.countryCode,
                                        phone: user.phone,
                                        age: user.age,
                                        gender: user.gender,
                                        avatar: user.avatar,
                                        latitude: user.location ? user.location[1] : undefined,
                                        longitude: user.location ? user.location[0] : undefined
                                    };
                                });
                                reply(users);
                                _context.next = 15;
                                break;

                            case 12:
                                _context.prev = 12;
                                _context.t0 = _context['catch'](0);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 15:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[0, 12]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.usersResponse
    }
};

var getUser = {
    method: 'GET',
    path: '/portal/user/{id}',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets everything about a user from here. Call this endpoint as GET /portal/user/(id of the user).',
        notes: 'To get bookings related to this guide use GET /user/call-history.',
        validate: {
            params: {
                id: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(request, reply) {
                var id, user;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                id = request.params.id;
                                _context2.next = 4;
                                return _models.User.findById(id);

                            case 4:
                                user = _context2.sent;


                                reply({
                                    id: user.id,
                                    name: user.name,
                                    email: user.email,
                                    countryCode: user.countryCode,
                                    phone: user.phone,
                                    age: user.age,
                                    gender: user.gender,
                                    avatar: user.avatar,
                                    latitude: user.location ? user.location[1] : undefined,
                                    longitude: user.location ? user.location[0] : undefined
                                });
                                _context2.next = 11;
                                break;

                            case 8:
                                _context2.prev = 8;
                                _context2.t0 = _context2['catch'](0);

                                (0, _responses.handleError)(request, reply, _context2.t0);

                            case 11:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, undefined, [[0, 8]]);
            }));

            function handler(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.userResponse
    }
};

exports.getUsers = getUsers;
exports.getUser = getUser;