'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _nearbyPlaces = require('./nearbyPlaces');

var nearbyRoutes = _interopRequireWildcard(_nearbyPlaces);

var _query = require('./query');

var queryRoutes = _interopRequireWildcard(_query);

var _guide = require('./guide');

var guideRoutes = _interopRequireWildcard(_guide);

var _user = require('./user');

var userRoutes = _interopRequireWildcard(_user);

var _stats = require('./stats');

var _stats2 = _interopRequireDefault(_stats);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _values2.default)(nearbyRoutes).concat((0, _values2.default)(queryRoutes)).concat((0, _values2.default)(guideRoutes)).concat((0, _values2.default)(userRoutes)).concat([_stats2.default]);