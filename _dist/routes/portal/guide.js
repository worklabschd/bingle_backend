'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.clearGuideDues = exports.getGuideDues = exports.changeNewGuideStatus = exports.getNewGuides = exports.getGuide = exports.getGuides = undefined;

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _lodash = require('lodash');

var _ = _interopRequireWildcard(_lodash);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _xlsx = require('xlsx');

var _xlsx2 = _interopRequireDefault(_xlsx);

var _models = require('../../models');

var _responses = require('../utils/responses');

var _time = require('../utils/time');

var _time2 = _interopRequireDefault(_time);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getGuides = {
    method: 'GET',
    path: '/portal/guides',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets the guides present in the system from here.',
        notes: 'Offset starts from 0 with a default value of 0. Limit is set to a default value of 20.',
        validate: {
            query: {
                name: _joi2.default.string().default('').optional(),
                phone: _joi2.default.string().default('').optional(),
                language: _joi2.default.string().valid((0, _values2.default)(_models.Language)).optional(),
                offset: _joi2.default.number().optional().default(0),
                limit: _joi2.default.number().optional().default(20)
            }
        },
        handler: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(request, reply) {
                var _request$query, name, phone, language, offset, limit, queryObject, guides;

                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                _request$query = request.query, name = _request$query.name, phone = _request$query.phone, language = _request$query.language, offset = _request$query.offset, limit = _request$query.limit;
                                queryObject = {};

                                queryObject.name = new RegExp(name, 'i');
                                queryObject.phone = new RegExp(phone, 'i');
                                if (language) {
                                    queryObject.language = language;
                                }
                                _context.next = 8;
                                return _models.Guide.find(queryObject).sort('name').skip(offset * limit).limit(limit);

                            case 8:
                                guides = _context.sent;


                                guides = _.map(guides, function (guide) {
                                    return {
                                        id: guide.id,
                                        name: guide.name,
                                        countryCode: guide.countryCode,
                                        phone: guide.phone,
                                        email: guide.email,
                                        languages: guide.languages,
                                        avatar: guide.avatar,
                                        latitude: guide.location ? guide.location[1] : undefined,
                                        longitude: guide.location ? guide.location[0] : undefined
                                    };
                                });
                                reply(guides);
                                _context.next = 16;
                                break;

                            case 13:
                                _context.prev = 13;
                                _context.t0 = _context['catch'](0);

                                (0, _responses.handleError)(request, reply, _context.t0);

                            case 16:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, undefined, [[0, 13]]);
            }));

            function handler(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.guidesResponse
    }
}; /**
    * Created by M on 20/01/18. With ❤
    */

var getGuide = {
    method: 'GET',
    path: '/portal/guide/{id}',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets everything about a guide from here. Call this endpoint as GET /portal/guide/(id of the guide).',
        notes: 'To get bookings related to this guide use GET /guide/call-history. To update this guide use POST /guide/edit-profile.',
        validate: {
            params: {
                id: _joi2.default.string().required()
            }
        },
        handler: function () {
            var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(request, reply) {
                var id, guide;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.prev = 0;
                                id = request.params.id;
                                _context2.next = 4;
                                return _models.Guide.findById(id);

                            case 4:
                                guide = _context2.sent;


                                reply({
                                    id: guide.id,
                                    name: guide.name,
                                    address: guide.address,
                                    state: guide.state,
                                    pinCode: guide.pinCode,
                                    email: guide.email,
                                    countryCode: guide.countryCode,
                                    phone: guide.phone,
                                    languages: guide.languages,
                                    license: guide.license,
                                    avatar: guide.avatar,
                                    accountNumber: guide.accountNumber,
                                    ifsc: guide.ifsc,
                                    licenseExpiry: guide.licenseExpiry ? (0, _moment2.default)(guide.licenseExpiry).format('DD-MM-YYYY') : undefined,
                                    dob: guide.dob ? (0, _moment2.default)(guide.dob).format('DD-MM-YYYY') : undefined,
                                    activatedOn: (0, _time2.default)(guide.createdAt).format('DD-MM-YYYY HH:mm'),
                                    lastActivity: guide.toggledDuty ? (0, _time2.default)(guide.toggledDuty).format('DD-MM-YYYY HH:mm') : undefined,
                                    latitude: guide.location ? guide.location[1] : undefined,
                                    longitude: guide.location ? guide.location[0] : undefined
                                });
                                _context2.next = 11;
                                break;

                            case 8:
                                _context2.prev = 8;
                                _context2.t0 = _context2['catch'](0);

                                (0, _responses.handleError)(request, reply, _context2.t0);

                            case 11:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, undefined, [[0, 8]]);
            }));

            function handler(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.guideResponse
    }
};

var getNewGuides = {
    method: 'GET',
    path: '/portal/new-guides',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets the new guide registrations that were done from the app.',
        notes: 'Offset starts from 0 with a default value of 0. Limit is set to a default value of 20.',
        validate: {
            query: {
                status: _joi2.default.boolean().optional().default(true),
                offset: _joi2.default.number().optional().default(0),
                limit: _joi2.default.number().optional().default(20)
            }
        },
        handler: function () {
            var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(request, reply) {
                var _request$query2, offset, limit, status, queryObject, newGuides;

                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.prev = 0;
                                _request$query2 = request.query, offset = _request$query2.offset, limit = _request$query2.limit, status = _request$query2.status;
                                queryObject = {};

                                if (status) {
                                    // New guide registrations.
                                    queryObject.status = false;
                                } else {
                                    // Rejected guide registrations.
                                    queryObject.rejected = true;
                                }
                                _context3.next = 6;
                                return _models.NewGuide.find(queryObject).sort('name').skip(offset * limit).limit(limit);

                            case 6:
                                newGuides = _context3.sent;


                                newGuides = _.map(newGuides, function (newGuide) {
                                    return {
                                        id: newGuide.id,
                                        name: newGuide.name,
                                        countryCode: newGuide.countryCode,
                                        phone: newGuide.phone,
                                        email: newGuide.email,
                                        status: newGuide.status,
                                        rejected: newGuide.rejected,
                                        registredWhen: (0, _time2.default)(newGuide.createdAt).format('DD-MM-YYYY HH:mm'),
                                        activatedWhen: newGuide.activatedWhen ? (0, _time2.default)(newGuide.activatedWhen).format('DD-MM-YYYY HH:mm') : undefined,
                                        rejectedWhen: newGuide.rejectedWhen ? (0, _time2.default)(newGuide.rejectedWhen).format('DD-MM-YYYY HH:mm') : undefined
                                    };
                                });

                                reply(newGuides);
                                _context3.next = 14;
                                break;

                            case 11:
                                _context3.prev = 11;
                                _context3.t0 = _context3['catch'](0);

                                (0, _responses.handleError)(request, reply, _context3.t0);

                            case 14:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, undefined, [[0, 11]]);
            }));

            function handler(_x5, _x6) {
                return _ref3.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.newGuidesResponse
    }
};

var changeNewGuideStatus = {
    method: 'PUT',
    path: '/portal/change-new-guide-status/{id}',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin approves or rejects a new guide registration. Call this endpoint as GET /portal/change-new-guide-status/(id of the new guide). ',
        notes: 'This endpoint also takes a status query param. Send true to approve and false to reject. If approved, the newly generated ID of the guide is returned as message.',
        validate: {
            params: {
                id: _joi2.default.string().required()
            },
            query: {
                status: _joi2.default.boolean().required()
            }
        },
        handler: function () {
            var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(request, reply) {
                var id, status, newGuide;
                return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _context4.prev = 0;
                                id = request.params.id, status = request.query.status;
                                _context4.next = 4;
                                return _models.NewGuide.findById(id);

                            case 4:
                                newGuide = _context4.sent;

                                if (!status) {
                                    _context4.next = 13;
                                    break;
                                }

                                _context4.t0 = reply;
                                _context4.next = 9;
                                return newGuide.activate();

                            case 9:
                                _context4.t1 = _context4.sent;
                                _context4.t2 = {
                                    message: _context4.t1
                                };
                                (0, _context4.t0)(_context4.t2);
                                return _context4.abrupt('return');

                            case 13:
                                _context4.next = 15;
                                return newGuide.reject();

                            case 15:
                                reply({ message: 'REJECTED' });
                                _context4.next = 21;
                                break;

                            case 18:
                                _context4.prev = 18;
                                _context4.t3 = _context4['catch'](0);

                                (0, _responses.handleError)(request, reply, _context4.t3);

                            case 21:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, undefined, [[0, 18]]);
            }));

            function handler(_x7, _x8) {
                return _ref4.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

var getGuideDues = {
    method: 'GET',
    path: '/portal/guide-dues',
    config: {
        tags: ['portal', 'api'],
        description: 'The admin can get guide dues from here.',
        notes: 'An admin can get how much the guides have earned during a particular period.\n        To and from are optional query params must be in the format MM-DD-YYYY.\n        If they are not given, start and end of the current day is chosen in IST.\n        An appropriate file will be downloaded given by the format query param. Default is .xlsx.',
        validate: {
            query: {
                to: _joi2.default.date().max('now').optional(),
                from: _joi2.default.date().max(_joi2.default.ref('to')).optional(),
                format: _joi2.default.string().valid(['excel', 'csv']).default('excel')
            }
        },
        handler: function () {
            var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(request, reply) {
                var _request$query3, to, from, format, fromDate, toDate, dateFilter, guideReceipts, result, headers, wb, ws, wbbuf;

                return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                _request$query3 = request.query, to = _request$query3.to, from = _request$query3.from, format = _request$query3.format;
                                fromDate = (0, _time2.default)(from).startOf('day').toDate();
                                toDate = (0, _time2.default)(to).endOf('day').toDate();
                                dateFilter = { $gte: fromDate, $lte: toDate };
                                _context5.prev = 4;
                                _context5.next = 7;
                                return _models.GuideReceipt.find({
                                    status: _models.GuideReceiptStatus.DUE,
                                    createdAt: dateFilter
                                }).populate('guide');

                            case 7:
                                guideReceipts = _context5.sent;
                                result = {};

                                guideReceipts.forEach(function (receipt) {
                                    var _receipt$guide = receipt.guide,
                                        id = _receipt$guide.id,
                                        name = _receipt$guide.name,
                                        phone = _receipt$guide.phone,
                                        address = _receipt$guide.address,
                                        state = _receipt$guide.state,
                                        pinCode = _receipt$guide.pinCode,
                                        email = _receipt$guide.email,
                                        accountNumber = _receipt$guide.accountNumber,
                                        ifsc = _receipt$guide.ifsc,
                                        licenseExpiry = _receipt$guide.licenseExpiry,
                                        dob = _receipt$guide.dob,
                                        createdAt = _receipt$guide.createdAt;
                                    var amount = receipt.amount;

                                    amount -= 0;
                                    result[id] = result[id] || [name, amount, phone, address, state, pinCode, email, accountNumber, ifsc, licenseExpiry, dob, createdAt];
                                    result[id][1] += amount - 0;
                                });
                                headers = ['Name', 'Amount', 'Phone', 'Address', 'State', 'Pin Code', 'Email', 'Account Number', 'IFSC', 'License Expiry', 'DOB', 'Activated On'];
                                wb = _xlsx2.default.utils.book_new();
                                ws = _xlsx2.default.utils.aoa_to_sheet([headers].concat((0, _toConsumableArray3.default)((0, _values2.default)(result))));

                                if (!(format === 'csv')) {
                                    _context5.next = 16;
                                    break;
                                }

                                reply(_xlsx2.default.utils.sheet_to_csv(ws)).header('Content-Type', 'application/octet-stream').header('Content-Disposition', 'attachment; filename=Guides.csv');
                                return _context5.abrupt('return');

                            case 16:
                                wb.SheetNames.push('Guides');
                                wb.Sheets.Guides = ws;
                                wbbuf = _xlsx2.default.write(wb, {
                                    type: 'buffer'
                                });

                                reply(wbbuf).header('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet').header('Content-Disposition', 'attachment; filename=Guides.xlsx');
                                _context5.next = 25;
                                break;

                            case 22:
                                _context5.prev = 22;
                                _context5.t0 = _context5['catch'](4);

                                (0, _responses.handleError)(request, reply, _context5.t0);

                            case 25:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, undefined, [[4, 22]]);
            }));

            function handler(_x9, _x10) {
                return _ref5.apply(this, arguments);
            }

            return handler;
        }()
    }
};

var clearGuideDues = {
    method: 'PUT',
    path: '/portal/guide-clear-dues',
    config: {
        tags: ['portal', 'api'],
        description: 'The admin can clear guide dues from here.',
        notes: 'To and from are optional query params must be in the format MM-DD-YYYY.\n        If they are not given, start and end of the current day is chosen in IST.',
        validate: {
            query: {
                to: _joi2.default.date().max('now').optional(),
                from: _joi2.default.date().max(_joi2.default.ref('to')).optional()
            }
        },
        handler: function () {
            var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(request, reply) {
                var _request$query4, to, from, fromDate, toDate, dateFilter;

                return _regenerator2.default.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                _request$query4 = request.query, to = _request$query4.to, from = _request$query4.from;
                                fromDate = (0, _time2.default)(from).startOf('day').toDate();
                                toDate = (0, _time2.default)(to).endOf('day').toDate();
                                dateFilter = { $gte: fromDate, $lte: toDate };
                                _context6.prev = 4;
                                _context6.next = 7;
                                return _models.GuideReceipt.update({
                                    status: _models.GuideReceiptStatus.DUE,
                                    createdAt: dateFilter
                                }, { status: _models.GuideReceiptStatus.PAID });

                            case 7:

                                reply({ message: 'OK' });
                                _context6.next = 13;
                                break;

                            case 10:
                                _context6.prev = 10;
                                _context6.t0 = _context6['catch'](4);

                                (0, _responses.handleError)(request, reply, _context6.t0);

                            case 13:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, undefined, [[4, 10]]);
            }));

            function handler(_x11, _x12) {
                return _ref6.apply(this, arguments);
            }

            return handler;
        }(),
        response: _responses.generalResponse
    }
};

exports.getGuides = getGuides;
exports.getGuide = getGuide;
exports.getNewGuides = getNewGuides;
exports.changeNewGuideStatus = changeNewGuideStatus;
exports.getGuideDues = getGuideDues;
exports.clearGuideDues = clearGuideDues;