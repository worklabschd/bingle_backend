'use strict';

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _hapi = require('hapi');

var _hapi2 = _interopRequireDefault(_hapi);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _config = require('./config.json');

var _routes = require('./routes');

var _plugins = require('./utils/plugins');

var _plugins2 = _interopRequireDefault(_plugins);

var _templating = require('./utils/templating');

var _templating2 = _interopRequireDefault(_templating);

var _deepstream = require('./events/deepstream');

var _constants = require('./events/constants');

var _constants2 = _interopRequireDefault(_constants);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by M on 11/10/17. With ❤
 */

var isProd = process.env.NODE_ENV === 'production';

var connectToMongo = function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var uri;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        uri = 'mongodb://' + (isProd ? 'mongo' : 'localhost') + ':27017/' + _config.database.DB_NAME;

                        _mongoose2.default.Promise = require('bluebird');
                        _context.next = 4;
                        return _mongoose2.default.connect(uri);

                    case 4:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function connectToMongo() {
        return _ref.apply(this, arguments);
    };
}();

var configureHapi = function configureHapi() {
    // Intialize the Hapi server instance.
    var server = new _hapi2.default.Server();
    server.connection({
        port: _config.serverConfig.PORT,
        host: _config.serverConfig.HOST,
        routes: { cors: true },
        labels: ['api', 'user', 'guide', 'portal', 'web', 'hooks', 'partner']
    });

    // Put in place user routes.
    server.select('api').select('user').route(_routes.userRoutes);

    // Put in place guide routes.
    server.select('api').select('guide').route(_routes.guideRoutes);

    // Put in place portal routes.
    server.select('api').select('portal').route(_routes.portalRoutes);

    // Put in open routes for website.
    server.select('api').select('web').route(_routes.webRoutes);

    // Put in routes for partner dashboard.
    server.select('api').select('partner').route(_routes.partnerRoutes);

    // Put in place all the web hooks.
    server.select('hooks').route(_routes.hooks);

    // Put in place swagger and other plugins like logging.
    server.register(_plugins2.default, function (err) {
        if (err) {
            throw err;
        }

        // Put in place the templating engine.
        server.views(_templating2.default);

        // Start the server.
        server.start(function (e) {
            if (e) {
                throw e;
            }
            console.log('Bingle Server running at port: ' + server.info.port);
        });
    });
};

// Fire up everything.
(0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
    return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
            switch (_context2.prev = _context2.next) {
                case 0:
                    _context2.prev = 0;
                    _context2.next = 3;
                    return connectToMongo();

                case 3:
                    _context2.next = 5;
                    return (0, _deepstream.setupDSListeners)((0, _values2.default)(_constants2.default));

                case 5:
                    // Connection to Mongo and Deepstream successful.

                    // Start configuring Hapi.
                    configureHapi();
                    _context2.next = 12;
                    break;

                case 8:
                    _context2.prev = 8;
                    _context2.t0 = _context2['catch'](0);

                    // Connections were unsuccessful.
                    console.warn('Couldn\'t start server - ' + _context2.t0);
                    process.exit(1);

                case 12:
                case 'end':
                    return _context2.stop();
            }
        }
    }, _callee2, undefined, [[0, 8]]);
}))();