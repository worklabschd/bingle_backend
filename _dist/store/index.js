'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getAppOpens = exports.incrementAppOpens = undefined;

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _redis = require('redis');

var _redis2 = _interopRequireDefault(_redis);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var APP_OPENS = 'APP_OPENS';
var APP_OPENS_EXPIRE = 86400; // 1 day.

var isProd = process.env.NODE_ENV === 'production';

// Don't care if connection to redis is unsuccessful,
// since the server will exit anyhow inside queues/queue.js.
var client = _redis2.default.createClient({
    host: isProd ? 'redis' : 'localhost'
});

// Increment counter for each app open.
var incrementAppOpens = function incrementAppOpens() {
    return new _promise2.default(function (resolve, reject) {
        client.incr(APP_OPENS, function (err, opens) {
            if (err) {
                reject(err);
                return;
            }
            // If the counter was reset or has just started,
            // set an expiration or reset timeout for the counter
            // to one day.
            if (opens === 1) {
                client.expire(APP_OPENS, APP_OPENS_EXPIRE, function (e) {
                    if (e) {
                        reject(err);
                        return;
                    }
                    resolve();
                });
                return;
            }
            resolve();
        });
    });
};

// Get app opens from the last 24 hours.
var getAppOpens = function getAppOpens() {
    return new _promise2.default(function (resolve, reject) {
        client.get(APP_OPENS, function (err, opens) {
            if (err) {
                reject(err);
                return;
            }
            resolve(opens - 0);
        });
    });
};

exports.incrementAppOpens = incrementAppOpens;
exports.getAppOpens = getAppOpens;