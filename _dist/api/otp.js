'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.verifyOTP = exports.retryOTP = exports.sendOTP = undefined;

var _sendotp = require('sendotp');

var _sendotp2 = _interopRequireDefault(_sendotp);

var _config = require('../config.json');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SMS = 'Hi! One time password for your Meego registration is {{otp}}.';
var otp = new _sendotp2.default(_config.msg91.AUTH_KEY, SMS);

// Set 15 mins for OTP expiry.
otp.setOtpExpiry('15');

var callbackHOF = function callbackHOF(cb, verify) {
    return function (error, data) {
        if (error) {
            // If any errors.
            cb(error);
        } else if (data.type === 'error' && !verify) {
            // if data.type is not success somehow and we're not veryfing OTP either.
            cb(new Error(data.message));
        } else {
            cb(null, data.message);
        }
    };
};

var sendOTP = function sendOTP(countryCode, phone, callback) {
    otp.send(countryCode.replace('+', '') + phone, _config.msg91.SENDER_ID, callbackHOF(callback));
};

var retryOTP = function retryOTP(countryCode, phone, callback) {
    otp.retry(countryCode.replace('+', '') + phone, true, callbackHOF(callback));
};

var verifyOTP = function verifyOTP(countryCode, phone, otpToVerify, callback) {
    otp.verify('' + countryCode + phone, otpToVerify, callbackHOF(callback, true));
};

exports.sendOTP = sendOTP;
exports.retryOTP = retryOTP;
exports.verifyOTP = verifyOTP;