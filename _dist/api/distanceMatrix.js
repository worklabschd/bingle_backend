'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _unirest = require('unirest');

var _unirest2 = _interopRequireDefault(_unirest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MAPS_API_KEY = 'AIzaSyBh_VKN456wD_wnB9bewzxdfmJvCv6opq0';

var calculateDistanceToPlaces = function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(latitude, longitude, places) {
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        return _context.abrupt('return', new _promise2.default(function (resolve, reject) {
                            var destinations = places.reduce(function (acc, place) {
                                return '' + acc + place.location[1] + ',' + place.location[0] + '|';
                            }, '');
                            var responses = [].concat((0, _toConsumableArray3.default)(places));
                            _unirest2.default.get('https://maps.googleapis.com/maps/api/distancematrix/json').query({
                                key: MAPS_API_KEY,
                                origins: latitude + ',' + longitude,
                                destinations: '' + destinations
                            }).end(function (_ref2) {
                                var ok = _ref2.ok,
                                    body = _ref2.body;

                                if (!ok) {
                                    reject(body);
                                    return;
                                }
                                if (body.status === 'OK') {
                                    var elements = body.rows[0].elements;

                                    elements.forEach(function (element, index) {
                                        var status = element.status,
                                            distance = element.distance,
                                            duration = element.duration;

                                        if (status === 'OK') {
                                            responses[index].distance = distance.text;
                                            responses[index].duration = duration.text;
                                            return;
                                        }
                                        responses[index].distance = 'NA';
                                    });
                                } else {
                                    reject(new Error('DIRECTION MATRIX ERROR ' + body.status + '.'));
                                }
                                resolve(responses);
                            });
                        }));

                    case 1:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function calculateDistanceToPlaces(_x, _x2, _x3) {
        return _ref.apply(this, arguments);
    };
}();

exports.default = calculateDistanceToPlaces;