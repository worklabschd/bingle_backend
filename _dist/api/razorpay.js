'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.capturePayment = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _razorpay = require('razorpay');

var _razorpay2 = _interopRequireDefault(_razorpay);

var _config = require('../config.json');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var KEY_ID = _config.razorpayConfig.KEY_ID,
    KEY_SECRET = _config.razorpayConfig.KEY_SECRET;

var capturePayment = function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(paymentID, amount) {
        var razorpay, _ref2, method, card_id, bank, wallet, email, contact, fee, tax;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        razorpay = new _razorpay2.default({
                            key_id: KEY_ID,
                            key_secret: KEY_SECRET
                        });
                        _context.next = 3;
                        return razorpay.payments.capture(paymentID, amount * 100);

                    case 3:
                        _ref2 = _context.sent;
                        method = _ref2.method;
                        card_id = _ref2.card_id;
                        bank = _ref2.bank;
                        wallet = _ref2.wallet;
                        email = _ref2.email;
                        contact = _ref2.contact;
                        fee = _ref2.fee;
                        tax = _ref2.tax;
                        return _context.abrupt('return', {
                            method: method,
                            cardID: card_id,
                            bank: bank,
                            wallet: wallet,
                            email: email,
                            contact: contact,
                            fee: fee / 100,
                            tax: tax / 100
                        });

                    case 13:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function capturePayment(_x, _x2) {
        return _ref.apply(this, arguments);
    };
}();

exports.capturePayment = capturePayment; // eslint-disable-line