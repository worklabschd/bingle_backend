'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.googleLogin = exports.facebookLogin = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _unirest = require('unirest');

var _unirest2 = _interopRequireDefault(_unirest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var resolveRequest = function resolveRequest(resolve, response) {
    resolve({
        name: response.name,
        email: response.email,
        gender: response.gender || 'na',
        birthday: response.birthday || 0
    });
};

var facebookLogin = function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(token) {
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        return _context.abrupt('return', new _promise2.default(function (resolve, reject) {
                            _unirest2.default.get('https://graph.facebook.com/me?fields=name,email,birthday,gender').query({
                                access_token: token
                            }).end(function (response) {
                                if (!response.ok) {
                                    reject(response.body);
                                    return;
                                }
                                var body = JSON.parse(response.body);
                                resolveRequest(resolve, body);
                            });
                        }));

                    case 1:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function facebookLogin(_x) {
        return _ref.apply(this, arguments);
    };
}();

var googleLogin = function () {
    var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(token) {
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        return _context2.abrupt('return', new _promise2.default(function (resolve, reject) {
                            _unirest2.default.get('https://www.googleapis.com/userinfo/v2/me').headers({ Authorization: 'Bearer ' + token }).end(function (response) {
                                if (!response.ok) {
                                    reject(response.body);
                                    return;
                                }
                                resolveRequest(resolve, response.body);
                            });
                        }));

                    case 1:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, undefined);
    }));

    return function googleLogin(_x2) {
        return _ref2.apply(this, arguments);
    };
}();

exports.facebookLogin = facebookLogin;
exports.googleLogin = googleLogin;