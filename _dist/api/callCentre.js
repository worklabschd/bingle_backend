'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _unirest = require('unirest');

var _unirest2 = _interopRequireDefault(_unirest);

var _config = require('../config.json');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var basePath = 'http://www.prpmobility.in/Api/ClickCallReq.aspx';

var requestCall = function requestCall(userNumber, guideNumber, callCentreToken) {
    return new _promise2.default(function (resolve, reject) {
        _unirest2.default.get(basePath).query({
            uname: _config.callConfig.CALL_CENTRE_USERNAME,
            pass: _config.callConfig.CALL_CENTRE_PASS,
            aparty: userNumber,
            bparty: guideNumber,
            reqid: '' + callCentreToken
        }).end(function (_ref) {
            var ok = _ref.ok,
                body = _ref.body;

            if (!ok) {
                reject(body);
                return;
            }
            if (!body.includes('SUCCESS')) {
                reject(body);
                return;
            }
            resolve();
        });
    });
};

exports.default = requestCall;