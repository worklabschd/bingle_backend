'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.IMAGE_FOLDER = exports.PUBLIC_FOLDER = exports.saveImage = exports.deleteFile = undefined;

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _ba = require('ba64');

var _ba2 = _interopRequireDefault(_ba);

var _v = require('uuid/v1');

var _v2 = _interopRequireDefault(_v);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PUBLIC_FOLDER = 'assets';
var IMAGE_FOLDER = 'images';

// Function to delete any image.
var deleteFile = function deleteFile(folder, fileName) {
    return new _promise2.default(function (resolve, reject) {
        var oldFSPath = _path2.default.join(PUBLIC_FOLDER, IMAGE_FOLDER, folder, fileName);
        _fs2.default.unlink(oldFSPath, function (err) {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        });
    });
};

// Function to save a base64 image.
// Resolves with the the new filename with which the file was saved.
var saveImage = function saveImage(folder, image) {
    return new _promise2.default(function (resolve, reject) {
        var fileName = (0, _v2.default)();
        var newFSPath = _path2.default.join(PUBLIC_FOLDER, IMAGE_FOLDER, folder, fileName);
        _ba2.default.writeImage(newFSPath, image, function (err) {
            if (err) {
                reject(err);
                return;
            }
            resolve(fileName + '.' + _ba2.default.getExt(image));
        });
    });
};

exports.deleteFile = deleteFile;
exports.saveImage = saveImage;
exports.PUBLIC_FOLDER = PUBLIC_FOLDER;
exports.IMAGE_FOLDER = IMAGE_FOLDER;