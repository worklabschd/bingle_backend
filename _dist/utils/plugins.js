'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _hapiSwaggered = require('hapi-swaggered');

var _hapiSwaggered2 = _interopRequireDefault(_hapiSwaggered);

var _hapiSwaggeredUi = require('hapi-swaggered-ui');

var _hapiSwaggeredUi2 = _interopRequireDefault(_hapiSwaggeredUi);

var _vision = require('vision');

var _vision2 = _interopRequireDefault(_vision);

var _inert = require('inert');

var _inert2 = _interopRequireDefault(_inert);

var _hapiCors = require('hapi-cors');

var _hapiCors2 = _interopRequireDefault(_hapiCors);

var _logging = require('./logging');

var _logging2 = _interopRequireDefault(_logging);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var swaggeredPlugin = {
    register: _hapiSwaggered2.default,
    options: {
        supportedMethods: ['get', 'post', 'delete', 'put'],
        info: {
            title: 'Meego',
            description: 'API documentation for Meego Backend. Written in Node.js and Hapi.',
            version: '0.0.1',
            contact: {
                name: 'Mustansir Zia',
                url: 'www.mustansirzia.com',
                email: 'mustansir29@hotmail.com'
            }
        },
        tags: [{ name: 'user', description: 'All customer app endpoints go here.' }, { name: 'guide', description: 'All tour guide app endpoints go here.' }, { name: 'portal', description: 'All admin panel portal endpoints go here.' }, { name: 'hooks', description: 'Web hooks used for providing access from 3rd party APIs.' }, { name: 'web', description: 'All open APIs that are used inside the public website.' }, { name: 'partner', description: 'All partner dashboard APIs go here.' }]
    }
};

var swaggeredUIPlugin = {
    register: _hapiSwaggeredUi2.default,
    options: {
        title: 'Meego',
        path: '/swag',
        swaggerOptions: {
            validatorUrl: null
        }
    }
};

var corsPlugin = {
    register: _hapiCors2.default,
    options: {
        methods: ['GET, POST, PUT, OPTIONS, DELETE']
    }
};

var loggerPlugin = {
    register: function register(server, options, next) {
        var stringer = function stringer(error, request) {
            return 'error - ' + error + '. route - ' + request.path + '. queryParams - ' + (0, _stringify2.default)(request.query) + ', params - ' + (0, _stringify2.default)(request.params) + ', payload - ' + (0, _stringify2.default)(request.payload) + '.';
        };

        // Request error logging callback for 500s.
        server.on('request', function (request, event) {
            var message = stringer(event.data, request);

            // Start logging this error. Persist it in production.
            (0, _logging2.default)(message);
        });

        // Request exception error logging callback for uncaught exceptions.
        server.on('request-error', function (request, event) {
            var message = stringer(event, request);

            // Start logging this exception. Persist it in production.
            (0, _logging2.default)(message);
        });

        next();
    }
};

loggerPlugin.register.attributes = {
    name: 'Logging plugin for Meego Backend.',
    version: '0.0.1'
};

var fileHandlerPlugin = {
    register: function register(server, options, next) {
        server.route({
            method: 'GET',
            path: '/{param*}',
            handler: {
                directory: {
                    path: 'assets'
                }
            }
        });
        next();
    }
};

fileHandlerPlugin.register.attributes = {
    name: 'File handler plugin for Meego Backend.',
    version: '0.0.1'
};

exports.default = [corsPlugin, _vision2.default, _inert2.default, swaggeredPlugin, swaggeredUIPlugin, loggerPlugin, fileHandlerPlugin];