'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.selectRandElements = exports.randCallRequestToken = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _lodash = require('lodash');

var _ = _interopRequireWildcard(_lodash);

var _callRequest = require('../models/callRequest');

var _callRequest2 = _interopRequireDefault(_callRequest);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var randInt = function randInt(digits) {
    return _.random('9'.repeat(digits));
};

var randCallRequestToken = function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var rand, request;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        rand = randInt(8);
                        _context.next = 3;
                        return _callRequest2.default.findOne({
                            callCentreToken: rand
                        });

                    case 3:
                        request = _context.sent;

                        if (!request) {
                            _context.next = 8;
                            break;
                        }

                        _context.next = 7;
                        return randCallRequestToken();

                    case 7:
                        rand = _context.sent;

                    case 8:
                        return _context.abrupt('return', rand);

                    case 9:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function randCallRequestToken() {
        return _ref.apply(this, arguments);
    };
}();

var selectRandElements = function selectRandElements(elements, selectCount) {
    if (elements.length <= selectCount) {
        return elements;
    }
    var selectedIndexes = [];
    while (selectedIndexes.length !== selectCount) {
        var randomIndex = _.random(elements.length - 1);
        if (!selectedIndexes.includes(randomIndex)) {
            selectedIndexes.push(randomIndex);
        }
    }
    var selectedElements = _.pullAt(elements, selectedIndexes);
    return selectedElements;
};

exports.randCallRequestToken = randCallRequestToken;
exports.selectRandElements = selectRandElements;