'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
/* eslint no-unused-vars: "off" */
var winston = require('winston');
var _ = require('winston-mongodb').MongoDB;

var DB_NAME = require('../config.json').database.DB_NAME;

var isProd = process.env.NODE_ENV === 'production';

// If started inside a Docker container. Modify the uri accordingly.
var uri = 'mongodb://' + (isProd ? 'mongo' : 'localhost') + ':27017/' + DB_NAME + '_logs';

var logger = new winston.Logger({
    transports: [new winston.transports.MongoDB({ level: 'error', cappedMax: 200, db: uri })]
});

var queryLogs = function queryLogs(days, callback) {
    logger.query({
        from: new Date() - (24 * 60 * 60 * 1000 * days || 1),
        until: new Date(),
        limit: 30,
        start: 0,
        order: 'desc',
        fields: ['message', 'timestamp']
    }, function (err, logs) {
        callback(err, logs);
    });
};

var log = function log(message) {
    // If this is production. Persist logs.
    if (isProd) {
        logger.error(message instanceof Error ? message.toString() : message);
    }
    console.error(message);
};

exports.queryLogs = queryLogs;
exports.default = log;