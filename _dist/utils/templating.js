'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _handlebars = require('handlebars');

var _handlebars2 = _interopRequireDefault(_handlebars);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Meego's templating engine.
exports.default = {
    engines: {
        html: _handlebars2.default
    },
    relativeTo: _path2.default.join(__dirname, '..'),
    path: 'views'
};