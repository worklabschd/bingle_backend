'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _guide = require('./guide');

var _guide2 = _interopRequireDefault(_guide);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose2.default.Promise = require('bluebird'); /**
                                                   * Created by M on 06/05/17. With ❤
                                                   */

var Schema = _mongoose2.default.Schema;


var newGuideSchema = new Schema({
    name: { type: String, required: true },
    phone: { type: String, required: true },
    countryCode: { type: String, required: true },
    address: { type: String },
    state: { type: String },
    pinCode: { type: String },
    email: {
        type: String, required: true, unique: true, lowercase: true
    },
    status: { type: Boolean, default: false },
    rejected: { type: Boolean, default: false },
    password: { type: String, required: true },
    activatedWhen: Date,
    rejectedWhen: Date,
    createdAt: Date
}, { runSettersOnQuery: true });

newGuideSchema.pre('save', function (next) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

// This method activates the new registration and creates a tour guide account.
newGuideSchema.methods.activate = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
    var hashed, guide;
    return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
            switch (_context.prev = _context.next) {
                case 0:
                    hashed = _bcrypt2.default.hashSync(this.password, 10);
                    _context.next = 3;
                    return _guide2.default.create({
                        name: this.name,
                        phone: this.phone,
                        countryCode: this.countryCode,
                        address: this.address,
                        state: this.state,
                        pinCode: this.pinCode,
                        email: this.email,
                        password: hashed
                    });

                case 3:
                    guide = _context.sent;

                    // Plain text password is reset.
                    this.password = 'stale';
                    this.status = true;
                    this.activatedWhen = new Date();
                    _context.next = 9;
                    return this.save();

                case 9:
                    return _context.abrupt('return', guide.id);

                case 10:
                case 'end':
                    return _context.stop();
            }
        }
    }, _callee, this);
}));

// This method rejects the new registration.
newGuideSchema.methods.reject = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
    return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
            switch (_context2.prev = _context2.next) {
                case 0:
                    // Plain text password is reset.
                    this.password = 'stale';
                    this.rejected = true;
                    this.rejectedWhen = new Date();
                    _context2.next = 5;
                    return this.save();

                case 5:
                case 'end':
                    return _context2.stop();
            }
        }
    }, _callee2, this);
}));

var NewGuide = _mongoose2.default.model('NewGuide', newGuideSchema);

exports.default = NewGuide;