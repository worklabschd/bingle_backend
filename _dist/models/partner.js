'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by M on 06/05/17. With ❤
 */

_mongoose2.default.Promise = require('bluebird');

var Schema = _mongoose2.default.Schema;


var partnerSchema = new Schema({
    name: { type: String, required: true },
    phone: { type: String, required: true },
    countryCode: { type: String, required: true },
    address: { type: String },
    state: { type: String },
    pinCode: { type: String },
    email: {
        type: String, required: true, unique: true, lowercase: true
    },
    password: { type: String, required: true },
    license: { type: String },
    createdAt: { type: Date, default: new Date()
        // so a Mustansir29@hotmail.com would be eq to mustansir29@hotmail.com
    } }, { runSettersOnQuery: true });

partnerSchema.methods.comparePassword = function (plainTextPassword) {
    return _bcrypt2.default.compareSync(plainTextPassword, this.password);
};

var Partner = _mongoose2.default.model('Partner', partnerSchema);

exports.default = Partner;