'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.QueryStatus = exports.QueryType = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose2.default.Promise = require('bluebird'); /**
                                                   * Created by M on 06/05/17. With ❤
                                                   */

var QueryType = {
    USER_QUERY: 'user',
    GUIDE_QUERY: 'guide',
    OPEN_QUERY: 'open'
};

var QueryStatus = {
    CREATED: 'created',
    NOT_RESOLVED: 'not_resolved',
    RESOLVED: 'resolved'
};

var Schema = _mongoose2.default.Schema;


var querySchema = new Schema({
    guide: { type: Schema.Types.ObjectId, ref: 'Guide' },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    name: { type: String, required: true },
    type: { type: String, required: true },
    phone: { type: String, required: true },
    email: { type: String, required: true },
    body: { type: String },
    status: { type: String, default: QueryStatus.CREATED },
    createdAt: Date
}, { runSettersOnQuery: true });

querySchema.pre('save', function (next) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

var Query = _mongoose2.default.model('Query', querySchema);

exports.QueryType = QueryType;
exports.QueryStatus = QueryStatus;
exports.default = Query;