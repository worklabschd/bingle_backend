'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.GuideReceiptStatus = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose2.default.Promise = require('bluebird'); /**
                                                   * Created by M on 03/03/18. With ❤
                                                   */

var Schema = _mongoose2.default.Schema;


var GuideReceiptStatus = {
    DUE: 'DUE',
    PAID: 'PAID'
};

var guideReceiptSchema = new Schema({
    guide: { type: Schema.Types.ObjectId, ref: 'Guide', required: true },
    amount: { type: String, required: true },
    status: { type: String, required: true },
    createdAt: { type: Date, default: new Date() }
});

var GuideReceipt = _mongoose2.default.model('GuideReceipt', guideReceiptSchema);

exports.GuideReceiptStatus = GuideReceiptStatus;
exports.default = GuideReceipt;