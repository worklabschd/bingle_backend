'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.NearbyPlaceType = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose2.default.Promise = require('bluebird'); /**
                                                   * Created by M on 06/05/17. With ❤
                                                   */

var NearbyPlaceType = {
    POPULAR: 'popular',
    MONUMENT: 'monument',
    FOOD: 'food',
    ATM: 'atm'
};

var Schema = _mongoose2.default.Schema;


var nearbyPlaceSchema = new Schema({
    type: { type: String, required: true },
    name: { type: String, required: true, index: 'text' },
    description: { type: String, required: true },
    image: { type: String, required: true },
    location: { type: [Number], index: '2dsphere' },
    city: { type: String },
    createdAt: Date
});

nearbyPlaceSchema.pre('save', function (next) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

var NearbyPlace = _mongoose2.default.model('NearbyPlace', nearbyPlaceSchema);

exports.NearbyPlaceType = NearbyPlaceType;
exports.default = NearbyPlace;