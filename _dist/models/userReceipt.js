'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose2.default.Promise = require('bluebird'); /**
                                                   * Created by M on 04/02/18. With ❤
                                                   */

var Schema = _mongoose2.default.Schema;


var userReceiptSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    paymentGateway: { type: String, required: true },
    amount: { type: String, required: true },
    paymentID: { type: String, required: true },
    international: { type: Boolean, default: false },
    method: { type: String, required: true },
    cardID: { type: String },
    bank: { type: String },
    wallet: { type: String },
    email: { type: String },
    contact: { type: String },
    fee: { type: String },
    tax: { type: String },
    createdAt: Date
});

userReceiptSchema.pre('save', function (next) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

var UserReceipt = _mongoose2.default.model('UserReceipt', userReceiptSchema);

exports.default = UserReceipt;