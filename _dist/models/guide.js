'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Language = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by M on 06/05/17. With ❤
 */

_mongoose2.default.Promise = require('bluebird');

var Language = {
    ENGLISH: 'english',
    HINDI: 'hindi',
    FRENCH: 'french',
    includes: function includes(language) {
        return (0, _values2.default)(this).includes(language);
    }
};

var Schema = _mongoose2.default.Schema;


var guideSchema = new Schema({
    name: { type: String, required: true },
    phone: { type: String, required: true },
    countryCode: { type: String, required: true },
    address: { type: String },
    state: { type: String },
    pinCode: { type: String },
    email: {
        type: String, required: true, unique: true, lowercase: true
    },
    password: { type: String, required: true },
    location: { type: [Number], index: '2dsphere' },
    languages: { type: [String], default: [Language.ENGLISH, Language.HINDI] },
    onDuty: { type: Boolean, default: false },
    onQueue: { type: Boolean, default: false },
    toggledDuty: { type: Date },
    license: { type: String },
    avatar: { type: String },
    accountNumber: { type: String },
    ifsc: { type: String },
    licenseExpiry: { type: Date },
    dob: { type: Date },
    createdAt: { type: Date
        // so a Mustansir29@hotmail.com would be eq to mustansir29@hotmail.com
    } }, { runSettersOnQuery: true });

guideSchema.pre('save', function (next) {
    // If this is a new guide.
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

guideSchema.methods.comparePassword = function (plainTextPassword) {
    return _bcrypt2.default.compareSync(plainTextPassword, this.password);
};

// Queue a guide so he doesn't get a booking.
guideSchema.methods.queueGuide = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
    return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
            switch (_context.prev = _context.next) {
                case 0:
                    this.onQueue = true;
                    _context.next = 3;
                    return this.save();

                case 3:
                case 'end':
                    return _context.stop();
            }
        }
    }, _callee, this);
}));

// Un Queue a guide so he may get a booking now.
guideSchema.methods.unQueueGuide = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
    return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
            switch (_context2.prev = _context2.next) {
                case 0:
                    this.onQueue = false;
                    _context2.next = 3;
                    return this.save();

                case 3:
                case 'end':
                    return _context2.stop();
            }
        }
    }, _callee2, this);
}));

var Guide = _mongoose2.default.model('Guide', guideSchema);

exports.Language = Language;
exports.default = Guide;