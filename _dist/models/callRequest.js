'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.RequestStatus = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose2.default.Promise = require('bluebird'); /**
                                                   * Created by M on 06/05/17. With ❤
                                                   */

var RequestStatus = {
    CREATED: 'created',
    ACCEPTED: 'accepted',
    UNBILLED: 'unbilled',
    BILLED: 'billed'
};

var Schema = _mongoose2.default.Schema;


var callRequestSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    associatedGuides: [{ type: Schema.Types.ObjectId, ref: 'Guide' }],
    guide: { type: Schema.Types.ObjectId, ref: 'Guide' },
    guideReceipt: { type: Schema.Types.ObjectId, ref: 'GuideReceipt' },
    callCentreToken: { type: String },
    duration: { type: Number },
    fare: { type: String },
    rate: { type: String },
    guideRate: { type: String },
    location: { type: [Number], index: '2dsphere' },
    phone: { type: String, required: true },
    // type RequestStatus.
    language: { type: String, required: true },
    status: { type: String, required: true },
    createdAt: Date
});

callRequestSchema.pre('save', function (next) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

// Called when a call request is accepted by a guide.
callRequestSchema.methods.acceptRequest = function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(guideID) {
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        this.guide = guideID;
                        this.status = RequestStatus.ACCEPTED;
                        _context.next = 4;
                        return this.save();

                    case 4:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

var CallRequest = _mongoose2.default.model('CallRequest', callRequestSchema);

exports.RequestStatus = RequestStatus;
exports.default = CallRequest;