'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _events = require('events');

var _events2 = _interopRequireDefault(_events);

var _deepstream = require('./deepstream');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MeegoEmitter = function (_EventEmitter) {
    (0, _inherits3.default)(MeegoEmitter, _EventEmitter);

    function MeegoEmitter() {
        (0, _classCallCheck3.default)(this, MeegoEmitter);
        return (0, _possibleConstructorReturn3.default)(this, (MeegoEmitter.__proto__ || (0, _getPrototypeOf2.default)(MeegoEmitter)).apply(this, arguments));
    }

    (0, _createClass3.default)(MeegoEmitter, [{
        key: 'emitToGuidesAndRegisterOnce',
        value: function emitToGuidesAndRegisterOnce(eventName, eventID, guideIDs, data, timeout, callback) {
            var _this2 = this;

            var eventNameFull = eventName + '/' + eventID;
            var timeoutFunc = setTimeout(function () {
                _this2.removeAllListeners(eventNameFull);
                callback(null);
            }, timeout);
            this.once(eventNameFull, function (payload) {
                clearTimeout(timeoutFunc);
                callback(payload);
            });
            guideIDs.forEach(function (guideID) {
                (0, _deepstream.emitToDS)(guideID, { eventName: eventName, eventID: eventID, data: data });
            });
        }
    }]);
    return MeegoEmitter;
}(_events2.default);

exports.default = new MeegoEmitter();