'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.setupDSListeners = exports.emitToDS = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _deepstream = require('deepstream.io-client-js');

var _deepstream2 = _interopRequireDefault(_deepstream);

var _meegoEmitter = require('./meegoEmitter');

var _meegoEmitter2 = _interopRequireDefault(_meegoEmitter);

var _logging = require('../utils/logging');

var _logging2 = _interopRequireDefault(_logging);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isProd = process.env.NODE_ENV === 'production';
// import { deepstreamConfig } from '../config.json';

var client = null;
var dsClient = null;

// Variable to keep track of Deepstream client connection status.
var dsConnected = false;

// If connection to deepstream ever breaks down, we'd know about it. Plus Vice Versa!
var stateChangeListener = function stateChangeListener(state) {
    switch (state) {
        case _deepstream2.default.CONSTANTS.CONNECTION_STATE.ERROR:
            dsConnected = false;
            (0, _logging2.default)('Deepstream Disconnected : WorkerID ' + process.pid);
            break;
        case _deepstream2.default.CONSTANTS.CONNECTION_STATE.OPEN:
            dsConnected = true;
            break;
        default:
    }
};

// Explicit DS error handling.
var errorHandler = function errorHandler(error, event, topic) {
    (0, _logging2.default)('Deepstream Error | msg: ' + error + ' | event: ' + event + ' |  topic: ' + topic + ' | worker: ' + process.pid);
    // If this the server has started the first time (maybe deepstream is down), exit.
    if (!dsClient) {
        process.exit(1);
    }
};

// Function to connect to Deepstream.
var connectDSClient = function connectDSClient() {
    return new _promise2.default(function (resolve, reject) {
        // Login as a client.
        client.login({}, function (success, data) {
            if (!success) {
                reject(new Error('Deepstream authentication error - ' + (0, _stringify2.default)(data)));
                return;
            }
            resolve();
        });
    });
};

var emitToDS = function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(eventName, payload) {
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        if (!dsConnected) {
                            _context.next = 3;
                            break;
                        }

                        // Emit to Deepstream if Deepstream client is connected.
                        dsClient.emit(eventName, payload);
                        return _context.abrupt('return');

                    case 3:
                        _context.next = 5;
                        return connectDSClient();

                    case 5:
                        dsClient.emit(eventName, payload);

                    case 6:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function emitToDS(_x, _x2) {
        return _ref.apply(this, arguments);
    };
}();

var setupDSListeners = function () {
    var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(eventNames) {
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        // Connect to deepstream first.
                        client = (0, _deepstream2.default)((isProd ? 'deepstream' : 'localhost') + ':6020');
                        client.on('error', errorHandler);
                        client.on('connectionStateChanged', stateChangeListener);
                        _context2.next = 5;
                        return connectDSClient();

                    case 5:
                        dsClient = client.event;
                        eventNames.forEach(function (eventName) {
                            dsClient.subscribe(eventName, function (payload) {
                                _meegoEmitter2.default.emit(eventName + '/' + payload.eventID, payload);
                            });
                        });

                    case 7:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, undefined);
    }));

    return function setupDSListeners(_x3) {
        return _ref2.apply(this, arguments);
    };
}();

exports.emitToDS = emitToDS;
exports.setupDSListeners = setupDSListeners;