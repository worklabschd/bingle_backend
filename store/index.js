import redis from 'redis';

const APP_OPENS = 'APP_OPENS';
const APP_OPENS_EXPIRE = 86400; // 1 day.

const isProd = process.env.NODE_ENV === 'production';

// Don't care if connection to redis is unsuccessful,
// since the server will exit anyhow inside queues/queue.js.
const client = redis.createClient({
    host: isProd ? 'redis' : 'localhost'
});

// Increment counter for each app open.
const incrementAppOpens = () => new Promise((resolve, reject) => {
    client.incr(APP_OPENS, (err, opens) => {
        if (err) {
            reject(err);
            return;
        }
        // If the counter was reset or has just started,
        // set an expiration or reset timeout for the counter
        // to one day.
        if (opens === 1) {
            client.expire(APP_OPENS, APP_OPENS_EXPIRE, e => {
                if (e) {
                    reject(err);
                    return;
                }
                resolve();
            });
            return;
        }
        resolve();
    });
});

// Get app opens from the last 24 hours.
const getAppOpens = () => new Promise((resolve, reject) => {
    client.get(APP_OPENS, (err, opens) => {
        if (err) {
            reject(err);
            return;
        }
        resolve(opens - 0);
    });
});

export {
    incrementAppOpens,
    getAppOpens
};
