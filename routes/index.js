/**
 * Created by M on 17/04/17. With ❤
 */

import guideRoutes from './guide';
import userRoutes from './user';
import portalRoutes from './portal';
import webRoutes from './web';
import partnerRoutes from './partner';
import hooks from './hooks';
import chatClientRoutes from './chat';

export { guideRoutes, userRoutes, portalRoutes, webRoutes, partnerRoutes, hooks, chatClientRoutes };
