import Joi from 'joi';
import moment from 'moment';
import { Partner } from '../../models'; // not ../../models/Partner okay? will look in index.js for ../../models if you dont specify a file. is it okay now ? yes. Let me run it
import { generalResponse, newPartnerResponse, handleError } from '../utils/responses';
import bcrypt from 'bcrypt';

const login = {
    method: 'POST',
    path: '/partner/login',
    config: {
        tags: ['partner', 'api'],
        description: 'Partner logs in into the partner dashboard using this endpoint.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                email: Joi.string().required(),
                password: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const { email, password } = request.payload;
                const partner = await Partner.findOne({ email });
                if (!partner) {
                    reply({ message: 'INVALID EMAIL' }).code(400);
                } else if (partner.comparePassword(password)) {
                    console.log("Successful");
                    reply({
                        id: partner.id,
                        name: partner.name,
                        address: partner.address,
                        state: partner.state,
                        pinCode: partner.pinCode,
                        email: partner.email,
                        countryCode: partner.countryCode,
                        phone: partner.phone,
                        license: partner.license
                    });
                } else {
                    reply({ message: 'INVALID PASSWORD' }).code(400);
                }
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

// const validateEmail = {
//     method: 'GET',
//     path: '/partner/validateEmail',
//     config: {
//         tags: ['guide', 'api'],
//         description: 'This validates the email of the tour guide for uniqueness',
//         notes: 'This should be unauthenticated.',
//         validate: {
//             query: {
//                 email: Joi.string().required(),
//             }
//         },
//         handler: async (request, reply) => {
//             try {
//                 const { email } = request.query;
//                 const user = await Guide.findOne({ email });
//                 if (!user) {
//                     reply({ message: 'OK' });
//                 } else {
//                     reply({ message: 'INVALID' }).code(400);
//                 }
//             } catch (err) {
//                 handleError(request, reply, err);
//             }
//         },
//         response: generalResponse
//     }
// };

const register = {
    method: 'POST',
    path: '/partner/register',
    config: {
        tags: ['partner', 'api'],
        description: 'Partner registers on the Portal using this endpoint.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                name: Joi.string().required(),
                countryCode: Joi.string().required(),
                phone: Joi.string().required(),
                email: Joi.string().email().required(),
                password: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    email, name, countryCode, phone, password
                } = request.payload;

                // Checks if the tour guide has already registered in our system.
                // By checking email || (countryCode && phone)
                const oldRegisteration = await Partner.findOne({
                    $or: [
                        { email }
                    ],
                    status: true
                });
                if (oldRegisteration) {
                    reply({ message: 'ALREADY REGISTERED' }).code(400);
                    return;
                }

                // Checks if the tour guide is already present in our system.
                // By again checking email || (countryCode && phone)
                const partner = await Partner.findOne({
                    $or: [
                        { email }
                    ]
                });
                if (partner) {
                    reply({ message: 'ACCOUNT EXISTS' }).code(400);
                    return;
                }
                // Add a new registration.
                const hashed = bcrypt.hashSync(password, 10);
                const newPartner = await Partner.create({
                    name,
                    phone,
                    countryCode,
                    email,
                    password : hashed
                });

                // Let's activate the partner, for now. (Since we don't have the portal yet)
                //const partnerID = await newPartner.activate();

                reply({ message: "Account Created Successfully" });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

export { register, login };
