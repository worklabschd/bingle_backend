/**
 * Created by M on 06/05/17. With ❤
 */

import Joi from 'joi';
import { User, Chat, ChatMessage, WifiRoom } from '../../models';
import { 
    generalResponse, 
    handleError
} from '../utils/responses';

const loadChatView = {
    method: 'GET',
    path: '/chat/chat-view',
    config: {
        tags: ['chat', 'api'],
        description: 'Handler that serves the chat view.',
        notes: 'This should be unauthenticated.',
        handler: async (request, reply) => {
            try {
                reply.view('chatView');
            } catch (err) {
                handleError(request, reply, err);
            }
        }
    }
};

const getAllChats = {
    method: 'GET',
    path: '/chat',
    config: {
        tags: ['chat', 'api'],
        description: 'Handler that serves the chat list user has already started.',
        notes: 'This should be authenticated.',
        validate: {
            query: {
                username: Joi.string().required(),
                offset: Joi.number().required()
            }
        },
        handler: async (request, reply) => {
            const { username, offset } = request.query;
            try {
                let chatRequests = await Chat.find({ 
                    users: username
                })
                .sort('-updatedAt')
                .skip(offset * 20)
                .limit(20);

                
                reply({
                    statusCode: 200,
                    data: {
                        chatList: chatRequests
                    },
                    message: 'chat list fetched successfully'
                });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
}

const getChatHistory = {
    method: 'GET',
    path: '/chat/history',
    config: {
        tags: ['chat', 'api'],
        description: 'Handler that serves the chat history of particular chat.',
        notes: 'This should be authenticated.',
        validate: {
            query: {
                username: Joi.string().required(),
                chat: Joi.string().required(),
                offset: Joi.number().required()
            }
        },
        handler: async (request, reply) => {
            const { username, chat, offset } = request.query;
            try {
                let chatRequests = await Chat.find({ 
                    users: username,
                    _id: chat
                });

                if (chatRequests.length){
                    let anotherChatRequest = await ChatMessage.find({
                        chat: chat
                    })
                    .sort('-createdAt')
                    .skip(offset * 20)
                    .limit(20);
                    
                    reply({
                        statusCode: 200,
                        data: {
                            chatMessages: anotherChatRequest
                        },
                        message: 'chat messages fetched successfully'
                    });
                    
                } else {
                    reply({
                        statusCode: 200,
                        data: []
                    });
                }
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
}


const wifiRoomStats = {
    method: 'GET',
    path: '/chat/wifi/stats',
    config: {
        tags: ['chat', 'api'],
        description: 'Handler that serves stats of this wifi room.',
        notes: 'This should be authenticated.',
        validate: {
            query: {
                room: Joi.string().required(),
                username: Joi.string().optional(),
                offset: Joi.number().required(),
                limit: Joi.number().required()
            }
        },
        handler: async (request, reply) => {
            const { room, username, offset, limit } = request.query;
            try {
                let data;
                if(username) {
                    data = {
                        room: room,
                        user: username
                    }
                } else {
                    data = {
                        room: room
                    }
                }
                let statRequests = await WifiRoom.find(data)
                .sort('-createdAt')
                .skip(offset * limit)
                .limit(limit);
                
                reply({
                    statusCode: 200,
                    data: {
                        stats: statRequests
                    },
                    message: 'stats fetched successfully'
                });
                
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
}


export {
    loadChatView,
    getAllChats,
    getChatHistory,
    wifiRoomStats
};
