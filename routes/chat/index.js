import * as chatClientRoutes from './chatClient';

export default Object.values(chatClientRoutes);