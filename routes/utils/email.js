import nodemailer from 'nodemailer';

import { smtp } from '../../config.json';

const {
    ADDRESS,
    HOST,
    USERNAME,
    PASSWORD,
    PORT,
    EMAIL
} = smtp;

const transporter = nodemailer.createTransport({
    host: HOST,
    port: PORT,
    secure: true,
    auth: {
        user: USERNAME,
        pass: PASSWORD
    }
});

const send = (to, subject, html) => new Promise((resolve, reject) => {
    transporter.sendMail({
        from: EMAIL,
        to,
        subject,
        html
    }, (error, info) => {
        if (error) {
            reject(error);
            return;
        }
        resolve(info.messageId);
    });
});

const sendForgotPasswordEmail = (
    email,
    resetToken
) => send(
    email,
    'Forgot your password?',
    `
      Looks like you've requested for a new password. Click <a href="${ADDRESS}/user/reset-password?token=${resetToken}">here</a> to continue.
      <br />
      This link shall remain active for the next <b>one hour</b>.
      <br />
      Ignore this mail if this wasn't you.
    `
);

const sendQueryReply = (
    email,
    reply,
    query
) => send(
    email,
    'Meego Support',
    `
    ${reply}
    <br />
    <br />
    Your query raised was: ${query}
  `
);

export { sendForgotPasswordEmail, sendQueryReply }; // eslint-disable-line
