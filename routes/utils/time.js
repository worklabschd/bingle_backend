import moment from 'moment';

const TIME_ZONE = '+05:30';
const localMoment = date => moment(date).utcOffset(TIME_ZONE);

export default localMoment;
