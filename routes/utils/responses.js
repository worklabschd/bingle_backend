// Contains response models for Hapi.js endpoints.
import Joi from 'joi';
import * as _ from 'lodash';
import { Language, NearbyPlaceType, QueryType, QueryStatus } from '../../models';

// General reponses.
const general = Joi.object({
    statusCode: Joi.number().optional(),
    data: Joi.object().optional(),
    message: Joi.string().required()
});
const error = general.keys({
    error: Joi.string().optional(),
});

const response = successful => ({ status: { 200: successful, 400: error, 500: error } });

const generalResponse = response(general);

// User responses.
const otp = general.keys({
    status: Joi.string()
        .valid(['OTP_VALID', 'OTP_INVALID', 'MOBILE_NOT_FOUND', 'INVALID_MOBILE', 'OTP_EXPIRED'])
        .required()
});
const user = Joi.object({
    id: Joi.string().required(),
    name: Joi.string().required(),
    countryCode: Joi.string().required(),
    phone: Joi.string().required(),
    age: Joi.number().optional(),
    gender: Joi.string().valid(['male', 'female', 'na']),
    avatar: Joi.string().optional(),
    email: Joi.string().email().required(),
    blocked: Joi.boolean().optional(),
    latitude: Joi.number().optional(),
    longitude: Joi.number().optional()
});
const social = Joi.object({
    id: Joi.string().optional(),
    name: Joi.string().required(),
    email: Joi.string().email().required(),
    countryCode: Joi.string().optional(),
    phone: Joi.string().optional(),
    age: Joi.optional(),
    gender: Joi.string().optional().valid(['male', 'female', 'na']),
    avatar: Joi.string().optional(),
    status: Joi.string().valid(['EXISTING', 'NEW']).required()
});
const nearbyPlace = Joi.object({
    id: Joi.string().optional(),
    name: Joi.string().required(),
    description: Joi.string().required(),
    image: Joi.string().required(),
    type: Joi.string().required(),
    latitude: Joi.number().required(),
    longitude: Joi.number().required(),
    distance: Joi.string().optional(),
    duration: Joi.string().optional()
});
const nearbyPlaces = Joi.array().items(nearbyPlace);
const nearbyPlacesForApp = Joi.object(_.mapValues(
    _.mapKeys(
        NearbyPlaceType,
        placeType => placeType.toLowerCase()
    ),
    () => nearbyPlaces
));
const callHistory = Joi.object({
    id: Joi.string().required(),
    status: Joi.string().required(),
    fare: Joi.string().required(),
    duration: Joi.string().required(),
    date: Joi.string().required(),
    year: Joi.string().required(),
    time: Joi.string().required(),
    language: Joi.string().valid(Object.values(Language)).required(),
    startLocation: Joi.string().required(),
    endLocation: Joi.string().required(),
    latitude: Joi.number().required(),
    longitude: Joi.number().required()
});
const customerCallHistory = Joi.array().items(callHistory.keys({
    guide: Joi.string().required(),
    avatar: Joi.string().optional()
}));
const due = Joi.object({
    bookings: Joi.number().required(),
    due: Joi.number().required(),
    paid: Joi.number().required(),
});
const userResponse = response(user);
const otpResponse = response(otp);
const socialResponse = response(social);
const nearbyAppResponse = response(nearbyPlacesForApp);
const customerCallHistoryResponse = response(customerCallHistory);
const dueResponse = response(due);

// Guide responses.
const guide = Joi.object({
    id: Joi.string().required(),
    image: Joi.string().optional(),
    logo: Joi.string().optional(),
    gallery: Joi.array().optional(),
    name: Joi.string().required(),
    address_1: Joi.string().optional(),
    address_2: Joi.string().optional(),
    city: Joi.string().optional(),
    state: Joi.string().optional(),
    pinCode: Joi.string().optional(),
    countryCode: Joi.string().required(),
    phone: Joi.string().required(),
    email: Joi.string().required(),
    description: Joi.string().required(),
    monday: Joi.string().optional(),
    tuesday: Joi.string().optional(),
    wednesday: Joi.string().optional(),
    thursday: Joi.string().optional(),
    friday: Joi.string().optional(),
    saturday: Joi.string().optional(),
    sunday: Joi.string().optional(),
    info: Joi.string().required(),
    website: Joi.string().optional(),
    ccn: Joi.string().required(),
    expirydate: Joi.string().optional(),
    cvv: Joi.string().required(),
    wifiname: Joi.string().required(),
    restaurant_type: Joi.string().optional(),
    languages: Joi.array().items(Joi.string().valid(Object.values(Language))).optional(),
    latitude: Joi.number().optional(),
    longitude: Joi.number().optional(),
    status: Joi.boolean().required()
});
const duty = Joi.object({
    onDuty: Joi.boolean().required()
});
const guideCallHistory = Joi.array().items(callHistory.keys({
    user: Joi.string().required()
}));
const earnings = Joi.object({
    earnings: Joi.string().required(),
    received: Joi.string().required(),
    pending: Joi.string().required(),
    duration: Joi.string().required(),
    duty: Joi.string().required(),
    durationPercent: Joi.string().required(),
    dutyPercent: Joi.string().required(),
    yesterdaysDuration: Joi.string().required(),
    yesterdaysDuty: Joi.string().required()
});
const guideResponse = response(guide);
const dutyResponse = response(duty);
const guideCallHistoryResponse = response(guideCallHistory);
const earningsResponse = response(earnings);

// Portal responses.
const queries = Joi.array().items(Joi.object({
    id: Joi.string().required(),
    guide: Joi.alternatives().try(Joi.object({
        name: Joi.string().required(),
        phone: Joi.string().required(),
        email: Joi.string().required()
    }), null).optional(),
    user: Joi.alternatives().try(Joi.object({
        name: Joi.string().required(),
        phone: Joi.string().required(),
        email: Joi.string().required()
    }), null).optional(),
    type: Joi.string().valid(Object.values(QueryType)).required(),
    status: Joi.string().valid(Object.values(QueryStatus)).required(),
    body: Joi.string().required(),
    phone: Joi.string().required(),
    email: Joi.string().required(),
    name: Joi.string().required(),
    createdAt: Joi.string().required()
}));
const guides = Joi.array().items(guide);
const users = Joi.array().items(user);
const newGuide = Joi.object({
    id: Joi.string().required(),
    name: Joi.string().required(),
    countryCode: Joi.string().required(),
    phone: Joi.string().required(),
    email: Joi.string().required(),
    status: Joi.boolean().required(),
    rejected: Joi.boolean().required(),
    registredWhen: Joi.string().required(),
    activatedWhen: Joi.string().optional(),
    rejectedWhen: Joi.string().optional()
});
const newGuides = Joi.array().items(newGuide);

const newPartner = Joi.object({
    id: Joi.string().required(),
    name: Joi.string().required(),
    countryCode: Joi.string().required(),
    phone: Joi.string().required(),
    email: Joi.string().required(),
    address: Joi.string().optional(),
    state: Joi.string().optional(),
    pinCode: Joi.string().optional()
});
const newPartners = Joi.array().items(newPartner);

const stats = Joi.object({
    numberOfGuides: Joi.number().required(),
    guidesOnDuty: Joi.number().required(),
    callOrders: Joi.number().required(),
    callOrdersDelivered: Joi.number().required(),
    moneyReceived: Joi.number().required(),
    appOpens: Joi.number().required()
});
const nearbyAddResponse = response(nearbyPlace);
const nearbyResponse = response(nearbyPlaces);
const queriesResponse = response(queries);
const guidesResponse = response(guides);
const usersResponse = response(users);
const newGuidesResponse = response(newGuides);
const newPartnerResponse = response(newPartners);
const statsResponse = response(stats);


const handleError = (request, reply, err) => {
    request.log(['error'], err);
    reply({ message: 'INTERNAL SERVER ERROR' }).code(500);
};

export {
    generalResponse,
    userResponse,
    otpResponse,
    socialResponse,
    nearbyAppResponse,
    customerCallHistoryResponse,
    dueResponse,
    guideResponse,
    dutyResponse,
    guideCallHistoryResponse,
    earningsResponse,
    nearbyAddResponse,
    nearbyResponse,
    queriesResponse,
    guidesResponse,
    usersResponse,
    newGuidesResponse,
    newPartnerResponse,
    statsResponse,
    handleError
};
