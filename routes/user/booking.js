/**
 * Created by M on 05/01/18. With ❤
 */

import Joi from 'joi';
import * as _ from 'lodash';
import moment from 'moment';
import meegoEmitter from '../../events/meegoEmitter';
import eventNames from '../../events/constants';
import { randCallRequestToken, selectRandElements } from '../../utils/random';
import { User, Guide, CallRequest, Language, RequestStatus, NearbyPlace, NearbyPlaceType } from '../../models';
import { generalResponse, nearbyAppResponse, customerCallHistoryResponse, handleError } from '../utils/responses';
import requestCall from '../../api/callCentre';
import calculateDistanceToPlaces from '../../api/distanceMatrix';
import unQueueGuide from '../../queues/unQueueGuide';
import { geoConfig, callConfig } from '../../config.json';
import localMoment from '../utils/time';

const updateLocation = {
    method: 'POST',
    path: '/user/update-location',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'Using this endpoint, a user updates his current location (if he is logged in) after he opens the app and gets nearby places in return.',
        notes: 'This should not be authenticated.',
        validate: {
            payload: {
                id: Joi.string().optional(),
                latitude: Joi.number().required(),
                longitude: Joi.number().required(),
            }
        },
        handler: async (request, reply) => {
            const { id, latitude, longitude } = request.payload;
            try {
                if (id) {
                    // Updating the user's location if id present.
                    // That is, if the user has logged in.
                    await User.update({ _id: id }, { location: [longitude, latitude] });
                }
                // Get nearby places.
                let places = await NearbyPlace.find({
                    location: {
                        $nearSphere: {
                            $geometry: {
                                type: 'Point',
                                coordinates: [longitude, latitude]
                            },
                            $maxDistance: geoConfig.NEARBY_PLACE_RADIUS
                        }
                    }
                }).limit(30);

                if (!places.length) {
                    reply(_.mapValues(
                        _.mapKeys(
                            NearbyPlaceType,
                            placeType => placeType.toLowerCase()
                        ),
                        () => []
                    ));
                    return;
                }
                places = await calculateDistanceToPlaces(latitude, longitude, places);

                places = _.groupBy(
                    places.map(place => ({
                        name: place.name,
                        description: place.description,
                        image: place.image,
                        type: place.type,
                        distance: place.distance,
                        duration: place.duration,
                        latitude: place.location[1],
                        longitude: place.location[0]
                    })),
                    place => place.type
                );

                // We put in place an empty array if a
                // type has no places associated with it.
                // This ensures data consistency in the API.
                Object.values(NearbyPlaceType).forEach(type => {
                    if (!places[type]) {
                        places[type] = [];
                    }
                });

                // TODO add fare.

                reply(places);
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: nearbyAppResponse
    }
};

const makeBooking = {
    method: 'GET',
    path: '/user/call-booking',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'Endpoint responsible for call bookings generated by the customer app.',
        notes: 'This should be authenticated.',
        validate: {
            query: {
                id: Joi.string().required(),
                latitude: Joi.number().required(),
                longitude: Joi.number().required(),
                phone: Joi.string().required(),
                language: Joi.string().valid(Object.values(Language)).required()
            }
        },
        handler: async (request, reply) => {
            const {
                id, latitude, longitude, phone, language
            } = request.query;

            // Check if there's any unbilled balance for this user.
            const unbilledRequests = await CallRequest.find({
                user: id,
                status: RequestStatus.UNBILLED
            });

            if (unbilledRequests.length) {
                const unbilled = unbilledRequests.reduce((sum, { fare }) => sum + (fare - 0), 0);
                reply({ message: `BALANCE PENDING|${unbilled}` });
                return;
            }

            // If all went well, start the booking process.
            try {
                // Get 5 nearby available guides for this language.
                const guides = await Guide
                    .find({
                        languages: language,
                        onDuty: true,
                        onQueue: false,
                        location: {
                            $nearSphere: {
                                $geometry: {
                                    type: 'Point',
                                    coordinates: [longitude, latitude]
                                },
                                $maxDistance: geoConfig.GUIDE_CALL_BOOKING_RADIUS
                            }
                        }
                    })
                    .select('_id')
                    .limit(50);

                // If no nearby guides.
                if (!guides.length) {
                    reply({ message: 'NO GUIDE AVAILABLE' });
                    return;
                }

                // Generate guideIDs and a unique call request token.
                const guideIDs = guides.map(guide => guide.id);
                const requestToken = await randCallRequestToken();

                // Create a new booking request.
                const callRequest = await CallRequest.create({
                    user: id,
                    associatedGuides: guideIDs,
                    location: [longitude, latitude],
                    callCentreToken: requestToken,
                    phone,
                    language,
                    rate: `${callConfig.RATE}/${callConfig.TIME_UNIT_MINS}`,
                    guideRate: `${callConfig.GUIDE_RATE}/${callConfig.TIME_UNIT_MINS}`,
                    status: RequestStatus.CREATED
                });

                // Choose max of 5 guideIDs from the above guideID list.
                const selectedGuideIDs = selectRandElements(guideIDs, 5);

                // Get user and fare details (TODO Fare later).

                const user = await User.findById(id).select('name');

                const { CALL_REQUEST_EVENT } = eventNames;

                // Send booking to these 5 guides and wait for the first one to accept.
                // This request times out if no guides responded within the alloted time.
                meegoEmitter.emitToGuidesAndRegisterOnce(
                    CALL_REQUEST_EVENT,
                    requestToken,
                    selectedGuideIDs,
                    {
                        name: user.name,
                        fare: `${callConfig.GUIDE_RATE}/${callConfig.TIME_UNIT_MINS}mins`,
                        language: _.upperFirst(language)
                    },
                    callConfig.TIMEOUT * 1000,
                    async payload => {
                        if (!payload) {
                            // No guide responded in the alloted time.
                            reply({ message: 'NO GUIDE AVAILABLE' });
                            return;
                        }
                        try {
                            const { guideID, guidePhone } = payload;

                            // Save the accepted state.
                            await callRequest.acceptRequest(guideID);

                            // Connect both the parties using call centre API.
                            // Call the guide first though.
                            await requestCall(guidePhone, phone, requestToken);

                            const guide = guides.find(g => g.id === guideID);

                            // Set queued state for this guide so he doesn't get a booking.
                            await guide.queueGuide();

                            // Un queue this guide so he may get a booking after 2 hours.
                            unQueueGuide.queue(guideID);

                            reply({ message: 'OK' });
                        } catch (err) {
                            handleError(request, reply, err);
                        }
                    }
                );
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const callHistory = {
    method: 'GET',
    path: '/user/call-history',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'Using this endpoint, a user gets all his completed call bookings with the status of UNBILLED OR BILLED.',
        notes: 'Offset starts from 0. Incrementing the offset will get the next batch of 20 bookings.',
        validate: {
            query: {
                id: Joi.string().required(),
                offset: Joi.number().required()
            }
        },
        handler: async (request, reply) => {
            const { id, offset } = request.query;
            try {
                let callRequests = await CallRequest.find({
                    user: id,
                    $or: [
                        { status: RequestStatus.UNBILLED },
                        { status: RequestStatus.BILLED }
                    ]
                })
                    .sort('-createdAt')
                    .skip(offset * 20)
                    .limit(20)
                    .populate('guide');

                callRequests = _.map(callRequests, callRequest => {
                    const {
                        fare,
                        duration,
                        createdAt,
                        guide: { name, avatar },
                        language,
                        location
                    } = callRequest;
                    const [longitude, latitude] = location;
                    const createdAtMoment = localMoment(createdAt);
                    const durationMoment = moment().startOf('day').seconds(duration);
                    return {
                        id: callRequest.id,
                        status: callRequest.status,
                        fare: `Rs ${fare}/-`,
                        duration: `${durationMoment.minutes()} min(s) ${durationMoment.seconds()} second(s)`,
                        date: createdAtMoment.format('D MMM'),
                        year: createdAtMoment.format('YYYY'),
                        time: createdAtMoment.format('HH:mm'),
                        guide: name,
                        avatar,
                        language,
                        startLocation: 'Not Available',
                        endLocation: 'Not Available',
                        latitude,
                        longitude
                    };
                });
                reply(callRequests);
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: customerCallHistoryResponse
    }
};

export { updateLocation, makeBooking, callHistory };
