/**
 * Created by M on 17/04/17. With ❤
 */


import * as loginRoutes from './login';
import * as bookingRoutes from './booking';
import * as profileRoutes from './profile';
import * as paymentRoutes from './payment';
import queryRoute from './query';
import statsRoute from './stats';

export default Object.values(loginRoutes)
    .concat(Object.values(bookingRoutes))
    .concat(Object.values(profileRoutes))
    .concat(Object.values(paymentRoutes))
    .concat([queryRoute, statsRoute]);
