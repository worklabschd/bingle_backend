/**
 * Created by M on 12/01/18. With ❤
 */

import Joi from 'joi';
import { Query, QueryType } from '../../models';
import { generalResponse, handleError } from '../utils/responses';


const receiveUserQuery = {
    method: 'POST',
    path: '/user/query',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User submits a support query here.',
        notes: 'This should be authenticated.',
        validate: {
            payload: {
                id: Joi.string().required(),
                name: Joi.string().required(),
                phone: Joi.string().required(),
                email: Joi.string().email().required(),
                body: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    id, name, phone, email, body
                } = request.payload;
                await Query.create({
                    user: id,
                    type: QueryType.USER_QUERY,
                    name,
                    phone,
                    email,
                    body
                });
                reply({ message: 'OK' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

export default receiveUserQuery;
