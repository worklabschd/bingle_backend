import Joi from 'joi';
import { CallRequest, RequestStatus, UserReceipt } from '../../models';
import { generalResponse, dueResponse, handleError } from '../utils/responses';
import { capturePayment } from '../../api/razorpay';

const getDue = {
    method: 'GET',
    path: '/user/payment-due',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User gets his payment from here.',
        notes: 'This should be authenticated.',
        validate: {
            query: {
                id: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const { id } = request.query;
                const bookings = await CallRequest.count({
                    user: id,
                    $or: [
                        { status: RequestStatus.UNBILLED },
                        { status: RequestStatus.BILLED }
                    ]
                });
                const unbilledBookings = await CallRequest.find({
                    user: id,
                    status: RequestStatus.UNBILLED
                });
                const due = unbilledBookings.reduce((acc, booking) => acc + (booking.fare - 0), 0);
                const receipts = await UserReceipt.find({
                    user: id
                });
                const paid = receipts.reduce((acc, receipt) => acc + (receipt.amount - 0), 0);
                reply({ bookings, due, paid });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: dueResponse
    }
};

const getPaymentScreen = {
    method: 'GET',
    path: '/user/make-payment',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User gets served the razor pay screen from here.',
        notes: 'This should be authenticated.',
        validate: {
            query: {
                link: Joi.string().required(),
                name: Joi.string().required(),
                email: Joi.string().required(),
                phone: Joi.string().required(),
                amount: Joi.string().required(),
                paymentGateway: Joi.string().valid(['razorpay']).required()
            }
        },
        handler: async (request, reply) => {
            const {
                link,
                name,
                email,
                phone,
                amount,
                paymentGateway
            } = request.query;
            reply.view(paymentGateway, {
                link: link.replace(' ', '+'),
                name,
                email,
                phone,
                amount: (amount - 0) * 100
            });
        }
    }
};

const makePayment = {
    method: 'POST',
    path: '/user/make-payment',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User payment is confirmed by our backend here.',
        notes: 'This should be authenticated.',
        validate: {
            payload: {
                id: Joi.string().required(),
                paymentID: Joi.string().required(),
                amount: Joi.number().required(),
                paymentGateway: Joi.string().valid(['razorpay']).required()
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    id, paymentID, amount, paymentGateway
                } = request.payload;
                switch (paymentGateway) {
                // Acknowledge this payment in Razorpay.
                case 'razorpay':
                    await UserReceipt.create({
                        user: id,
                        amount,
                        paymentGateway,
                        paymentID,
                        // Do the actual acknowledgement and save return values.
                        ...(await capturePayment(paymentID, amount))
                    });
                    await CallRequest.update({
                        user: id,
                        status: RequestStatus.UNBILLED
                    }, { status: RequestStatus.BILLED }, { multi: true });
                    reply({ message: 'OK' });
                    break;

                default:
                    reply({ message: 'INVALID PAYMENT GATEWAY' }).code(400);
                }
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

export { getDue, makePayment, getPaymentScreen };
