/**
 * Created by M on 06/05/17. With ❤
 */

import Joi from 'joi';
import bcrypt from 'bcrypt';
import { User } from '../../models';
import { generalResponse, userResponse, otpResponse, socialResponse, handleError } from '../utils/responses';
import { sendOTP, retryOTP, verifyOTP } from '../../api/otp';
import { facebookLogin, googleLogin } from '../../api/social';
import { sendForgotPasswordEmail } from '../utils/email';
import {
    createToken,
    verifyToken,
    EXPIRED_TOKEN,
    INVALID_TOKEN
} from '../utils/token';

const login = {
    method: 'POST',
    path: '/user/login',
    config: {
        tags: ['user', 'api'],
        description: 'User logs in inside the app using this endpoint.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                email: Joi.string().email().required(),
                password: Joi.string().required(),
            }
        },
        handler: async (request, reply) => {
            try {
                const { email, password } = request.payload;
                const user = await User.findOne({ email });
                if (!user) {
                    reply({ message: 'INVALID EMAIL' }).code(400);
                } else if(user.blocked){
                    reply({ message: 'USER BLOCKED' }).code(400);
                } else if (user.comparePassword(password)) {
                    reply({
                        id: user.id,
                        name: user.name,
                        email: user.email,
                        countryCode: user.countryCode,
                        phone: user.phone,
                        age: user.age,
                        gender: user.gender,
                        avatar: user.avatar
                    });
                } else {
                    reply({ message: 'INVALID PASSWORD' }).code(400);
                }
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: userResponse
    }
};

const register = {
    method: 'POST',
    path: '/user/register',
    config: {
        tags: ['user', 'api'],
        description: 'User registers inside the app using this endpoint.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                name: Joi.optional(),
                countryCode: Joi.string().required(),
                phone: Joi.string().required(),
                email: Joi.string().email().required(),
                password: Joi.string().required(),
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    email, password, countryCode, phone
                } = request.payload;
                const name = request.payload.name || 'Meego Traveller';
                const oldUser = await User.findOne({ email });
                if (oldUser) {
                    reply({ message: 'EMAIL TAKEN' }).code(400);
                    return;
                }
                const hashed = bcrypt.hashSync(password, 10);
                const user = await User.create({
                    email, password: hashed, countryCode, phone, name
                });
                if (user) {
                    reply({
                        id: user.id,
                        name,
                        countryCode,
                        phone,
                        email
                    });
                }
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: userResponse
    }
};

const validateEmail = {
    method: 'GET',
    path: '/user/validate-email',
    config: {
        tags: ['user', 'api'],
        description: 'Validates uniqueness of a user\'s email.',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                email: Joi.string().email().required(),
            }
        },
        handler: async (request, reply) => {
            try {
                const { email } = request.query;
                const user = await User.findOne({ email });
                if (!user) {
                    reply({ message: 'OK' });
                } else {
                    reply({ message: 'EMAIL TAKEN' }).code(400);
                }
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const sendOTPHandler = {
    method: 'GET',
    path: '/user/send-otp',
    config: {
        tags: ['user', 'api'],
        description: 'Used for sending OTP to the user for phone verification. (On the condition that the phone number is unique)',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                countryCode: Joi.string().required(),
                phone: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const { countryCode, phone } = request.query;
                sendOTP(countryCode, phone, err => {
                    if (err) {
                        handleError(request, reply, err);
                    } else {
                        reply({ message: 'OK' });
                    }
                });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const retryOTPHandler = {
    method: 'GET',
    path: '/user/resend-otp',
    config: {
        tags: ['user', 'api'],
        description: 'Used for re sending OTP to the user for phone verification in case the earlier attempt fails.',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                countryCode: Joi.string().required(),
                phone: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const { countryCode, phone } = request.query;
                retryOTP(countryCode, phone, err => {
                    if (err) {
                        handleError(request, reply, err);
                    } else {
                        reply({ message: 'OK' });
                    }
                });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const verifyOTPHandler = {
    method: 'GET',
    path: '/user/verify-otp',
    config: {
        tags: ['user', 'api'],
        description: 'Used for veryfing OTP sent by the user.',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                countryCode: Joi.string().required(),
                phone: Joi.string().required(),
                otp: Joi.number().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const { countryCode, phone, otp } = request.query;
                verifyOTP(countryCode, phone, otp, (err, status) => {
                    if (err) {
                        handleError(request, reply, err);
                        return;
                    }
                    switch (status) {
                    case 'otp_verified':
                        reply({ message: 'OK', status: 'OTP_VALID' });
                        break;
                    case 'otp_not_verified':
                        reply({ message: 'ERROR', status: 'OTP_INVALID' });
                        break;
                    case 'otp_expired':
                        reply({ message: 'ERROR', status: 'OTP_EXPIRED' });
                        break;
                    case 'mobile_not_found':
                        reply({ message: 'ERROR', status: 'MOBILE_NOT_FOUND' });
                        break;
                    case 'invalid_mobile':
                        reply({ message: 'ERROR', status: 'INVALID_MOBILE' });
                        break;
                    default:
                        reply({ message: 'ERROR', status });
                    }
                });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: otpResponse
    }
};

const socialLogin = {
    method: 'POST',
    path: '/user/social-login',
    config: {
        tags: ['user', 'api'],
        description: 'Used for social login. Takes a OAuth token and a type. Response may not have all the user details depending on the login status.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                type: Joi.string().valid(['facebook', 'google']).required(),
                token: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const { type, token } = request.payload;
                let resp;
                if (type === 'facebook') {
                    resp = await facebookLogin(token);
                } else {
                    resp = await googleLogin(token);
                }
                const existingUser = await User.findOne({ email: resp.email });
                // If this is a new user, forward details
                // fetched from social media and move on to OTP.
                if (!existingUser) {
                    reply({
                        status: 'NEW',
                        email: resp.email,
                        name: resp.name,
                        gender: resp.gender,
                        age: resp.birthday
                    });
                    return;
                }
                // If user is already registered in our system,
                // fetch all the details and proceed with login.
                reply({
                    status: 'EXISTING',
                    id: existingUser.id,
                    email: existingUser.email,
                    name: existingUser.name,
                    gender: existingUser.gender,
                    age: existingUser.age,
                    avatar: existingUser.avatar,
                    countryCode: existingUser.countryCode,
                    phone: existingUser.phone
                });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: socialResponse
    }
};

const forgotPassword = {
    method: 'GET',
    path: '/user/forgot-password',
    config: {
        tags: ['user', 'api'],
        description: 'A user sends his email and we send an email with further instructions on how to reset their password.',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                email: Joi.string().email().required(),
            }
        },
        handler: async (request, reply) => {
            const { email } = request.query;
            try {
                const user = await User.findOne({ email });
                if (!user) {
                    reply({ message: 'INVALID EMAIL' }).code(400);
                    return;
                }
                const token = await createToken(user.id);
                await sendForgotPasswordEmail(email, token);
                reply({ message: 'OK' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const getResetPassword = {
    method: 'GET',
    path: '/user/reset-password',
    config: {
        tags: ['user', 'api'],
        description: 'Handler that serves the reset password form.',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                token: Joi.string().required(),
            }
        },
        handler: async (request, reply) => {
            const { token } = request.query;
            try {
                const decoded = await verifyToken(token);
                if (decoded === EXPIRED_TOKEN) {
                    reply({ message: 'TOKEN EXPIRED' }).status(400);
                    return;
                }
                if (decoded === INVALID_TOKEN) {
                    reply({ message: 'TOKEN INVALID' }).status(400);
                    return;
                }
                const user = await User.findById(decoded.data);
                if (!user) {
                    reply({ message: 'TOKEN INVALID' }).status(400);
                    return;
                }
                reply.view('resetPassword', {
                    token
                });
            } catch (err) {
                handleError(request, reply, err);
            }
        }
    }
};

const resetPassword = {
    method: 'POST',
    path: '/user/reset-password',
    config: {
        tags: ['user', 'api'],
        description: 'Handler that resets the user\'s password.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                password: Joi.string().required(),
                token: Joi.string().required(),
            }
        },
        handler: async (request, reply) => {
            const { token, password } = request.payload;
            try {
                const decoded = await verifyToken(token);
                if (decoded === EXPIRED_TOKEN) {
                    reply({ message: 'TOKEN EXPIRED' }).status(400);
                    return;
                }
                if (decoded === INVALID_TOKEN) {
                    reply({ message: 'TOKEN INVALID' }).status(400);
                    return;
                }
                const user = await User.findById(decoded.data);
                if (!user) {
                    reply({ message: 'TOKEN INVALID' }).status(400);
                    return;
                }
                const hashed = bcrypt.hashSync(password, 10);
                user.password = hashed;
                await user.save();
                reply({ message: 'OK' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

export {
    login,
    register,
    validateEmail,
    sendOTPHandler,
    retryOTPHandler,
    verifyOTPHandler,
    socialLogin,
    forgotPassword,
    getResetPassword,
    resetPassword
};
