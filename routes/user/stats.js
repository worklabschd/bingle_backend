import { incrementAppOpens } from '../../store';
import { generalResponse, handleError } from '../utils/responses';

const appOpened = {
    method: 'PUT',
    path: '/user/app-opened',
    config: {
        tags: ['user', 'api'],
        description: 'User increments a global counter in the system whenever he opens his app.',
        notes: 'This should be unauthenticated.',
        handler: async (request, reply) => {
            try {
                await incrementAppOpens();
                reply({ message: 'OK' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

export default appOpened;
