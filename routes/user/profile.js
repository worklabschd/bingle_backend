/**
 * Created by M on 21/01/18. With ❤
 */

import Joi from 'joi';
import { User } from '../../models';
import { generalResponse, handleError } from '../utils/responses';
import { deleteFile, saveImage, IMAGE_FOLDER } from '../../utils/fs';

const editUserProfile = {
    method: 'POST',
    path: '/user/edit-profile',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User can change his profile details from here.',
        notes: 'This should be authenticated.',
        validate: {
            payload: {
                id: Joi.string().required(),
                name: Joi.string().optional(),
                email: Joi.string().email().optional(),
                age: Joi.number().optional(),
                gender: Joi.string().valid(['male', 'female']).optional()
            }
        },
        handler: async (request, reply) => {
            try {
                await User.update({
                    _id: request.payload.id
                }, request.payload);
                reply({ message: 'OK' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const editUserAvatar = {
    method: 'POST',
    path: '/user/edit-avatar',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User can change his profile picture from here.',
        notes: 'This should be authenticated. Avatar is a base64 encoded image.',
        payload: {
            maxBytes: 1000 * 1000 * 40 // 40MB file size limit for uploads.
        },
        validate: {
            payload: {
                id: Joi.string().required(),
                avatar: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const { id, avatar } = request.payload;
                const user = await User.findById(id);

                // If a previous avatar exists, delete it.
                if (user.avatar) {
                    await deleteFile('user', user.avatar.split('user/')[1]);
                }
                // Save the avatar and user.
                user.avatar = `/${IMAGE_FOLDER}/user/${await saveImage('user', avatar)}`;
                await user.save();

                reply({ message: user.avatar });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};


const BlockProfile = {
    method: 'POST',
    path: '/user/block',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User can change his profile details from here.',
        notes: 'This should be authenticated.',
        validate: {
            payload: {
                id: Joi.string().required(),
            }
        },
        handler: async (request, reply) => {
            try {
                await User.update({
                    _id: request.payload.id
                }, { $set: { 'blocked': true } });
                reply({ message: 'User Blocked' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const UnBlockProfile = {
    method: 'POST',
    path: '/user/unblock',
    config: {
        tags: ['user', 'api', 'auth'],
        description: 'User can change his profile details from here.',
        notes: 'This should be authenticated.',
        validate: {
            payload: {
                id: Joi.string().required(),
            }
        },
        handler: async (request, reply) => {
            try {
                await User.update({
                    _id: request.payload.id
                }, { $set: { 'blocked': false } });
                reply({ message: 'User Unblocked' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};


export { editUserProfile, editUserAvatar, UnBlockProfile, BlockProfile };
