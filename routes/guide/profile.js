/**
 * Created by M on 27/01/18. With ❤
 */

import Joi from 'joi';
import moment from 'moment';
import {
    generalResponse,
    handleError
} from '../utils/responses';
import { Guide, Language } from '../../models';
import {
    saveImage,
    deleteFile,
    IMAGE_FOLDER
} from '../../utils/fs';

var base64Img = require('base64-img');

const editProfile = {
    method: 'POST',
    path: '/guide/edit-profile',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Guide app uses this endpoint to update a tour guide\'s profile. ',
        notes: 'avatar and license are both base64 encoded images and both are optional.',
        payload: {
            maxBytes: 1000 * 1000 * 40 // 40MB file size limit for uploads.
        },
        validate: {
            payload: {
                id: Joi.string().required(),
                name: Joi.string().required(),
                image: Joi.string().optional(),
                logo: Joi.string().optional(),
                gallery: Joi.array().optional(),
		restaurant_type: Joi.string().required(),
                address_1: Joi.string().required(),
                address_2: Joi.string().optional(),
                city: Joi.string().required(),
                state: Joi.string().required(),
                country: Joi.string().required(),
                pinCode: Joi.string().required(),
                countryCode: Joi.string().required(),
                phone: Joi.string().required(),
                email: Joi.string().required(),
                description: Joi.string().required(),
                monday: Joi.string().optional(),
                tuesday: Joi.string().optional(),
                wednesday: Joi.string().optional(),
                thursday: Joi.string().optional(),
                friday: Joi.string().optional(),
                saturday: Joi.string().optional(),
                sunday: Joi.string().optional(),
                info: Joi.string().required(),
                website: Joi.string(),
                ccn: Joi.string().required(),
                expirydate: Joi.string().required(),
                cvv: Joi.string().required(),
                wifiname: Joi.string().required(),
                latitude: Joi.number().min(-90).max(90).optional(),
                longitude: Joi.number().min(-180).max(180).optional(),
            }
        },
        handler: async (request, reply) => {
            try {
                const { id } = request.payload;
                const guide = await Guide.findById(id);

                if (request.payload.logo) {
                    var logoName = new Date().getTime() + '-' + Math.floor(Math.random() * 10000)
                    var logoInfo = base64Img.imgSync(request.payload.logo, __dirname + '/../../assets/images/logo/', logoName);
                    request.payload.logo = 'images/logo/' + logoName + '.' + logoInfo.split('.')[1]
                }

                if (request.payload.image) {

                    var imageName = new Date().getTime() + '-' + Math.floor(Math.random() * 10000)
                    var imageInfo = base64Img.imgSync(request.payload.image, __dirname + '/../../assets/images/profileImages/', imageName);
                    request.payload.image = 'images/profileImages/' + imageName + '.' + imageInfo.split('.')[1]
                }

                if (request.payload.gallery) {
                    for (let i = 0; i < request.payload.gallery.length; i++) {
                        var galName = new Date().getTime() + '-' + Math.floor(Math.random() * 10000)
                        var galInfo = base64Img.imgSync(request.payload.gallery[i], __dirname + '/../../assets/images/gallery/', galName);
                        galInfo.split('.')[1]
                        request.payload.gallery[i] = 'images/gallery/' + logoName + '.' + galInfo.split('.')[1]
                    }
                }

                await guide.update({
                    ...request.payload,
                });
                reply({ message: `Guide Updated` });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }

};
const deleteProfile = {
    method: 'DELETE',
    path: '/guide/{id}',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Guide app uses this endpoint to delete a tour guide\'s profile. ',
        notes: 'avatar and license are both base64 encoded images and both are optional.',
        validate: {
            params: {
                id: Joi.string().required(),
            }
        },
        handler: async (request, reply) => {
            try {
                const { id } = request.params;
                const guide = await Guide.update({'_id':id},{ 'status': false });
                reply({ message: `Guide Removed` });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const getProfile = {
    method: 'GET',
    path: '/guide/{id}',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Guide app uses this endpoint to delete a tour guide\'s profile. ',
        notes: 'avatar and license are both base64 encoded images and both are optional.',
        validate: {
            params: {
                id: Joi.string().required(),
            }
        },
        handler: async (request, reply) => {
            try {
                const { id } = request.params;
                const guide = await Guide.findOne({ '_id': id });

                reply({ message: `Guide Data`, data: guide });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};


export { editProfile, deleteProfile, getProfile };
