import Joi from 'joi';
import moment from 'moment';
import { Guide, NewGuide } from '../../models';
import { generalResponse, guideResponse, handleError } from '../utils/responses';
import { sendOTP } from '../../api/otp';
var base64Img = require('base64-img');

const login = {
    method: 'POST',
    path: '/guide/login',
    config: {
        tags: ['guide', 'api'],
        description: 'Tour guide logs in inside the app using this endpoint.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                email: Joi.string().required(),
                password: Joi.string().required(),
            }
        },
        handler: async (request, reply) => {


            try {
                const { email, password } = request.payload;
                const guide = await Guide.findOne({ email , status:{ $ne:false } });
                if (!guide) {
                    reply({ message: 'INVALID EMAIL ADDRESS OR PASSWORD' }).code(400);
                } else if (guide.password == password) {
                    reply({ message: `Guide Data`, data:guide });

                } else {
                    reply({ message: 'INVALID PASSWORD' }).code(400);

                }
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const validateEmail = {
    method: 'GET',
    path: '/guide/validateEmail',
    config: {
        tags: ['guide', 'api'],
        description: 'This validates the email of the tour guide for uniqueness',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                email: Joi.string().required(),
            }
        },
        handler: async (request, reply) => {
            try {
                const { email } = request.query;
                const user = await Guide.findOne({ email });
                if (!user) {
                    reply({ message: 'OK' });
                } else {
                    reply({ message: 'INVALID' }).code(400);
                }
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const register = {
    method: 'POST',
    path: '/guide/register',
    config: {
        tags: ['guide', 'api'],
        description: 'Guide registers inside the app using this endpoint. This registration is subject to verification.',
        notes: 'This should be unauthenticated.',
        validate: {
            payload: {
                name: Joi.string().required(),
                image: Joi.string().optional(),
                logo: Joi.string().optional(),
                gallery: Joi.array().optional(),
                address_1: Joi.string().required(),
                address_2: Joi.string().optional(),
                city: Joi.string().required(),
                state: Joi.string().required(),
                country: Joi.string().required(),
                pinCode: Joi.string().required(),
                countryCode: Joi.string().required(),
                phone: Joi.string().required(),
                email: Joi.string().required(),
                password: Joi.string().required(),
                description: Joi.string().required(),
                monday: Joi.string().optional(),
                tuesday: Joi.string().optional(),
                wednesday: Joi.string().optional(),
                thursday: Joi.string().optional(),
                friday: Joi.string().optional(),
                saturday: Joi.string().optional(),
                sunday: Joi.string().optional(),
                info: Joi.string().required(),
                website: Joi.string().required(),
                ccn: Joi.string().required(),
                expirydate: Joi.string().optional(),
                cvv: Joi.string().required(),
                wifiname: Joi.string().required(),
                wifisecret: Joi.string().optional(),
                restaurant_type: Joi.string().required(),
                latitude: Joi.number().min(-90).max(90).optional(),
                longitude: Joi.number().min(-180).max(180).optional(),

            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    email, password, wifiname
                } = request.payload;

                // Checks if the tour guide has already registered in our system.
                // By checking email || (countryCode && phone)
                const oldRegisteration = await NewGuide.findOne({
                    email: email,
                    status: true
                });
                if (oldRegisteration) {
                    reply({ message: 'ALREADY REGISTERED' }).code(400);
                    return;
                }

                // Checks if the tour guide is already present in our system.
                // By again checking email || (countryCode && phone)
                const guide = await Guide.findOne({ email: email });
                if (guide) {
                    reply({ message: 'ACCOUNT EXISTS' }).code(400);
                    return;
                }

                const wifiexits = await Guide.findOne({ wifiname: wifiname });
                if (wifiexits) {
                    reply({ message: 'WIFI NAME EXISTS' }).code(400);
                    return;
                }

		var logoName =  new Date().getTime()+'-'+Math.floor(Math.random()*10000)
		var logoInfo = base64Img.imgSync(request.payload.logo, __dirname+'/../../assets/images/logo/', logoName);
		request.payload.logo = 'images/logo/'+logoName+'.'+logoInfo.split('.')[1]


		var imageName =  new Date().getTime()+'-'+Math.floor(Math.random()*10000)
		var imageInfo = base64Img.imgSync(request.payload.image, __dirname+'/../../assets/images/profileImages/', imageName);
		request.payload.image = 'images/profileImages/'+imageName+'.'+imageInfo.split('.')[1]

		for(let i=0; i< request.payload.gallery.length;i++)
		{
			var galName =  new Date().getTime()+'-'+Math.floor(Math.random()*10000)
			var galInfo = base64Img.imgSync(request.payload.gallery[i], __dirname+'/../../assets/images/gallery/', galName);
			galInfo.split('.')[1]
			request.payload.gallery[i] = 'images/gallery/'+logoName+'.'+galInfo.split('.')[1]
		}

		// Add a new registration.
                const newGuide = await Guide.create({
                    ...request.payload,
                });

                reply({ message: 'SUCCESSFULLY REGISTERED' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const sendOTPHandler = {
    method: 'GET',
    path: '/guide/send-otp',
    config: {
        tags: ['guide', 'api'],
        description: 'Used for sending OTP to the user for phone verification. (On the condition that the phone number is unique)',
        notes: 'This should be unauthenticated.',
        validate: {
            query: {
                countryCode: Joi.string().required(),
                phone: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const { countryCode, phone } = request.query;
                const guide = await Guide.findOne({ countryCode, phone });
                if (!guide) {
                    sendOTP(countryCode, phone, err => {
                        if (err) {
                            handleError(request, reply, err);
                            return;
                        }
                        reply({ message: 'OK' });
                    });
                    return;
                }
                reply({ message: 'PHONE NUMBER TAKEN' }).code(400);
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};


export { login, validateEmail, register, sendOTPHandler };
