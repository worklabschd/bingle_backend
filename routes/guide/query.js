/**
 * Created by M on 12/01/18. With ❤
 */

import Joi from 'joi';
import { Query, QueryType } from '../../models';
import { generalResponse, handleError } from '../utils/responses';


const receiveGuideQuery = {
    method: 'POST',
    path: '/guide/query',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Guide submits a support query here.',
        notes: 'This should be authenticated.',
        validate: {
            payload: {
                id: Joi.string().required(),
                name: Joi.string().required(),
                phone: Joi.string().required(),
                email: Joi.string().email().required(),
                body: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    id, name, phone, email, body
                } = request.payload;
                await Query.create({
                    guide: id,
                    type: QueryType.GUIDE_QUERY,
                    name,
                    phone,
                    email,
                    body
                });
                reply({ message: 'OK' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

export default receiveGuideQuery;
