/**
 * Created by M on 12/10/17. With ❤
 */

import * as loginRoutes from './login';
import * as bookingRoutes from './booking';
import * as bookingRoutesprofileRoute from './profile';
import queryRoute from './query';

export default Object.values(loginRoutes)
.concat(Object.values(bookingRoutes))
.concat(Object.values(bookingRoutesprofileRoute))
.concat([queryRoute]);
