/**
 * Created by M on 05/01/18. With ❤
 */

import Joi from 'joi';
import * as _ from 'lodash';
import moment from 'moment';
import numeral from 'numeral';
import {
    dutyResponse,
    generalResponse,
    guideCallHistoryResponse,
    earningsResponse,
    handleError
} from '../utils/responses';
import { Guide, CallRequest, RequestStatus, GuideReceiptStatus } from '../../models';
import localMoment from '../utils/time';

const updateLocation = {
    method: 'POST',
    path: '/guide/update-location',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Using this endpoint, a guide updates his current location after he opens the app. He gets his onDuty status in return.',
        notes: 'This should be authenticated.',
        validate: {
            payload: {
                id: Joi.string().required(),
                latitude: Joi.number().required(),
                longitude: Joi.number().required(),
            }
        },
        handler: async (request, reply) => {
            const { id, latitude, longitude } = request.payload;
            try {
                const guide = await Guide.findByIdAndUpdate(
                    id,
                    { location: [longitude, latitude] }
                );
                reply({ onDuty: guide.onDuty });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: dutyResponse
    }
};

const toggleDuty = {
    method: 'POST',
    path: '/guide/toggle-duty',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Using this endpoint, a guide changes his on duty status so he can go on and off duty.',
        notes: 'This should be authenticated.',
        validate: {
            payload: {
                id: Joi.string().required(),
                onDuty: Joi.boolean().required()
            }
        },
        handler: async (request, reply) => {
            const { id, onDuty } = request.payload;
            try {
                await Guide.update({ _id: id }, { onDuty, toggledDuty: new Date() });
                reply({ message: 'OK' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const callHistory = {
    method: 'GET',
    path: '/guide/call-history',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Using this endpoint, a guide gets all his completed call bookings with the status of UNBILLED OR BILLED.',
        notes: 'Offset starts from 0. Incrementing the offset will get the next batch of 20 bookings.',
        validate: {
            query: {
                id: Joi.string().required(),
                offset: Joi.number().required()
            }
        },
        handler: async (request, reply) => {
            const { id, offset } = request.query;
            try {
                let callRequests = await CallRequest.find({
                    guide: id,
                    $or: [
                        { status: RequestStatus.UNBILLED },
                        { status: RequestStatus.BILLED }
                    ]
                })
                    .sort('-createdAt')
                    .skip(offset * 20)
                    .limit(20)
                    .populate('user')
                    .populate('guideReceipt');

                callRequests = _.map(callRequests, callRequest => {
                    const {
                        duration,
                        createdAt,
                        user: { name },
                        language,
                        location,
                        guideReceipt: { amount }
                    } = callRequest;
                    const [longitude, latitude] = location;
                    const createdAtMoment = localMoment(createdAt);
                    const durationMoment = moment().startOf('day').seconds(duration);
                    return {
                        id: callRequest.id,
                        status: callRequest.status,
                        fare: `Rs ${amount}/-`,
                        duration: `${durationMoment.minutes()} min(s) ${durationMoment.seconds()} second(s)`,
                        date: createdAtMoment.format('D MMM'),
                        year: createdAtMoment.format('YYYY'),
                        time: createdAtMoment.format('HH:mm'),
                        user: name,
                        language,
                        startLocation: 'Not Available',
                        endLocation: 'Not Available',
                        latitude,
                        longitude
                    };
                });
                reply(callRequests);
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: guideCallHistoryResponse
    }
};

const earnings = {
    method: 'GET',
    path: '/guide/earnings',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Using this endpoint, a guide gets all his completed call bookings with the status of UNBILLED OR BILLED.',
        notes: 'Offset starts from 0. Incrementing the offset will get the next batch of 20 bookings.',
        validate: {
            query: {
                id: Joi.string().required(),
                timeSpan: Joi.string().valid(['weekly', 'monthly']).required()
            }
        },
        handler: async (request, reply) => {
            const { id, timeSpan } = request.query;
            try {
                const today = moment().startOf('day');
                const yesterday = today.clone().subtract(1, 'days');
                const currentThreshold = today.clone();
                let previousThreshold;

                // If the guide wants a monthly comparison.
                if (timeSpan === 'monthly') {
                    currentThreshold.subtract(1, 'months').add(1, 'days');
                    previousThreshold = currentThreshold.clone().subtract(1, 'months');
                } else {
                    // If the guide wants a weekly comparison.
                    currentThreshold.subtract(6, 'days');
                    previousThreshold = currentThreshold.clone().subtract(7, 'days');
                }

                const callRequests = await CallRequest.find({
                    guide: id,
                    $or: [
                        { status: RequestStatus.UNBILLED },
                        { status: RequestStatus.BILLED }
                    ]
                })
                    .populate('guideReceipt')
                    .select('fare duration createdAt')
                    .sort('-createdAt');

                /* eslint-disable */
                let currentDuration = 0,
                    previousDuration = 0,
                    todaysDuration = 0,
                    yesterdaysDuration = 0,
                    currentDuties = 0,
                    previousDuties = 0,
                    todaysDuties = 0,
                    yesterdaysDuties = 0,
                    earnings = 0,
                    received = 0,
                    totalDuration = 0,
                    totalDuties = 0;
                /* eslint-enable */

                callRequests.forEach(callRequest => {
                    const { duration, createdAt, guideReceipt: { amount, status } } = callRequest;
                    const createdAtMoment = moment(createdAt);

                    // For yesterday's comparison.
                    if (createdAtMoment.isSameOrAfter(today)) {
                        todaysDuration += (duration - 0);
                        todaysDuties += 1;
                    } else if (createdAtMoment.isSameOrAfter(yesterday)) {
                        yesterdaysDuration += (duration - 0);
                        yesterdaysDuties += 1;
                    }

                    // For timeSpan comparison.
                    if (createdAtMoment.isSameOrAfter(currentThreshold)) {
                        currentDuration += (duration - 0);
                        currentDuties += 1;
                    } else if (createdAtMoment.isSameOrAfter(previousThreshold)) {
                        previousDuration += (duration - 0);
                        previousDuties += 1;
                    }

                    // Lifetime statistics.
                    // Duration and duties.
                    totalDuration += (duration - 0);
                    totalDuties += 1;

                    // To calculate guide's earnings.
                    if (status === GuideReceiptStatus.PAID) {
                        received += (amount - 0);
                    }
                    earnings += (amount - 0);
                });

                const percentFormatter = (current, previous) => {
                    if (!previous) {
                        return numeral(current).format('%0a');
                    }
                    return numeral((current / previous) - 1).format('%0a');
                };
                reply({
                    earnings: `Rs ${numeral(earnings).format('0,000')}`,
                    received: `Rs ${numeral(received).format('0,000')}`,
                    pending: `Rs ${numeral(earnings - received).format('0,000')}`,
                    duration: totalDuration ? moment.duration(currentDuration, 'seconds').humanize() : '0 seconds',
                    duty: `${totalDuties}`,
                    durationPercent: percentFormatter(currentDuration, previousDuration),
                    dutyPercent: percentFormatter(currentDuties, previousDuties),
                    yesterdaysDuration: percentFormatter(todaysDuration, yesterdaysDuration),
                    yesterdaysDuty: percentFormatter(todaysDuties, yesterdaysDuties)
                });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: earningsResponse
    }
};


export { updateLocation, toggleDuty, callHistory, earnings };
