// Web hooks for call centre API.

import { generalResponse, handleError } from '../utils/responses';
import { CallRequest, RequestStatus, GuideReceipt, GuideReceiptStatus } from '../../models';

// This webhook is called by prp to signal answer or hangup of a call.
const hungUpHook = {
    method: 'GET',
    path: '/hooks/hungup',
    config: {
        tags: ['guide', 'api', 'auth'],
        description: 'Web hook for call centre API.',
        notes: 'After a call is hung up or answered. This end point should get called.',
        handler: async (request, reply) => {
            try {
                console.log(request.query);
                /* eslint-disable */
                let {
                    type,
                    duration_out,
                    call_status_out,
                    call_status_in,
                    call_in,
                    outgoing_picked,
                    src
                } = request.query;
                type = type.toLowerCase();
                call_status_out = call_status_out ? call_status_out.toLowerCase() : '';
                call_status_in = call_status_in ? call_status_in.toLowerCase() : '';
                call_in = call_in ? call_in.toLowerCase() : '';
                let call_out = request.query['call _out'] ? request.query['call _out'].toLowerCase() : '';
                duration_out -= 0;
                // Just to make the call was infact received by both the parties.
                if (
                  type === 'hangup' && (
                    (call_status_out === 'answered' && call_status_in === 'answered') ||
                    (call_out === 'answered' && call_in === 'answered')
                  )) {
                    let callRequests = await CallRequest.find({ phone: outgoing_picked })
                    .populate(
                      'guide',
                      null,
                      { phone: src }
                    )
                    .sort('-createdAt');

                    console.log('callRequests', callRequests)
                    callRequests = callRequests.filter(({ guide }) => !!guide);

                    // Processing the latest call for this guide/user pair.
                    const callRequest = callRequests[0];
                    if (callRequest) {
                        // Attach a fare to this call request.
                        callRequest.fare = calculateFare(duration_out, callRequest.rate);
                        if (callRequest.fare) {
                            callRequest.status = RequestStatus.UNBILLED;
                        } else {
                            callRequest.status = RequestStatus.BILLED;
                        }
                        callRequest.duration = duration_out - 0;

                        // Generate the receipt for guide.
                        const guideReceipt = await GuideReceipt.create({
                          guide: callRequest.guide.id,
                          amount: calculateFare(duration_out, callRequest.guideRate),
                          status: GuideReceiptStatus.DUE
                        });

                        // Attach the receipt to this call request.
                        callRequest.guideReceipt = guideReceipt.id;

                        // Save this call quest.
                        await callRequest.save();
                    }
                }

                /* eslint-enable */
                reply({ message: 'OK' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const calculateFare = (durationSecs, rate) => {
    if (durationSecs) {
        let [ratePerUnit, timeUnit] = rate.split('/');
        ratePerUnit -= 0;
        timeUnit -= 0;

        // 17 mins = 6 time units if 1 time unit = 3 mins.
        const durationAsPerTimeUnit = Math.ceil(Math.ceil(durationSecs / 60) / timeUnit);

        // Call fare = duration in time units * (rate per unit)
        return durationAsPerTimeUnit * ratePerUnit;
    }
    return 0;
};

export { hungUpHook }; // eslint-disable-line
