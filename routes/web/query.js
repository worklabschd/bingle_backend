import Joi from 'joi';
import { Query, QueryType } from '../../models';
import { generalResponse, handleError } from '../utils/responses';

const receiveOpenQuery = {
    method: 'POST',
    path: '/web/query',
    config: {
        tags: ['web', 'api'],
        description: 'Anyone can submit a support query here.',
        notes: 'Ideally this should come from the website and does not need to be authenticated.',
        validate: {
            payload: {
                name: Joi.string().required(),
                phone: Joi.string().required(),
                email: Joi.string().email().required(),
                body: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    name, phone, email, body
                } = request.payload;
                await Query.create({
                    type: QueryType.OPEN_QUERY,
                    name,
                    phone,
                    email,
                    body
                });
                reply({ message: 'OK' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

export default receiveOpenQuery;
