/**
 * Created by M on 20/01/18. With ❤
 */

import Joi from 'joi';
import * as _ from 'lodash';
import { User } from '../../models';
import { usersResponse, userResponse, handleError } from '../utils/responses';


const getUsers = {
    method: 'GET',
    path: '/portal/users',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets the users present in the system from here.',
        notes: 'Offset starts from 0 with a default value of 0. Limit is set to a default value of 20.',
        validate: {
            query: {
                name: Joi.string().default('').optional(),
                phone: Joi.string().default('').optional(),
                blocked: Joi.boolean().default().optional(),
                offset: Joi.number().optional().default(0),
                limit: Joi.number().optional().default(20)
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    name, phone, offset, limit, blocked
                } = request.query;
                const queryObject = {};
                queryObject.name = new RegExp(name, 'i');
                queryObject.phone = new RegExp(phone, 'i');
                let users = await User.find(queryObject)
                    .sort('name')
                    .skip(offset * limit)
                    .limit(limit);

                users = _.map(users, user => ({
                    id: user.id,
                    name: user.name,
                    email: user.email,
                    countryCode: user.countryCode,
                    phone: user.phone,
                    blocked: user.blocked,
                    age: user.age,
                    gender: user.gender,
                    avatar: user.avatar,
                    latitude: user.location ? user.location[1] : undefined,
                    longitude: user.location ? user.location[0] : undefined
                }));
                reply(users);
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: usersResponse
    }
};

const getUser = {
    method: 'GET',
    path: '/portal/user/{id}',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets everything about a user from here. Call this endpoint as GET /portal/user/(id of the user).',
        notes: 'To get bookings related to this guide use GET /user/call-history.',
        validate: {
            params: {
                id: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    id
                } = request.params;
                const user = await User.findById(id);

                reply({
                    id: user.id,
                    name: user.name,
                    email: user.email,
                    countryCode: user.countryCode,
                    phone: user.phone,
                    age: user.age,
                    gender: user.gender,
                    avatar: user.avatar,
                    blocked: user.blocked,
                    latitude: user.location ? user.location[1] : undefined,
                    longitude: user.location ? user.location[0] : undefined
                });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: userResponse
    }
};

export { getUsers, getUser };
