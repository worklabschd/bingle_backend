/**
 * Created by M on 01/03/18. With ❤
 */

import Joi from 'joi';
import { Guide, CallRequest, RequestStatus, UserReceipt } from '../../models';
import { statsResponse, handleError } from '../utils/responses';
import localMoment from '../utils/time';
import { getAppOpens } from '../../store';

const getStats = {
    method: 'GET',
    path: '/portal/stats',
    config: {
        tags: ['portal', 'api'],
        description: 'The admin can get vital statistics of Meego from here.',
        notes: `Statistics include total number of guides,
        guides on duty, total number of call orders,
        total numer of call orders successfully completed, total money received, app opens in the last 24 hours, etc.
        To and from are optional query params that apply to call orders, call orders received and money received and must be in the format MM-DD-YYYY.
        If they are not given, start and end of the current day is chosen in IST.`,
        validate: {
            query: {
                to: Joi.date().max('now').optional(),
                from: Joi.date().max(Joi.ref('to')).optional()
            }
        },
        handler: async (request, reply) => {
            const {
                to,
                from
            } = request.query;
            const fromDate = localMoment(from).startOf('day').toDate();
            const toDate = localMoment(to).endOf('day').toDate();
            const dateFilter = { $gte: fromDate, $lte: toDate };

            try {
                const numberOfGuides = await Guide.find({}).count();
                const guidesOnDuty = await Guide.find({ onDuty: true }).count();

                const callOrders = await CallRequest.find({
                    status: {
                        $ne: RequestStatus.CREATED
                    },
                    createdAt: dateFilter
                }).count();
                const callOrdersDelivered = await CallRequest.find({
                    $or: [
                        { status: RequestStatus.UNBILLED },
                        { status: RequestStatus.BILLED },
                    ],
                    createdAt: dateFilter
                }).count();

                const userReceipts = await UserReceipt.find({
                    createdAt: dateFilter
                }).select('amount');

                const moneyReceived = userReceipts.reduce(
                    (sum, { amount }) => sum + (amount - 0),
                    0
                );
                const appOpens = await getAppOpens();
                reply({
                    numberOfGuides,
                    guidesOnDuty,
                    callOrders,
                    callOrdersDelivered,
                    moneyReceived,
                    appOpens
                });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: statsResponse
    }
};

export default getStats;
