/**
 * Created by M on 20/01/18. With ❤
 */


import Joi from 'joi';
import * as _ from 'lodash';
import moment from 'moment';
import XLSX from 'xlsx';
import { Guide, NewGuide, Language, GuideReceipt, GuideReceiptStatus } from '../../models';
import {
    guidesResponse,
    guideResponse,
    newGuidesResponse,
    generalResponse,
    handleError
} from '../utils/responses';
import localMoment from '../utils/time';
import geolib from 'geolib';


const getGuides = {
    method: 'GET',
    path: '/portal/guides',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets the guides present in the system from here.',
        notes: 'Offset starts from 0 with a default value of 0. Limit is set to a default value of 20.',
        validate: {
            query: {
                name: Joi.string().default('').optional(),
                phone: Joi.string().default('').optional(),
                language: Joi.string().valid(Object.values(Language)).optional(),
                offset: Joi.number().optional().default(0),
                limit: Joi.number().optional().default(20),
                latitude: Joi.number().min(-90).max(90).optional(),
                longitude: Joi.number().min(-180).max(180).optional(),
                radius: Joi.number().min(0).optional(),
                status: Joi.boolean().default(true).optional()
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    name, phone, language, offset, limit, latitude, longitude, radius, status
                } = request.query;
                const queryObject = {};
                queryObject.name = new RegExp(name, 'i');
                queryObject.phone = new RegExp(phone, 'i');
                if (language) {
                    queryObject.language = language;
                }
		queryObject.status = {$ne:false}
                let guides = await Guide.find(queryObject).sort('name')

                var newGuides = []
                if (latitude && longitude && radius > 0) {
                    newGuides = guides.filter((obj) => {
                        var objRad = geolib.getDistance(
                            { latitude: parseFloat(latitude), longitude: parseFloat(longitude) },
                            { latitude: parseFloat(obj.latitude), longitude: parseFloat(obj.longitude) }
                        );
                        console.log(objRad)
                        return radius > (objRad / 1000)
                    })
                }
                else
                {
                    newGuides = guides
                }
                if(newGuides.length>0)
                guides = newGuides.splice(offset * limit, ((offset * limit) + limit));
                guides = _.sortBy(guides, 'name');
                guides = _.map(guides, guide => ({
                    id: ""+guide._id,
                    name: guide.name,
                    image: guide.image,
                    logo: guide.logo,
                    address_1: guide.address_1,
                    address_2: guide.address_2,
                    city: guide.city,
                    state: guide.state,
                    pinCode: guide.pinCode,
                    email: guide.email,
                    countryCode: guide.countryCode,
                    phone: guide.phone,
                    description: guide.description,
                    info: guide.info,
                    ccn: guide.ccn,
                    cvv: guide.cvv,
                    wifiname: guide.wifiname,
                    restaurant_type: guide.restaurant_type,
                    status: guide.status,
                    latitude: parseFloat(guide.latitude),
                    longitude: parseFloat(guide.longitude),
 		                gallery: guide.gallery
                }));
                reply(guides);
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: guidesResponse
    }
};

const getGuide = {
    method: 'GET',
    path: '/portal/guide/{id}',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets everything about a guide from here. Call this endpoint as GET /portal/guide/(id of the guide).',
        notes: 'To get bookings related to this guide use GET /guide/call-history. To update this guide use POST /guide/edit-profile.',
        validate: {
            params: {
                id: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    id
                } = request.params;
                const guide = await Guide.findOne({'_id':id, status: { $ne : false } });
                reply({ message: `Guide Data`, data:guide });

            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const getNewGuides = {
    method: 'GET',
    path: '/portal/new-guides',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets the new guide registrations that were done from the app.',
        notes: 'Offset starts from 0 with a default value of 0. Limit is set to a default value of 20.',
        validate: {
            query: {
                status: Joi.boolean().optional().default(true),
                offset: Joi.number().optional().default(0),
                limit: Joi.number().optional().default(20)
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    offset, limit, status
                } = request.query;
                const queryObject = {};
                if (status) {
                    // New guide registrations.
                    queryObject.status = false;
                } else {
                    // Rejected guide registrations.
                    queryObject.rejected = true;
                }
                let newGuides = await NewGuide.find(queryObject)
                    .sort('name')
                    .skip(offset * limit)
                    .limit(limit);

                newGuides = _.map(newGuides, newGuide => ({
                    id: newGuide.id,
                    name: newGuide.name,
                    countryCode: newGuide.countryCode,
                    phone: newGuide.phone,
                    email: newGuide.email,
                    status: newGuide.status,
                    rejected: newGuide.rejected,
                    registredWhen: localMoment(newGuide.createdAt).format('DD-MM-YYYY HH:mm'),
                    activatedWhen: newGuide.activatedWhen ? localMoment(newGuide.activatedWhen).format('DD-MM-YYYY HH:mm') : undefined,
                    rejectedWhen: newGuide.rejectedWhen ? localMoment(newGuide.rejectedWhen).format('DD-MM-YYYY HH:mm') : undefined
                }));

                reply(newGuides);
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: newGuidesResponse
    }
};

const changeNewGuideStatus = {
    method: 'PUT',
    path: '/portal/change-new-guide-status/{id}',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin approves or rejects a new guide registration. Call this endpoint as GET /portal/change-new-guide-status/(id of the new guide). ',
        notes: 'This endpoint also takes a status query param. Send true to approve and false to reject. If approved, the newly generated ID of the guide is returned as message.',
        validate: {
            params: {
                id: Joi.string().required()
            },
            query: {
                status: Joi.boolean().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    params: {
                        id
                    },
                    query: {
                        status
                    }
                } = request;
                const newGuide = await NewGuide.findById(id);
                if (status) {
                    reply({ message: await newGuide.activate() });
                    return;
                }
                await newGuide.reject();
                reply({ message: 'REJECTED' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const getGuideDues = {
    method: 'GET',
    path: '/portal/guide-dues',
    config: {
        tags: ['portal', 'api'],
        description: 'The admin can get guide dues from here.',
        notes: `An admin can get how much the guides have earned during a particular period.
        To and from are optional query params must be in the format MM-DD-YYYY.
        If they are not given, start and end of the current day is chosen in IST.
        An appropriate file will be downloaded given by the format query param. Default is .xlsx.`,
        validate: {
            query: {
                to: Joi.date().max('now').optional(),
                from: Joi.date().max(Joi.ref('to')).optional(),
                format: Joi.string().valid(['excel', 'csv']).default('excel')
            }
        },
        handler: async (request, reply) => {
            const {
                to,
                from,
                format
            } = request.query;
            const fromDate = localMoment(from).startOf('day').toDate();
            const toDate = localMoment(to).endOf('day').toDate();
            const dateFilter = { $gte: fromDate, $lte: toDate };

            try {
                const guideReceipts = await GuideReceipt.find({
                    status: GuideReceiptStatus.DUE,
                    createdAt: dateFilter
                })
                    .populate('guide');

                const result = {};
                guideReceipts.forEach(receipt => {
                    const {
                        guide: {
                            id,
                            name,
                            phone,
                            address_1,
                            address_2,
                            state,
                            pinCode,
                            email,
                            accountNumber,
                            ifsc,
                            licenseExpiry,
                            dob,
                            createdAt
                        }
                    } = receipt;
                    let { amount } = receipt;
                    amount -= 0;
                    result[id] = result[id] || [
                        name,
                        amount,
                        phone,
                        address_1,
                        address_2,
                        state,
                        pinCode,
                        email,
                        accountNumber,
                        ifsc,
                        licenseExpiry,
                        dob,
                        createdAt
                    ];
                    result[id][1] += (amount - 0);
                });
                const headers = [
                    'Name',
                    'Amount',
                    'Phone',
                    'Address',
                    'State',
                    'Pin Code',
                    'Email',
                    'Account Number',
                    'IFSC',
                    'License Expiry',
                    'DOB',
                    'Activated On'
                ];
                const wb = XLSX.utils.book_new();
                const ws = XLSX.utils.aoa_to_sheet([
                    headers,
                    ...Object.values(result)
                ]);
                if (format === 'csv') {
                    reply(XLSX.utils.sheet_to_csv(ws))
                        .header('Content-Type', 'application/octet-stream')
                        .header('Content-Disposition', 'attachment; filename=Guides.csv');
                    return;
                }
                wb.SheetNames.push('Guides');
                wb.Sheets.Guides = ws;
                const wbbuf = XLSX.write(wb, {
                    type: 'buffer'
                });
                reply(wbbuf)
                    .header('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                    .header('Content-Disposition', 'attachment; filename=Guides.xlsx');
            } catch (err) {
                handleError(request, reply, err);
            }
        }
    }
};

const clearGuideDues = {
    method: 'PUT',
    path: '/portal/guide-clear-dues',
    config: {
        tags: ['portal', 'api'],
        description: 'The admin can clear guide dues from here.',
        notes: `To and from are optional query params must be in the format MM-DD-YYYY.
        If they are not given, start and end of the current day is chosen in IST.`,
        validate: {
            query: {
                to: Joi.date().max('now').optional(),
                from: Joi.date().max(Joi.ref('to')).optional()
            }
        },
        handler: async (request, reply) => {
            const {
                to,
                from
            } = request.query;
            const fromDate = localMoment(from).startOf('day').toDate();
            const toDate = localMoment(to).endOf('day').toDate();
            const dateFilter = { $gte: fromDate, $lte: toDate };

            try {
                await GuideReceipt.update({
                    status: GuideReceiptStatus.DUE,
                    createdAt: dateFilter
                }, { status: GuideReceiptStatus.PAID });

                reply({ message: 'OK' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

export {
    getGuides,
    getGuide,
    getNewGuides,
    changeNewGuideStatus,
    getGuideDues,
    clearGuideDues
};
