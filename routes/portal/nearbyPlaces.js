/**
 * Created by M on 06/01/18. With ❤
 */

import Joi from 'joi';
import path from 'path';
import ba64 from 'ba64';
import uuid from 'uuid/v1';
import { NearbyPlace, NearbyPlaceType } from '../../models';
import { generalResponse, nearbyResponse, nearbyAddResponse, handleError } from '../utils/responses';

const getNearby = {
    method: 'GET',
    path: '/portal/get-nearby',
    config: {
        tags: ['portal', 'api'],
        description: 'The admin can get list of all the nearby places in the system.',
        notes: 'All the query params are optional. Offset will default to 0 and limit to 20.',
        validate: {
            query: {
                name: Joi.string().default('').optional(),
                type: Joi.string().valid(Object.values(NearbyPlaceType)).optional(),
                city: Joi.string().optional(),
                offset: Joi.number().optional().default(0),
                limit: Joi.number().optional().default(20)
            }
        },
        handler: async (request, reply) => {
            const {
                name, type, city, offset, limit
            } = request.query;
            try {
                const queryObject = {};
                queryObject.name = new RegExp(name, 'i');
                if (type) {
                    queryObject.type = type;
                }
                if (city) {
                    queryObject.city = new RegExp(city, 'i');
                }
                let places = await NearbyPlace.find(queryObject)
                    .sort('name')
                    .skip(offset * limit)
                    .limit(limit);

                places = places.map(place => ({
                    id: place.id,
                    name: place.name,
                    description: place.description,
                    image: place.image,
                    type: place.type,
                    latitude: place.location[1],
                    longitude: place.location[0]
                }));

                reply(places);
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: nearbyResponse
    }
};

const addNearby = {
    method: 'POST',
    path: '/portal/add-nearby',
    config: {
        tags: ['portal', 'api'],
        description: 'Used for adding nearby places from the admin portal.',
        notes: 'All the params are required. Image stands for base64 encoded image.',
        validate: {
            payload: {
                name: Joi.string().required(),
                description: Joi.string().required(),
                type: Joi.string().valid(Object.values(NearbyPlaceType)).required(),
                image: Joi.string().required(),
                latitude: Joi.number().min(-180).max(180).required(),
                longitude: Joi.number().min(0).max(90).required()
            }
        },
        handler: async (request, reply) => {
            const {
                name, description, type, latitude, longitude, image
            } = request.payload;
            try {
                const fileName = uuid();
                const filePath = path.join('assets', 'images', 'nearby', fileName);
                ba64.writeImageSync(filePath, image);
                const newPlace = await NearbyPlace.create({
                    name,
                    description,
                    image: `/images/nearby/${fileName}.${ba64.getExt(image)}`,
                    type,
                    location: [longitude, latitude]
                });
                reply({
                    id: newPlace.id,
                    name,
                    description,
                    image: newPlace.image,
                    type,
                    latitude,
                    longitude
                });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: nearbyAddResponse
    }
};

const deleteNearby = {
    method: 'DELETE',
    path: '/portal/delete-nearby',
    config: {
        tags: ['portal', 'api'],
        description: 'Used for deleting a nearby place from the admin portal.',
        notes: 'Only needs a place ID',
        validate: {
            payload: {
                id: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            const {
                id
            } = request.payload;
            try {
                await NearbyPlace.remove({
                    _id: id
                });
                reply({ message: 'OK' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

export { getNearby, addNearby, deleteNearby };
