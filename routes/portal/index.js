import * as nearbyRoutes from './nearbyPlaces';
import * as queryRoutes from './query';
import * as guideRoutes from './guide';
import * as userRoutes from './user';
import statsRoute from './stats';

export default Object.values(nearbyRoutes)
    .concat(Object.values(queryRoutes))
    .concat(Object.values(guideRoutes))
    .concat(Object.values(userRoutes))
    .concat([statsRoute]);
