/**
 * Created by M on 20/01/18. With ❤
 */

import Joi from 'joi';
import * as _ from 'lodash';
import moment from 'moment';
import { Query, QueryType, QueryStatus } from '../../models';
import { generalResponse, queriesResponse, handleError } from '../utils/responses';
import { sendQueryReply } from '../utils/email';


const getQueries = {
    method: 'GET',
    path: '/portal/get-queries',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets support queries submitted inside both the apps from here.',
        notes: 'Offset starts from 0 with a default value of 0. Limit is set to a default value of 20. Type pertains to "user" or "guide". Status can be one of "created", "not_resolved" and "resolved". Type and status are both optional.',
        validate: {
            query: {
                offset: Joi.number().optional().default(0),
                limit: Joi.number().optional().default(20),
                type: Joi.string().valid(Object.values(QueryType)).optional(),
                status: Joi.string().valid(Object.values(QueryStatus)).optional()
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    offset, limit, type, status
                } = request.query;
                const queryObject = {};
                if (type) {
                    queryObject.type = type;
                }
                if (status) {
                    queryObject.status = status;
                }
                let queries = await Query.find(queryObject)
                    .sort('-createdAt')
                    .skip(offset * limit)
                    .limit(limit)
                    .populate('user')
                    .populate('guide');
                queries = _.map(queries, query => ({
                    ..._.omit(query.toJSON(), ['_id', '__v']),
                    id: query.id,
                    user: query.user ? _.pick(query.user.toJSON(), ['name', 'email', 'phone']) : null,
                    guide: query.guide ? _.pick(query.guide.toJSON(), ['name', 'email', 'phone']) : null,
                    createdAt: moment(query.createdAt).format('HH:mm DD:MM:YY')
                }));
                reply(queries);
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: queriesResponse
    }
};

const changeQueryStatus = {
    method: 'PUT',
    path: '/portal/change-query-status',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin gets to change a user/guide submitted query\'s state.',
        notes: 'Status can be one of "created", "not_resolved" and "resolved". id and status are both required.',
        validate: {
            payload: {
                id: Joi.string().required(),
                status: Joi.string().valid(Object.values(QueryStatus)).required()
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    id, status
                } = request.payload;
                await Query.findByIdAndUpdate(id, { status });
                reply({ message: 'OK' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

const replyToQuery = {
    method: 'POST',
    path: '/portal/reply-query',
    config: {
        tags: ['portal', 'api'],
        description: 'An admin can reply to a query raised by either the customer or the guide from here.',
        notes: 'The reply is sent on the email provided inside the query.',
        validate: {
            payload: {
                id: Joi.string().required(),
                response: Joi.string().required()
            }
        },
        handler: async (request, reply) => {
            try {
                const {
                    id, response
                } = request.payload;
                const { email, body } = await Query.findById(id);
                await sendQueryReply(email, response, body);
                reply({ message: 'OK' });
            } catch (err) {
                handleError(request, reply, err);
            }
        },
        response: generalResponse
    }
};

export { getQueries, changeQueryStatus, replyToQuery };
