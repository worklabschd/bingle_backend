import unirest from 'unirest';

const MAPS_API_KEY = 'AIzaSyBh_VKN456wD_wnB9bewzxdfmJvCv6opq0';

const calculateDistanceToPlaces = async (latitude, longitude, places) =>
    new Promise((resolve, reject) => {
        const destinations = places.reduce((acc, place) => `${acc}${place.location[1]},${place.location[0]}|`, '');
        const responses = [...places];
        unirest
            .get('https://maps.googleapis.com/maps/api/distancematrix/json')
            .query({
                key: MAPS_API_KEY,
                origins: `${latitude},${longitude}`,
                destinations: `${destinations}`
            })
            .end(({ ok, body }) => {
                if (!ok) {
                    reject(body);
                    return;
                }
                if (body.status === 'OK') {
                    const { elements } = body.rows[0];
                    elements.forEach((element, index) => {
                        const {
                            status,
                            distance,
                            duration
                        } = element;
                        if (status === 'OK') {
                            responses[index].distance = distance.text;
                            responses[index].duration = duration.text;
                            return;
                        }
                        responses[index].distance = 'NA';
                    });
                } else {
                    reject(new Error(`DIRECTION MATRIX ERROR ${body.status}.`));
                }
                resolve(responses);
            });
    });


export default calculateDistanceToPlaces;
