import unirest from 'unirest';
import { callConfig } from '../config.json';

const basePath = 'http://www.prpmobility.in/Api/ClickCallReq.aspx';

const requestCall = (userNumber, guideNumber, callCentreToken) => new Promise((resolve, reject) => {
    unirest.get(basePath)
        .query({
            uname: callConfig.CALL_CENTRE_USERNAME,
            pass: callConfig.CALL_CENTRE_PASS,
            aparty: userNumber,
            bparty: guideNumber,
            reqid: `${callCentreToken}`
        })
        .end(({ ok, body }) => {
            if (!ok) {
                reject(body);
                return;
            }
            if (!body.includes('SUCCESS')) {
                reject(body);
                return;
            }
            resolve();
        });
});

export default requestCall;
