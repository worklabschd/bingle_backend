import unirest from 'unirest';

const resolveRequest = (resolve, response) => {
    resolve({
        name: response.name,
        email: response.email,
        gender: response.gender || 'na',
        birthday: response.birthday || 0
    });
};

const facebookLogin = async token => new Promise((resolve, reject) => {
    unirest.get('https://graph.facebook.com/me?fields=name,email,birthday,gender')
        .query({
            access_token: token
        })
        .end(response => {
            if (!response.ok) {
                reject(response.body);
                return;
            }
            const body = JSON.parse(response.body);
            resolveRequest(resolve, body);
        });
});

const googleLogin = async token => new Promise((resolve, reject) => {
    unirest.get('https://www.googleapis.com/userinfo/v2/me')
        .headers({ Authorization: `Bearer ${token}` })
        .end(response => {
            if (!response.ok) {
                reject(response.body);
                return;
            }
            resolveRequest(resolve, response.body);
        });
});


export { facebookLogin, googleLogin };
