import Razorpay from 'razorpay';
import { razorpayConfig } from '../config.json';

const { KEY_ID, KEY_SECRET } = razorpayConfig;
const capturePayment = async (paymentID, amount) => {
    const razorpay = new Razorpay({
        key_id: KEY_ID,
        key_secret: KEY_SECRET
    });
    const {
        method,
        card_id, // eslint-disable-line
        bank,
        wallet,
        email,
        contact,
        fee,
        tax
    } = await razorpay.payments.capture(paymentID, amount * 100);
    return {
        method,
        cardID: card_id,
        bank,
        wallet,
        email,
        contact,
        fee: fee / 100,
        tax: tax / 100
    };
};

export { capturePayment }; // eslint-disable-line
