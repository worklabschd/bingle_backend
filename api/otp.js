import SendOtp from 'sendotp';
import { msg91 } from '../config.json';

const SMS = 'Hi! One time password for your Bingle registration is {{otp}}.';
const otp = new SendOtp(msg91.AUTH_KEY, SMS);

// Set 15 mins for OTP expiry.
otp.setOtpExpiry('15');

const callbackHOF = (cb, verify) => (error, data) => {
    if (error) {
        // If any errors.
        cb(error);
    } else if (data.type === 'error' && !verify) {
        // if data.type is not success somehow and we're not veryfing OTP either.
        cb(new Error(data.message));
    } else {
        cb(null, data.message);
    }
};

const sendOTP = (countryCode, phone, callback) => {
    otp.send(countryCode.replace('+', '') + phone, msg91.SENDER_ID, callbackHOF(callback));
};

const retryOTP = (countryCode, phone, callback) => {
    otp.retry(countryCode.replace('+', '') + phone, true, callbackHOF(callback));
};

const verifyOTP = (countryCode, phone, otpToVerify, callback) => {
    otp.verify(`${countryCode}${phone}`, otpToVerify, callbackHOF(callback, true));
};

export { sendOTP, retryOTP, verifyOTP };
