/**
 * Created by M on 11/10/17. With ❤
 */

import Hapi from 'hapi';
import mongoose from 'mongoose';
import { serverConfig, database } from './config.json';
import { userRoutes, guideRoutes, portalRoutes, webRoutes,  partnerRoutes, hooks, chatClientRoutes} from './routes';
import plugins from './utils/plugins';
import templating from './utils/templating';
import { setupDSListeners } from './events/deepstream';
import eventNames from './events/constants';

const isProd = process.env.NODE_ENV === 'production';

const connectToMongo = async () => {
    const uri = `mongodb://${isProd ? 'mongo' : 'localhost'}:27017/${database.DB_NAME}`;
    mongoose.Promise = require('bluebird');
    await mongoose.connect(uri);
};

const configureHapi = () => {
    // Intialize the Hapi server instance.
    const server = new Hapi.Server();
    server.connection({
        port: serverConfig.PORT,
        host: serverConfig.HOST,
        routes: { cors: true },
        labels: ['api', 'user', 'guide', 'portal', 'web', 'hooks', 'partner', 'chat']
    });

    // Put in place user routes.
    server.select('api').select('user').route(userRoutes);

    // Put in place guide routes.
    server.select('api').select('guide').route(guideRoutes);

    // Put in place portal routes.
    server.select('api').select('portal').route(portalRoutes);

    // Put in open routes for website.
    server.select('api').select('web').route(webRoutes);

    // Put in routes for partner dashboard.
    server.select('api').select('partner').route(partnerRoutes);

    // Put in routes for web chat Client
    server.select('api').select('chat').route(chatClientRoutes);

    // Put in place all the web hooks.
    server.select('hooks').route(hooks);

    // Put in place swagger and other plugins like logging.
    server.register(plugins, (err) => {
        if (err) {
            throw err;
        }

        // Put in place the templating engine.
        server.views(templating);

        // Start the server.
        server.start(e => {
            if (e) {
                throw e;
            }
            console.log(`Bingle Server running at port: ${server.info.port}`);
        });
    });
};

// Fire up everything.
(async () => {
    try {
        // Try to connect to Mongo.
        await connectToMongo();
        // Try to connect and try to set up deepstream listeners.
        await setupDSListeners(Object.values(eventNames));
        // Connection to Mongo and Deepstream successful.

        // Start configuring Hapi.
        configureHapi();
    } catch (err) {
        // Connections were unsuccessful.
        console.warn(`Couldn't start server - ${err}`);
        process.exit(1);
    }
})();
