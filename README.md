## Meego Backend

Server for Meego customer and Meego Tour Guide App. It is built using
#### • Node.js and
#### • Hapi.js
#### • MongoDB is used for persistence.

## API Docs - Swagger.
Available at `/swag`

## Instructions.
• `npm i`
• `npm run build` -> `npm start`
or
• `npm run dev`
