/**
 * Created by M on 06/05/17. With ❤
 */

import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import Guide from './guide';

mongoose.Promise = require('bluebird');

const { Schema } = mongoose;

const newGuideSchema = new Schema({
    name: { type: String, required: true },
    phone: { type: String, required: true },
    countryCode: { type: String, required: true },
    address: { type: String },
    state: { type: String },
    pinCode: { type: String },
    email: {
        type: String, required: true, unique: true, lowercase: true
    },
    status: { type: Boolean, default: false },
    rejected: { type: Boolean, default: false },
    password: { type: String, required: true },
    activatedWhen: Date,
    rejectedWhen: Date,
    createdAt: Date
}, { runSettersOnQuery: true });

newGuideSchema.pre('save', function (next) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

// This method activates the new registration and creates a tour guide account.
newGuideSchema.methods.activate = async function () {
    const hashed = bcrypt.hashSync(this.password, 10);
    const guide = await Guide.create({
        name: this.name,
        phone: this.phone,
        countryCode: this.countryCode,
        address: this.address,
        state: this.state,
        pinCode: this.pinCode,
        email: this.email,
        password: hashed,
    });
    // Plain text password is reset.
    this.password = 'stale';
    this.status = true;
    this.activatedWhen = new Date();
    await this.save();
    return guide.id;
};

// This method rejects the new registration.
newGuideSchema.methods.reject = async function () {
    // Plain text password is reset.
    this.password = 'stale';
    this.rejected = true;
    this.rejectedWhen = new Date();
    await this.save();
};

const NewGuide = mongoose.model('NewGuide', newGuideSchema);

export default NewGuide;
