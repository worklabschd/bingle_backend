/**
 * Created by M on 06/05/17. With ❤
 */

import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

mongoose.Promise = require('bluebird');

const { Schema } = mongoose;

const partnerSchema = new Schema({
    name: { type: String, required: true },
    phone: { type: String, required: true },
    countryCode: { type: String, required: true },
    address: { type: String },
    state: { type: String },
    pinCode: { type: String },
    email: {
        type: String, required: true, unique: true, lowercase: true
    },
    password: { type: String, required: true },
    license: { type: String },
    createdAt: { type: Date, default: new Date() }
    // so a Mustansir29@hotmail.com would be eq to mustansir29@hotmail.com
}, { runSettersOnQuery: true });

partnerSchema.methods.comparePassword = function (plainTextPassword) {
    return bcrypt.compareSync(plainTextPassword, this.password);
};

const Partner = mongoose.model('Partner', partnerSchema);

export default Partner;
