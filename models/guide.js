/**
 * Created by M on 06/05/17. With ❤
 */

import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

mongoose.Promise = require('bluebird');

const Language = {
    ENGLISH: 'english',
    HINDI: 'hindi',
    FRENCH: 'french',
    includes(language) {
        return Object.values(this).includes(language);
    }
};

const { Schema } = mongoose;

const guideSchema = new Schema({
    name: { type: String, required: true },
    image: { type: String, required: false },
    logo: { type: String, required: false },
    gallery: { type: Array, required: false },
    phone: { type: String, required: true },
    country: { type: String, required: true },
    address_1: { type: String },
    address_2: { type: String },
    city: { type: String },
    state: { type: String },
    pinCode: { type: String },
    countryCode: { type: String, required: true },
    email: {
        type: String, required: true, unique: true, lowercase: true
    },
    password: { type: String, required: true },
    languages: { type: [String], default: [Language.ENGLISH, Language.HINDI] },
    description: { type: String, required: true },
    monday: { type: String, required: false },
    tuesday: { type: String, required: false },
    wednesday: { type: String, required: false },
    thursday: { type: String, required: false },
    friday: { type: String, required: false },
    saturday: { type: String, required: false },
    sunday: { type: String, required: false },
    info: { type: String, required: true },
    website: { type: String, required: false },
    ccn: { type: String, required: true },
    expirydate: { type: String, required: false },
    cvv: { type: String, required: true },
    wifiname: { type: String, required: true },
    wifisecret: { type: String, required: false },
    restaurant_type: { type: String, required: true },
    latitude: { type: String, required: false },
    longitude: { type: String, required: true },
    status: { type: Boolean, required: true, default:true },
    createdAt: { type: Date }

    // so a Mustansir29@hotmail.com would be eq to mustansir29@hotmail.com
}, { runSettersOnQuery: true });

guideSchema.pre('save', function (next) {
    // If this is a new guide.
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

guideSchema.methods.comparePassword = function (plainTextPassword) {
    return bcrypt.compareSync(plainTextPassword, this.password);
};

// Queue a guide so he doesn't get a booking.
guideSchema.methods.queueGuide = async function () {
    this.onQueue = true;
    await this.save();
};

// Un Queue a guide so he may get a booking now.
guideSchema.methods.unQueueGuide = async function () {
    this.onQueue = false;
    await this.save();
};

const Guide = mongoose.model('Guide', guideSchema);

export { Language };

export default Guide;
