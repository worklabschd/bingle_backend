/**
 * Created by Jaspreet. With ❤
 */

import mongoose from 'mongoose';

mongoose.Promise = require('bluebird');

const { Schema } = mongoose;

/** Chat Schema - start*/
const chatSchema = new Schema({
    users: { type: [String], required: true },
    initiateFrom: { type: String, required: true },
    initiateTo: { type: String, required: true },
    accepted: { type: Boolean, required: true, default: false },
    updatedAt: { type: Date, default: Date.now }
}, { runSettersOnQuery: true });


chatSchema.methods.requestAccepted = async function () {
    this.accepted = true;
    await this.save();
};

chatSchema.methods.isAccepted = function () {
    return this.accepted;
};

const Chat = mongoose.model('Chat', chatSchema);

/** Chat Schema - start*/


/** Messages Schema - start */

const chatMessageSchema = new Schema({
    chat:  { type: Schema.Types.ObjectId, ref: 'Chat', required: true },
    from: { type: String, required: true },
    to: { type: String, required: true },
    message: { type: String, required: true },
    createdAt: { type: Date, default: Date.now }
}, { runSettersOnQuery: true });

const ChatMessage = mongoose.model('ChatMessage', chatMessageSchema);

/** Messages Schema - end */


/** WifiRoom Schema - start */

const wifiRoomSchema = new Schema({
    room: { type: String, required: true },
    user: { type: String, required: true },
    status: { type: String, required: true },
    atTime: { type: Date, default: Date.now }
}, { runSettersOnQuery: true });

const WifiRoom = mongoose.model('WifiRoom', wifiRoomSchema);

/** WifiRoom Schema - end */


export { ChatMessage, WifiRoom };

export default Chat;
