/**
 * Created by M on 03/03/18. With ❤
 */

import mongoose from 'mongoose';

mongoose.Promise = require('bluebird');

const { Schema } = mongoose;

const GuideReceiptStatus = {
    DUE: 'DUE',
    PAID: 'PAID'
};

const guideReceiptSchema = new Schema({
    guide: { type: Schema.Types.ObjectId, ref: 'Guide', required: true },
    amount: { type: String, required: true },
    status: { type: String, required: true },
    createdAt: { type: Date, default: new Date() }
});

const GuideReceipt = mongoose.model('GuideReceipt', guideReceiptSchema);


export { GuideReceiptStatus };
export default GuideReceipt;
