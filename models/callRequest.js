/**
 * Created by M on 06/05/17. With ❤
 */

import mongoose from 'mongoose';

mongoose.Promise = require('bluebird');

const RequestStatus = {
    CREATED: 'created',
    ACCEPTED: 'accepted',
    UNBILLED: 'unbilled',
    BILLED: 'billed'
};

const { Schema } = mongoose;

const callRequestSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    associatedGuides: [{ type: Schema.Types.ObjectId, ref: 'Guide' }],
    guide: { type: Schema.Types.ObjectId, ref: 'Guide' },
    guideReceipt: { type: Schema.Types.ObjectId, ref: 'GuideReceipt' },
    callCentreToken: { type: String },
    duration: { type: Number },
    fare: { type: String },
    rate: { type: String },
    guideRate: { type: String },
    location: { type: [Number], index: '2dsphere' },
    phone: { type: String, required: true },
    // type RequestStatus.
    language: { type: String, required: true },
    status: { type: String, required: true },
    createdAt: Date
});

callRequestSchema.pre('save', function (next) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

// Called when a call request is accepted by a guide.
callRequestSchema.methods.acceptRequest = async function (guideID) {
    this.guide = guideID;
    this.status = RequestStatus.ACCEPTED;
    await this.save();
};

const CallRequest = mongoose.model('CallRequest', callRequestSchema);

export { RequestStatus };
export default CallRequest;
