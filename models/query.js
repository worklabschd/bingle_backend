/**
 * Created by M on 06/05/17. With ❤
 */

import mongoose from 'mongoose';

mongoose.Promise = require('bluebird');

const QueryType = {
    USER_QUERY: 'user',
    GUIDE_QUERY: 'guide',
    OPEN_QUERY: 'open'
};

const QueryStatus = {
    CREATED: 'created',
    NOT_RESOLVED: 'not_resolved',
    RESOLVED: 'resolved'
};

const { Schema } = mongoose;

const querySchema = new Schema({
    guide: { type: Schema.Types.ObjectId, ref: 'Guide' },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    name: { type: String, required: true },
    type: { type: String, required: true },
    phone: { type: String, required: true },
    email: { type: String, required: true },
    body: { type: String },
    status: { type: String, default: QueryStatus.CREATED },
    createdAt: Date
}, { runSettersOnQuery: true });

querySchema.pre('save', function (next) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

const Query = mongoose.model('Query', querySchema);

export { QueryType, QueryStatus };
export default Query;
