/**
 * Created by M on 06/05/17. With ❤
 */

import mongoose from 'mongoose';

mongoose.Promise = require('bluebird');

const NearbyPlaceType = {
    POPULAR: 'popular',
    MONUMENT: 'monument',
    FOOD: 'food',
    ATM: 'atm'
};

const { Schema } = mongoose;

const nearbyPlaceSchema = new Schema({
    type: { type: String, required: true },
    name: { type: String, required: true, index: 'text' },
    description: { type: String, required: true },
    image: { type: String, required: true },
    location: { type: [Number], index: '2dsphere' },
    city: { type: String },
    createdAt: Date
});

nearbyPlaceSchema.pre('save', function (next) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

const NearbyPlace = mongoose.model('NearbyPlace', nearbyPlaceSchema);

export { NearbyPlaceType };
export default NearbyPlace;
