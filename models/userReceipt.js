/**
 * Created by M on 04/02/18. With ❤
 */

import mongoose from 'mongoose';

mongoose.Promise = require('bluebird');

const { Schema } = mongoose;

const userReceiptSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    paymentGateway: { type: String, required: true },
    amount: { type: String, required: true },
    paymentID: { type: String, required: true },
    international: { type: Boolean, default: false },
    method: { type: String, required: true },
    cardID: { type: String },
    bank: { type: String },
    wallet: { type: String },
    email: { type: String },
    contact: { type: String },
    fee: { type: String },
    tax: { type: String },
    createdAt: Date
});

userReceiptSchema.pre('save', function (next) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

const UserReceipt = mongoose.model('UserReceipt', userReceiptSchema);

export default UserReceipt;
