/**
 * Created by M on 06/05/17. With ❤
 */

import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

mongoose.Promise = require('bluebird');

const { Schema } = mongoose;

const userSchema = new Schema({
    name: { type: String, required: true },
    phone: { type: String, required: true },
    countryCode: { type: String, required: true },
    age: { type: Number },
    gender: { type: String },
    email: {
        type: String, required: true, unique: true, lowercase: true
    },
    password: { type: String, required: true },
    avatar: { type: String },
    location: { type: [Number], index: '2dsphere' },
    blocked: { type: Boolean, default:false },
    createdAt: Date
    // so a Mustansir29@hotmail.com would be eq to mustansir29@hotmail.com
}, { runSettersOnQuery: true });

// Yeah, these pre hooks are useless but keeping them anyway, cause chow!
userSchema.pre('save', function (next) {
    // If this is a new user.
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

userSchema.methods.comparePassword = function (plainTextPassword) {
    return bcrypt.compareSync(plainTextPassword, this.password);
};

const User = mongoose.model('User', userSchema);

export default User;
