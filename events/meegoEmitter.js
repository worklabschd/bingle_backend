import EventEmitter from 'events';
import { emitToDS } from './deepstream';

class MeegoEmitter extends EventEmitter {
    emitToGuidesAndRegisterOnce(eventName, eventID, guideIDs, data, timeout, callback) {
        const eventNameFull = `${eventName}/${eventID}`;
        const timeoutFunc = setTimeout(() => {
            this.removeAllListeners(eventNameFull);
            callback(null);
        }, timeout);
        this.once(eventNameFull, payload => {
            clearTimeout(timeoutFunc);
            callback(payload);
        });
        guideIDs.forEach(guideID => {
            emitToDS(guideID, { eventName, eventID, data });
        });
    }
}

export default new MeegoEmitter();
