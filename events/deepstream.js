import deepstream from 'deepstream.io-client-js';
import meegoEmitter from './meegoEmitter';
// import { deepstreamConfig } from '../config.json';
import log from '../utils/logging';

const isProd = process.env.NODE_ENV === 'production';
let client = null;
let dsClient = null;

// Variable to keep track of Deepstream client connection status.
let dsConnected = false;

// If connection to deepstream ever breaks down, we'd know about it. Plus Vice Versa!
const stateChangeListener = (state) => {
    switch (state) {
    case deepstream.CONSTANTS.CONNECTION_STATE.ERROR:
        dsConnected = false;
        log(`Deepstream Disconnected : WorkerID ${process.pid}`);
        break;
    case deepstream.CONSTANTS.CONNECTION_STATE.OPEN:
        dsConnected = true;
        break;
    default:
    }
};

// Explicit DS error handling.
const errorHandler = (error, event, topic) => {
    log(`Deepstream Error | msg: ${error} | event: ${event} |  topic: ${topic} | worker: ${process.pid}`);
    // If this the server has started the first time (maybe deepstream is down), exit.
    if (!dsClient) {
        process.exit(1);
    }
};

// Function to connect to Deepstream.
const connectDSClient = () => new Promise((resolve, reject) => {
    // Login as a client.
    client.login({}, (success, data) => {
        if (!success) {
            reject(new Error(`Deepstream authentication error - ${JSON.stringify(data)}`));
            return;
        }
        resolve();
    });
});

const emitToDS = async (eventName, payload) => {
    if (dsConnected) {
        // Emit to Deepstream if Deepstream client is connected.
        dsClient.emit(eventName, payload);
        return;
    }
    // If Deepstream is not connected, try to reconnect.
    await connectDSClient();
    dsClient.emit(eventName, payload);
};

const setupDSListeners = async eventNames => {
    // Connect to deepstream first.
    client = deepstream(`${isProd ? 'deepstream' : 'localhost'}:6020`);
    client.on('error', errorHandler);
    client.on('connectionStateChanged', stateChangeListener);
    await connectDSClient();
    dsClient = client.event;
    eventNames.forEach(eventName => {
        dsClient.subscribe(eventName, payload => {
            meegoEmitter.emit(`${eventName}/${payload.eventID}`, payload);
        });
    });
};


export { emitToDS, setupDSListeners };
