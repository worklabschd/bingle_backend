var socket = io();

var room = '';
var username = '';
var userListInRoom = [];

function updateUserListInRoom(){
    var element = document.getElementById('room');
    element.innerText = JSON.stringify(userListInRoom);
}

socket.on('connect', function(){
    console.log('new socket created');
});

/**
 * Event that we need to listen - start
 */

socket.on('notification:user_online', function(data){
    console.log('new user is online', data.username);
})

socket.on('notification:new_user_in_room', function(data){
    console.log('new user in our room', data.username);
    userListInRoom.push(data.username);
    updateUserListInRoom();
})

socket.on('notifications:list_of_online_users', function(data){
    console.log('users all alive', data);
});

socket.on('notifications:list_of_users_in_room', function(data){
    console.log('users all in room', data);
    userListInRoom = data.userList;
    updateUserListInRoom();
});

socket.on('error:no_username_provided_to_join', function(){
    console.log('error:no_username_provided_to_join');
});

socket.on('error:message_not_sent', function(data){
    console.log('error:message_not_sent', data);
});

socket.on('server:message', function(data){
    console.log('server:message', data);
});

socket.on('notification:user_left_room', function(data){
    console.log('notification:user_left_room', data);
    userListInRoom.splice(userListInRoom.indexOf(data.username), 1); 
    updateUserListInRoom();
});

socket.on('notification:user_offline', function(data){
    console.log('notification:user_offline', data);
});

/**
 * Event that we need to listen - end
 */

/**
 * Event that we need to send to server - start
 * All two events need to be sent by client, 'join' and 'client:message'
 */

function wrappingIt(){
    socket.emit('join', { username: 'username', room: 'default_room'});

    socket.emit('client:message', {from: 'username', to: 'username2', message: 'message here'});
}



// Below code is written for web client UI handling and you dont need to read it

function joinRoom(){
    username = prompt("Username?");
    room = prompt("Room name?");

    if ((username == null || username == "") && (room == null || room == "")) {
        alert('room or username or both are empty');
    } else {
        socket.emit('join', { username: username, room: room});
    }
}

function sendMessage(){
    var to = prompt("toWhome?");
    var message = prompt("message?");

    if ((username == null || username == "") && (to == null || to == "") && (message == null || message == "")) {
        alert('to or username or message is empty');
    } else {
        socket.emit('client:message', {from: username, to: to, message: message});
    }  
}


