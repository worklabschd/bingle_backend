import kue from 'kue';
import log from '../utils/logging';

const isProd = process.env.NODE_ENV === 'production';

const q = kue.createQueue({
    prefix: 'q',
    redis: {
        port: 6379,
        host: isProd ? 'redis' : 'localhost'
    }
});


// Allowing the pending jobs to finish.
const graceful = code => () => {
    q.shutdown(2000, err => {
        if (err) {
            log(`In Kue shutdown: ${err.toString()}`);
        }
        process.exit(code);
    });
};

process.once('SIGINT', graceful(1));

process.once('SIGTERM', graceful(0));

q.on('error', err => {
    log(`In Kue error: ${err.toString()}`);
    if (err.code === 'ECONNREFUSED') {
        process.exit(1);
    }
});


function Queue(name, delay, ttl, cb) {
    q.process(name, async (job, done) => {
        try {
            await cb(job.data);
            done(null);
        } catch (err) {
            log(`Error In Que - ${name}, data - ${job.data}: ${err.toString()}`);
            done(err);
        }
    });

    this.name = name;
    this.delay = delay;
    this.ttl = ttl;
}

Queue.prototype.queue = function (data) {
    q.create(this.name, data)
        .delay(this.delay)
        .ttl(this.ttl)
        .attempts(100)
        .backoff({ delay: this.delay, type: 'fixed' })
        .removeOnComplete(true)
        .save();
};

export default Queue;
