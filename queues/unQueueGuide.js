import Queue from './queue';
import Guide from '../models/guide';
import { callConfig } from '../config.json';

// Un queue a guide so he gets a booking again.
const unQueueGuide = async guideID => {
    console.log('unqueing this guide', guideID);
    await Guide.update({ _id: guideID }, { onQueue: false });
};

// Start a que for Un queueing a guide after 5 mins of having received a call.
export default new Queue('unQueueGuide', callConfig.GUIDE_QUEUE_PERIOD * 1000, 180000, unQueueGuide);
