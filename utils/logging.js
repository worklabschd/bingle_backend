/* eslint no-unused-vars: "off" */
const winston = require('winston');
const _ = require('winston-mongodb').MongoDB;
const { DB_NAME } = require('../config.json').database;

const isProd = process.env.NODE_ENV === 'production';

// If started inside a Docker container. Modify the uri accordingly.
const uri = `mongodb://${isProd ? 'mongo' : 'localhost'}:27017/${DB_NAME}_logs`;

const logger = new (winston.Logger)({
    transports: [
        new (winston.transports.MongoDB)({ level: 'error', cappedMax: 200, db: uri })
    ]
});

const queryLogs = (days, callback) => {
    logger.query({
        from: new Date() - (24 * 60 * 60 * 1000 * days || 1),
        until: new Date(),
        limit: 30,
        start: 0,
        order: 'desc',
        fields: ['message', 'timestamp']
    }, (err, logs) => {
        callback(err, logs);
    });
};

const log = message => {
    // If this is production. Persist logs.
    if (isProd) {
        logger.error(message instanceof Error ? message.toString() : message);
    }
    console.error(message);
};

export { queryLogs };

export default log;
