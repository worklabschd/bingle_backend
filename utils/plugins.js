import swaggered from 'hapi-swaggered';
import swaggeredUI from 'hapi-swaggered-ui';
import vision from 'vision';
import inert from 'inert';
import cors from 'hapi-cors';
import log from './logging';
import chatPlugin from './chat';

const swaggeredPlugin = {
    register: swaggered,
    options: {
        supportedMethods: ['get', 'post', 'delete', 'put'],
        info: {
            title: 'Bingle',
            description: 'API documentation for Meego Backend. Written in Node.js and Hapi.',
            version: '0.0.1',
            contact: {
                name: 'Worklabs',
                url: 'www.Worklabs.com',
                email: 'Worklabs@gmail.com'
            }
        },
        tags: [
            { name: 'user', description: 'All customer app endpoints go here.' },
            { name: 'guide', description: 'All tour guide app endpoints go here.' },
            { name: 'portal', description: 'All admin panel portal endpoints go here.' },
            { name: 'hooks', description: 'Web hooks used for providing access from 3rd party APIs.' },
            { name: 'web', description: 'All open APIs that are used inside the public website.' },
            { name: 'partner', description: 'All partner dashboard APIs go here.' }
        ]
    },
};

const swaggeredUIPlugin = {
    register: swaggeredUI,
    options: {
        title: 'Bingle',
        path: '/swag',
        swaggerOptions: {
            validatorUrl: null,
        },
    },
};

const corsPlugin = {
    register: cors,
    options: {
        methods: ['GET, POST, PUT, OPTIONS, DELETE']
    }
};

const loggerPlugin = {
    register(server, options, next) {
        const stringer = (error, request) => `error - ${error}. route - ${request.path}. queryParams - ${JSON.stringify(request.query)}, params - ${JSON.stringify(request.params)}, payload - ${JSON.stringify(request.payload)}.`;

        // Request error logging callback for 500s.
        server.on('request', (request, event) => {
            const message = stringer(event.data, request);

            // Start logging this error. Persist it in production.
            log(message);
        });

        // Request exception error logging callback for uncaught exceptions.
        server.on('request-error', (request, event) => {
            const message = stringer(event, request);

            // Start logging this exception. Persist it in production.
            log(message);
        });

        next();
    }
};

loggerPlugin.register.attributes = {
    name: 'Logging plugin for Meego Backend.',
    version: '0.0.1'
};

const fileHandlerPlugin = {
    register(server, options, next) {
        server.route({
            method: 'GET',
            path: '/{param*}',
            handler: {
                directory: {
                    path: 'assets'
                }
            }
        });
        next();
    }
};

fileHandlerPlugin.register.attributes = {
    name: 'File handler plugin for Meego Backend.',
    version: '0.0.1'
};

export default [
    corsPlugin,
    vision,
    inert,
    swaggeredPlugin,
    swaggeredUIPlugin,
    loggerPlugin,
    fileHandlerPlugin,
    chatPlugin
];
