import Socket from 'socket.io';

import { Chat, ChatMessage, WifiRoom } from '../../models';

const chatPlugin = {
    register(server, options, next) {

        var io = Socket(server.select('api').select('chat').listener);
        io.on('connection', (socket) => {

            /**
             * Join the user with his own room and wifi room and notifying all
             * Meta : {
             *  event : "join"
             *  data : {
             *          username: String,
             *          room: String
             *      }
             * }
             */
            socket.on('join', (data)=>{
                // join user with username
                if(data.username) {
                    /**
                     * Pending tasks:
                     * 1. authenticate user first with something like token and username otherwise return
                     */
                    socket.join(`user:${data.username}`);
                    socket.username = data.username;
                    
                    // Notify everyone that i am here
                    socket.broadcast.emit('notification:user_online', {username: data.username});

                    // Get list of all users online
                    let allUsers = Object.keys(io.sockets.sockets).map((key)=>{
                        return (io.sockets.sockets[key] && io.sockets.sockets[key].username) || '';
                    });
                    socket.emit('notifications:list_of_online_users', {userList: allUsers});

                    // Join user with wifi-room
                    if(data.room) {
                        /**
                         * Pending tasks:
                         * 1. Log into database that user joined the room now.
                         * 2. check whether user should connect to room or not
                         */

                        // get all users in this room and send to him first
                        let usersInRoom = (io.sockets.adapter.rooms[`room:${data.room}`] && Object.keys(io.sockets.adapter.rooms[`room:${data.room}`].sockets)) || [];
                        usersInRoom = usersInRoom.map((key)=>{
                            return (io.sockets.sockets[key] && io.sockets.sockets[key].username) || '';
                        })
                        socket.emit('notifications:list_of_users_in_room', {userList: usersInRoom});

                        // Join this room
                        socket.join(`room:${data.room}`);
                        socket.roomName = data.room;

                        // tell everyone in room that i am here
                        socket.broadcast.in(`room:${data.room}`).emit('notification:new_user_in_room', {username: data.username});

                        // Add entry to room table that I joined
                        WifiRoom.create({
                            room: socket.roomName,
                            user: socket.username,
                            status: 'joined'
                        }, function (err, response) {
                            return;
                        });
                    } else {
                        /**
                         * Pending tasks:
                         * 1. decide what to do when user connected from internet
                         */
                        socket.join('room:no_wifi');
                    }
                } else {
                    console.log('Chat logs : username not provided so not connecting', socket);
                    socket.emit('error:no_username_provided_to_join');
                }
            });




            /**
             * On Client Message, add chat id, save message and broadcast to both usernames rooms
             * Meta : {
             *  event : "client:message"
             *  data : {
             *          from: String,
             *          to: String,
             *          message: String
             *      }
             * }
             */
             socket.on('client:message', (data)=>{
                if(!(data.from && data.to && data.message)){
                    socket.emit('error:message_not_sent', {message: 'Reuired fields are not provided : from, to and message'});
                    return;     
                }

                if(socket.username != data.from){
                    socket.emit('error:message_not_sent', {message: 'Not Authenticated'});
                    return; 
                }

                Chat.findOneAndUpdate({ $or:[{users: { $eq: [ data.from, data.to ] } }, {users: { $eq: [ data.to, data.from ] } }] }, { users: [data.from, data.to], updatedAt: Date.now() }, { upsert: true, new: true, setDefaultsOnInsert: true }, function(error, result) {
                    if (error) {
                        socket.emit('error:message_not_sent', {message: 'Error in fetching chat'});
                        return;
                    }

                    ChatMessage.create({
                        chat: result.id,
                        from: data.from,
                        to: data.to,
                        message: data.message
                    }, function (err, response) {
                        if (err) {
                            socket.emit('error:message_not_sent', {message: 'Error in saving message'});
                            return;
                        }

                        io.sockets.in(`user:${data.from}`).emit('server:message', {from: data.from, to: data.to, message:data.message, chat_id: result.id});
                        io.sockets.in(`user:${data.to}`).emit('server:message', {from: data.from, to: data.to, message:data.message, chat_id: result.id});
                    });
                });
             });


             /**
             * Handling event when user disconnect especially loggin his room leave time
             * Meta : {
             *  event : "disconnect"
             */
            socket.on('disconnect', ()=>{
                // see he belongs to any room
                if(socket.username && socket.roomName) {
                    
                    // Notify everyone that i am leaving
                    socket.broadcast.emit('notification:user_offline', {username: socket.username});
                    socket.broadcast.in(`room:${socket.roomName}`).emit('notification:user_left_room', {username: socket.username});

                    WifiRoom.create({
                        room: socket.roomName,
                        user: socket.username,
                        status: 'left'
                    }, function (err, response) {
                        return;
                    });
                    
                }
            });
        });
        next();
    }
};

chatPlugin.register.attributes = {
    name: 'chat-module',
    version: '1.0.0'
};

export default chatPlugin;
