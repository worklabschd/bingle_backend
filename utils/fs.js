import ba64 from 'ba64';
import uuid from 'uuid/v1';
import path from 'path';
import fs from 'fs';

const PUBLIC_FOLDER = 'assets';
const IMAGE_FOLDER = 'images';

// Function to delete any image.
const deleteFile = (folder, fileName) => new Promise((resolve, reject) => {
    const oldFSPath = path.join(PUBLIC_FOLDER, IMAGE_FOLDER, folder, fileName);
    fs.unlink(oldFSPath, err => {
        if (err) {
            reject(err);
            return;
        }
        resolve();
    });
});

// Function to save a base64 image.
// Resolves with the the new filename with which the file was saved.
const saveImage = (folder, image) => new Promise((resolve, reject) => {
    const fileName = uuid();
    const newFSPath = path.join(PUBLIC_FOLDER, IMAGE_FOLDER, folder, fileName);
    ba64.writeImage(newFSPath, image, err => {
        if (err) {
            reject(err);
            return;
        }
        resolve(`${fileName}.${ba64.getExt(image)}`);
    });
});

export { deleteFile, saveImage, PUBLIC_FOLDER, IMAGE_FOLDER };
