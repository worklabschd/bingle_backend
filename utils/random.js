import * as _ from 'lodash';
import CallRequest from '../models/callRequest';

const randInt = digits => _.random('9'.repeat(digits));

const randCallRequestToken = async () => {
    let rand = randInt(8);
    const request = await CallRequest.findOne({
        callCentreToken: rand
    });
    // If this token exists, generate another token.
    if (request) {
        rand = await randCallRequestToken();
    }
    return rand;
};

const selectRandElements = (elements, selectCount) => {
    if (elements.length <= selectCount) {
        return elements;
    }
    const selectedIndexes = [];
    while (selectedIndexes.length !== selectCount) {
        const randomIndex = _.random(elements.length - 1);
        if (!selectedIndexes.includes(randomIndex)) {
            selectedIndexes.push(randomIndex);
        }
    }
    const selectedElements = _.pullAt(elements, selectedIndexes);
    return selectedElements;
};

export { randCallRequestToken, selectRandElements };
