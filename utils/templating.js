// Meego's templating engine.
import handlebars from 'handlebars';
import path from 'path';

export default {
    engines: {
        html: handlebars
    },
    relativeTo: path.join(__dirname, '..'),
    path: 'views'
};
